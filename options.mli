(* SPDX-License-Identifier: GPL-2.0 *)
val save_tmp : bool ref
val min : float ref
val max : float ref
val ranges : (float * float * string list) list ref
val after_sleep : bool ref
val nograph : bool ref
val outdir : string ref
val debug : bool ref
val tmpdir : string ref
val relaxed : bool ref
val standard_xsize : float ref
val standard_ysize : float ref
val modifiers : string list ref
val nanoseconds : bool ref
val syncstart : bool ref
val cmds : (int * string) list ref
val pids : int list ref

val generic_options : (string * Arg.spec * string) list
val fixmin : unit -> unit
