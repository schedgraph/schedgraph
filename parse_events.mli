(* SPDX-License-Identifier: GPL-2.0 *)
val target : string list ref
val ignored : string list ref
val nox : Str.regexp list ref
val sedout : Str.regexp list ref
val normeq : bool ref
val noargs : bool ref
val generic_options : (string * Arg.spec * string) list

val contains : string -> string -> bool
val event_ok : string -> bool
val isnum : string -> bool
val normalize_event : string -> string list -> string
val core_from_event : string -> int -> int
val getmark : int -> string
