(* SPDX-License-Identifier: GPL-2.0 *)
(* Generic code to keep track of runqueues.  Wakeup events are optional *)

type states = Running | RunningSleep | RunningYield | Waiting | Pending
type direction = Src of states | Dst of states

let init cores =
  let running = ([Running],Array.make cores []) in
  (* something is waiting and something is running *)
  let waiting = ([Waiting],Array.make cores []) in
  (* something is waiting but nothing is running *)
  let pending = ([Pending],Array.make cores []) in
  (running,waiting,pending)

let dir2c = function
    Src Running -> "src running"
  | Src RunningSleep -> "src running sleep"
  | Src RunningYield -> "src running yield"
  | Src Waiting -> "src waiting"
  | Src Pending -> "src pending"
  | Dst Running -> "dst running"
  | Dst RunningSleep -> "dst running sleep"
  | Dst RunningYield -> "dst running yield"
  | Dst Waiting -> "dst waiting"
  | Dst Pending -> "dst pending"

let find tbl dir st =
  let rec loop = function
      st::rest ->
	(try Hashtbl.find tbl (dir st)
	with _ -> loop rest)
    | _ -> (fun x y z -> ()) in
  loop st

let extend ops (ts,state) time core pid =
  let cur = Array.get state core in
  (if not (List.mem pid cur)
  then
    begin
      Array.set state core (pid :: cur);
      List.iter (fun ops -> find ops (fun x -> Dst x) ts time core pid) ops
    end)

let drop_bool ops (fs,state) time core pid =
  let cur = Array.get state core in
  if List.mem pid cur
  then
    begin
      Array.set state core
	(List.filter (fun x -> x <> pid) cur);
      List.iter (fun ops -> find ops (fun x -> Src x) fs time core pid) ops;
      true
    end
  else false

let drop ops s time core pid =
  ignore (drop_bool ops s time core pid)

let displace ops (fs,src) (ts,dst) time core =
  let srcfn pid =
    List.iter (fun ops -> find ops (fun x -> Src x) fs time core pid) ops in
  let dstfn pid =
    List.iter (fun ops -> find ops (fun x -> Dst x) ts time core pid) ops in
  List.iter
    (fun pid ->
      Array.set src core (List.filter (fun x -> x <> pid) (Array.get src core));
      srcfn pid;
      Array.set dst core (pid :: Array.get dst core);
      dstfn pid)
    (Array.get src core)

let check_disjoint (_,running) (_,waiting) (_,pending) str =
  if !Options.debug
  then
    begin
      let check l1 l2 msg =
	if List.exists (fun x -> List.mem x l2) l1
	then failwith (Printf.sprintf "%s: %s" str msg) in
      let r =
	Array.fold_left
	  (fun prev cur -> check cur prev "in running"; cur @ prev)
	  [] running in
      let w =
	Array.fold_left
	  (fun prev cur -> check cur prev "in waiting"; cur @ prev)
	  [] waiting in
      let p =
	Array.fold_left
	  (fun prev cur -> check cur prev "in pending"; cur @ prev)
	  [] pending in
      check r w "intersection between running and waiting";
      check r p "intersection between running and pending";
      check w p "intersection between waiting and pending"
    end

let process_line time core ops (running,waiting,pending) = function
    Parse_line.Sched_process_exec(cmd,oldcmd,pid,oldpid) ->
      drop ops running time core oldpid;
      extend ops running time core pid;
      check_disjoint running waiting pending "exec"
  | Parse_line.Sched_switch(fromcmd,frompid,state,tocmd,topid) ->
      (if frompid > 0
       then
         begin
	   let running =
	     if state = Parse_line.Yield
	     then ([RunningYield;Running],snd running)
	     else ([RunningSleep;Running],snd running) in
           drop ops running time core frompid;
           (if state = Parse_line.Yield
           then
	     (if topid > 0
	     then extend ops waiting time core frompid
             else extend ops pending time core frompid))
         end);
      (if topid > 0
      then
	begin
	  if not (drop_bool ops pending time core topid)
	  then drop ops waiting time core topid;
	  extend ops running time core topid;
	  displace ops pending waiting time core
	end
      else displace ops waiting pending time core);
      check_disjoint running waiting pending "switch"
  | Parse_line.Sched_wakeup_new(cmd,pid,_,cpu)
  | Parse_line.Sched_wakeup(cmd,pid,_,cpu) ->
      (match Array.get (snd running) cpu with
	[] -> extend ops pending time cpu pid
      | l ->
	  if not (List.mem pid l)
	  then extend ops waiting time cpu pid);
      check_disjoint running waiting pending "wakeup"
  | Parse_line.Sched_migrate_task(cmd,pid,oldcpu,newcpu,state) ->
      let present = (* only migrate if present *)
	match Array.get (snd running) oldcpu with
	  [] -> drop_bool ops pending time oldcpu pid
	| _ -> drop_bool ops waiting time oldcpu pid in
      (if present
      then
	match Array.get (snd running) newcpu with
	  [] -> extend ops pending time newcpu pid
	| _ -> extend ops waiting time newcpu pid)
  | _ -> ()
