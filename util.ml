(* SPDX-License-Identifier: GPL-2.0 *)
module O = Options

let sys_command command =
  (if !O.debug then (Printf.eprintf "syscommand: %s\n" command; flush stderr));
  Sys.command command

let process_output_to_list2 = fun command ->
  let chan = Unix.open_process_in command in
  let res = ref ([] : string list) in
  let rec process_otl_aux () =
    let e = input_line chan in
    res := e::!res;
    process_otl_aux() in
  try process_otl_aux ()
  with End_of_file ->
    let stat = Unix.close_process_in chan in (List.rev !res,stat)
let cmd_to_list command =
  (if !O.debug then Printf.eprintf "running: %s\n" command);
  let (l,_) = process_output_to_list2 command in l
let process_output_to_list = cmd_to_list
let cmd_to_list_and_status = process_output_to_list2

let ios n s =
  try int_of_string s
  with _ -> failwith (Printf.sprintf "util: %d: ios %s failed" n s)

let hashadd tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref [] in
      Hashtbl.add tbl k cell;
      cell in
  cell := v :: !cell

let hashadd_set tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref [] in
      Hashtbl.add tbl k cell;
      cell in
  if not (List.mem v !cell)
  then cell := v :: !cell

let hashadd_unique tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref [] in
      Hashtbl.add tbl k cell;
      cell in
  if List.mem v !cell
  then failwith (Printf.sprintf "non unique value\n")
  else cell := v :: !cell

let hashinc tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref 0 in
      Hashtbl.add tbl k cell;
      cell in
  cell := v + !cell

let hashincf tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref 0. in
      Hashtbl.add tbl k cell;
      cell in
  cell := v +. !cell

let mean l =
  (List.fold_left (+.) 0.0 l) /. (float_of_int (List.length l))

(* thanks to rosetta code *)
let nthroot n a ?(tol=0.001) () =
   let nf = float n in let nf1 = nf -. 1.0 in
   let rec iter x =
      let x' = (nf1 *. x +. a /. (x ** nf1)) /. nf in
      if tol > abs_float (x -. x') then x' else iter x' in
   iter 1.0
(*;;
 
let () =
  Printf.printf "%g\n" (nthroot 10 (7131.5 ** 10.0) ());
  Printf.printf "%g\n" (nthroot 5 34.0 ());*)

let geomean l =
  let l = List.filter (fun x -> not(Float.is_nan x)) l in
  nthroot (List.length l) (List.fold_left ( *. ) 1.0 l) ()

let geosd l =
  let l = List.filter (fun x -> not(Float.is_nan x)) l in
  let gmean = geomean l in
  let sumlogs =
    List.fold_left
      (fun prev cur ->
	let n = log (cur /. gmean) in
	n *. n)
      0.0 l in
  exp(sqrt(sumlogs /. (float_of_int (List.length l))))

let stddev l = (* sample standard deviation *)
  let lenm1 = float_of_int(List.length l - 1) in
  let mean = mean l in
  let squares =
    List.fold_left
      (fun prev xi ->
	let tmp = xi -. mean in
	(tmp *. tmp) +. prev)
      0.0 l in
  sqrt(squares /. lenm1)

let pearson l =
  let l1 = List.fold_left (fun prev (x,_) -> x :: prev) [] l in
  let l2 = List.fold_left (fun prev (_,y) -> y :: prev) [] l in
  let avgl1 = mean l1 in
  let avgl2 = mean l2 in
  let num =
    List.fold_left2
      (fun prev x y ->
	prev +. ((x -. avgl1) *. (y -. avgl2)))
      0. l1 l2 in
  let denmaker l avg =
    sqrt
      (List.fold_left
	 (fun prev x ->
	   let v = x -. avg in
	   prev +. (v *. v))
	 0. l) in
  num /. ((denmaker l1 avgl1) *. (denmaker l2 avgl2))

let speedup_and_std lower_is_better refavg values =
  if lower_is_better
  then
    let speedup = refavg /. (mean values) in
    let slowdowns =
      List.map (fun v -> v /. refavg) values in
    let invstdslowdowns = 1. /. ((mean slowdowns) -. (stddev slowdowns)) in
    (speedup,invstdslowdowns -. speedup)
  else
    let speedup = (mean values) /. refavg in
    let speedups =
      List.map (fun v -> v /. refavg) values in
    let stdspeedups = stddev speedups in
    (speedup,stdspeedups)

let iota n =
  let rec loop acc = function
      0 -> acc
    | n -> loop ((n-1) :: acc) (n-1) in
  loop [] n

(* ----------------------------------------------------- *)

let union l1 l2 =
  List.fold_left
    (fun prev x -> if List.mem x prev then prev else x::prev)
    l2 l1

(* ----------------------------------------------------- *)

type file_context =
    { text_file_name : string;
      after_sleep : bool;
      interrupt : bool;
      cores : int;
      start : float; (* actual time *)
      endoffset : float; (* actual end time *)
      drop : bool (* drop txt file? *) }

(* minimal trace parsing *)
let get_front c s =
  try String.sub s 0 (String.index s c)
  with _ -> s

let rec skip_to_sleep i interrupt =
  let l = input_line i in
  let (core,time,l) = Parse_line.parse_line l interrupt ["sched_switch"] in
  match l with
    Parse_line.Sched_switch("sleep",_,Parse_line.Terminate,_,_) -> time
  | _ -> skip_to_sleep i interrupt

let rec skip_front_continue ctx =
  let i = open_in ctx.text_file_name in
  let i1 = open_in ctx.text_file_name in
  let rec loop _ =
    let l = input_line i in
    let _ = input_line i1 in
    if get_front '=' l = "cpus"
    then
      (if ctx.after_sleep
      then
	begin
	  close_in i1;
	  ignore(skip_to_sleep i ctx.interrupt)
	end
      else
	begin
	  (try ignore(input_line i1)
	  with End_of_file ->
	    failwith "no data - did you ask for --freq but have no frequency info?");
	  close_in i1
	end)
    else loop() in
  loop();
  i

(* ----------------------------------------------------- *)

(* new attempt at collecting trace metadata *)
(* return number of cores, starting and ending time (if needed) *)

let time_from_one_line l =
  let (core,time,l) = Parse_line.parse_line l false [] in
  time

let times_from_text after_sleep have_end fl =
  let i = open_in fl in
  let rec get_cpus _ =
    let l = input_line i in
    match String.split_on_char '=' l with
      ["cpus";n] -> ios 1 n
    | _ -> get_cpus() in
  let cpus = get_cpus() in
  let minline = (* absolute min time *)
    if after_sleep
    then skip_to_sleep i false
    else time_from_one_line (input_line i) in
  let maxline = (* offset from start *)
    match have_end with
      Some endpoint -> endpoint
    | None -> time_from_one_line (List.hd (cmd_to_list ("tail -1 "^fl))) -. minline in
  close_in i;
  (cpus,minline,maxline)

let more_than_3_1_2 ver =
  let l = String.split_on_char '.' ver in
  let l = List.map int_of_string l in
  l >= [3;1;2]

let has_last_event file =
  let version =
    cmd_to_list
      (Printf.sprintf "trace-cmd report --version -q %s | head -5" file) in
  let version = List.map (fun s -> String.split_on_char ' ' s) version in
  match version with
    [["version";"=";n];[cpus];[ver;id]]
    when ios 2 n >= 7 && more_than_3_1_2 ver -> Some cpus
  | [[cpus];[ver;id]]
    when more_than_3_1_2 ver -> Some cpus
  | _ -> None

let get_minline file =
  let getnum l =
    float_of_string
      (String.trim
         (List.nth (String.split_on_char ':' l) 1)) in
  let minlines =
    cmd_to_list(Printf.sprintf "trace-cmd report --first-event %s" file) in
  let minlines =
    match minlines with
      "List of CPUs in mtrace.dat with data:"::rest -> List.map getnum rest
    | _::_::rest -> List.map getnum rest
    | _ -> failwith "unrecognized trace-cmd report --first-event output" in
  List.fold_left min (List.hd minlines) minlines

let get_maxline file =
  let getnum l =
    float_of_string
      (String.trim
         (List.nth (String.split_on_char ':' l) 1)) in
  let maxlines =
    cmd_to_list(Printf.sprintf "trace-cmd report --last-event %s" file) in
  let maxlines =
    match maxlines with
      "List of CPUs in mtrace.dat with data:"::rest -> List.map getnum rest
    | _::_::rest -> List.map getnum rest
    | _ -> failwith "unrecognized trace-cmd report --first-event output" in
  List.fold_left max (List.hd maxlines) maxlines

(* returns the actual start and the end offset *)
let get_start_end_dat_file after_sleep have_end tmp file =
  let getnum l =
    float_of_string
      (String.trim
         (List.nth (String.split_on_char ':' l) 1)) in
  match (after_sleep,has_last_event file) with
    (true,_) | (false,None) ->
      let base = Filename.chop_extension file in
      let b = String.concat "_" (String.split_on_char '/' base) in
      let fl = Filename.temp_file ~temp_dir:tmp b ".txt" in
      let extra =
	if have_end = None
	then ""
	else if after_sleep
	then " | head -50000"
	else " | head -500" in (* 500 lines should be enough to get past cores *)
      ignore
	(sys_command
	   (Printf.sprintf
	      "trace-cmd report -q -F sched_switch -F sched_process_fork -F sched_process_exec %s%s > %s"
	      file extra fl));
      times_from_text after_sleep have_end fl
  | (false,Some cpus) ->
      let minline = get_minline file in
      let cpus =
	match String.split_on_char '=' cpus with
	  ["cpus";n] -> ios 3 n
	| _ -> failwith "no cpu information" in
      let maxline =
	match have_end with
	  Some endpoint -> endpoint
	| None ->
	    let maxlines =
              cmd_to_list(Printf.sprintf "trace-cmd report --last-event %s" file) in
	    let maxlines = List.map getnum (List.tl(List.tl maxlines)) in
	    List.fold_left max (List.hd maxlines) maxlines -. minline in
      (cpus,minline,maxline)

(* ----------------------------------------------------- *)

(* get text traces *)
let get_files_and_endpoint files after_sleep interrupt events =
  let e = !O.max in
  let have_end =
    if List.length files <= 1 && e < 0.
    then Some e (* no need to get the end information *)
    else if e < 0. then None else Some e in
  let ctxres =
    { text_file_name = ""; after_sleep = after_sleep; interrupt = interrupt;
      cores = 0; start = 0.0; endoffset = 0.0; drop = false } in
  let infos_and_offsets =
    List.map
      (function file ->
	let base = Filename.chop_extension file in
	let local_base = Filename.basename base in
	if Filename.extension file = ".dat"
	then
	  begin
	    let (cores,start,end_offset) =
	      get_start_end_dat_file after_sleep have_end !O.tmpdir file in
	    let b = String.concat "_" (Str.split (Str.regexp "/") base) in
	    let fl =
	      let ext = Printf.sprintf "_%d.txt" (Unix.getpid()) in
	      Filename.temp_file ~temp_dir:!O.tmpdir b ext in
	    let arg = if interrupt then "-l" else "" in
	    let cmd =
	      let extra =
		if events = []
		then ""
		else
		  let events =
		    if List.mem "sched_process_exec" events
		    then "sched_process_fork" :: events (* see parse_line.ml *)
		    else events in
		  let events =
		    let (ns,events) =
		      List.partition (fun x -> x = "nosched") events in
		    let events =
		      String.concat " "
			(List.map (fun x -> "-F "^x) events) in
		    let ns = if ns = [] then "" else "-v -F sched" in
		    Printf.sprintf "%s %s" events ns in
		  if after_sleep
		  then "-F '.*:prev_comm == \"sleep\"' "^events
		  else events in
	      let extra = if !O.nanoseconds then " -t "^extra else "" in
	      if e < 0.0
	      then Printf.sprintf "trace-cmd report -t %s %s %s > %s"
		  arg extra file fl
	      else
		(* add a little extra at the end for the vm case, where host and
		   guest don't start at the same place, could calculate the start
		   and end times *)
		let e = if after_sleep then e +. 1.1 else e in
		let e = int_of_float(e *. 1000.) + 1 in
		let ofl = Filename.temp_file b ".dat"  in
		let c1 = Printf.sprintf "trace-cmd split -m %d -i %s -o %s > /dev/null 2>&1" e file ofl in
		ignore(sys_command c1);
		let ofl1 = ofl^".1" in
		let ofl = if Sys.file_exists ofl1 then ofl1 else ofl in
		let c2 =
		  Printf.sprintf "(trace-cmd report -t %s %s %s > %s)"
		    arg extra ofl fl in
		let c3 = Printf.sprintf "/bin/rm %s" ofl in
		Printf.sprintf "%s && %s" c2 c3 in
	    let res = sys_command cmd in
	    (if res <> 0 then (Printf.eprintf "failure on %s: %d\n" file res; flush stderr));
	    ((local_base,
	      {ctxres with text_file_name = fl; cores = cores; start = start;
		endoffset = end_offset; drop = true}),
	     end_offset)
	  end
	else
	  let (cores,start,end_offset) = times_from_text after_sleep have_end file in
	  ((local_base,
	    {ctxres with text_file_name = file; cores = cores; start = start;
	      endoffset = end_offset; drop = false}),
	   end_offset))
      files in
  let (infos,end_offsets) = List.split infos_and_offsets in
  O.max := List.fold_left max e end_offsets;
  let infos =
    if !O.syncstart
    then
      let minstart =
        List.fold_left (fun prev (_,cur) -> min prev cur.start)
	  (snd(List.hd infos)).start (List.tl infos) in
      List.map
        (fun (fl,cur) -> (fl, {cur with start = minstart}))
	infos
    else infos in
  infos

(* don't worry about who is the host; just get the difference between
the earliest and latest and add that to max *)
let synchronize_max files =
  List.iter
    (function file ->
      if has_last_event file = None
      then failwith "--first-event not supported, use a more recent version of trace-cmd")
    files;
  let minlines = List.map get_minline files in
  let fst = List.hd minlines in
  let others = List.tl minlines in
  let (mn,mx) =
    List.fold_left
      (fun (mn,mx) cur -> (min mn cur, max mx cur))
      (fst,fst) others in
  O.max := !O.max +. (mx -. mn)

(* ----------------------------------------------------- *)

let remove callsite info =
  if info.drop
  then
    begin
    (if !O.debug
     then Printf.eprintf "%d: removing %s\n" callsite info.text_file_name);
      Sys.remove info.text_file_name
    end

(* ----------------------------------------------------- *)

let iparse_loop ctx startpoint endpoint elements structure process_line =
  let last = ref (-1.0) in
  let pointers =
    List.map skip_front_continue ctx in
  let (cores,hstart) = (* first ctx is the host *)
    let host_context = List.hd ctx in
    (host_context.cores,host_context.start) in
  let start =
    List.fold_left (fun start ctx -> max start ctx.start) hstart ctx in
  let startpoint = startpoint +. start in
  let endpoint = if endpoint <= 0.0 then endpoint else endpoint +. start in
  let buffer = Array.make (List.length ctx) None in
  let ports =
    Array.of_list
      (List.map2 (fun ctx i -> (ctx.interrupt,i))
	 ctx pointers) in
  let rec loop _ =
    let (_,chosen) =
      Array.fold_left
	(fun (index,best) elem ->
	  try
	    let (core,time,l) as v =
	      match elem with
		Some((core,time,l) as v) -> v
	      | None ->
		  let (interrupt,i) = Array.get ports index in
		  let l = input_line i in
		  let ((core,time,l) as v) =
		    Parse_line.iparse_line index l interrupt elements in
		  Array.set buffer index (Some v);
		  v in
	    last := max time !last;
	    if endpoint <= 0.0 || time <= endpoint
	    then
	      match best with
		Some(_,_,best_time,_) ->
		  if time < best_time
		  then (index+1,Some(index,core,time,l))
		  else (index+1,best)
	      | None -> (index+1,Some(index,core,time,l))
	    else (index+1,best)
	  with End_of_file -> (index+1,best))
	(0,None) buffer in
    match chosen with
      Some(index,core,time,l) ->
	Array.set buffer index None;
	process_line index time core structure startpoint l;
	loop()
    | None -> () in
  let res =
    try (loop();true)
    with
      End_of_file -> true
    | Failure s ->
	Printf.printf "Failure %s\n" s; flush stdout;
	false in
  Array.iter (fun (interrupt,i) -> close_in i) ports;
  (res,start,!last,structure)

(* don't care about index *)
let parse_loop ctx startpoint endpoint elements structure process_line =
  iparse_loop [ctx] startpoint endpoint elements structure (fun _ -> process_line)

(* ----------------------------------------------------- *)

let choose_target target files =
  match !target with
    [] ->
      let fronts =
	List.map
	  (function file ->
	    let file = get_front '_' file in
	    match Str.split (Str.regexp_string ".") file with
	      [_;_;"x"] -> file (* NAS *)
	    | ["parsec";x] | ["splash2";x] | ["splash2x";x] -> x
	    | _ -> file)
	  files in
      let fronts =
	List.fold_left
	  (fun prev x -> if List.mem x prev then prev else x::prev)
	  [] fronts in
      let fronts =
	List.map
	  (function file ->
	    if String.length file > 15
	    then String.sub file 0 15
	    else file)
	  fronts in
      target := fronts
  | _-> ()

let take n l =
  let rec loop n = function
      [] -> []
    | x::xs ->
	if n = 0
	then []
	else x :: loop (n-1) xs in
  loop n l

let mask2cores msk =
  let newmask = Str.split (Str.regexp ",") msk in
  List.concat
    (List.map
       (fun l ->
	 match Str.split (Str.regexp_string "-") l with
	   [a;b] ->
	     let mx = ios 4 b in
	     let rec loop n =
	       if n = mx then [n]
	       else n :: loop (n+1) in
	     loop (ios 5 a)
	 | [a] -> [ios 6 a]
	 | _ -> failwith "bad mask range")
       newmask)

(* ----------------------------------------------------- *)
(* finding phase boundaries by command name *)

let matches s2 matches_succeeded s1 = (* is s1 a prefix of s2 *)
  let n1 = String.length s1 in
  let n2 = String.length s2 in
  let res = n2 >= n1 && (String.sub s2 0 n1 = s1) in
  (if res then matches_succeeded := true);
  res

let process_line _ time core (commands,live,livepids,tbl,start,index) startpoint l =
  let check_relevant cmd pid commands =
    if List.exists (matches cmd (ref false)) commands
    then
      begin
	live := Some cmd;
	Hashtbl.replace livepids pid ();
	let i = !index in
	index := !index + 1;
	let s = time -. start in
	hashadd tbl cmd (s,s,cmd,[pid],i)
      end in
  if time >= startpoint
  then
    match l with
      Parse_line.Sched_process_exec(cmd,_,pid,oldpid) ->
	let cmd = Filename.basename cmd in
	if !live = None
	then check_relevant cmd pid commands
	else if pid <> oldpid && Hashtbl.mem livepids oldpid
	then
	  begin
	    Hashtbl.remove livepids oldpid;
	    Hashtbl.replace livepids pid ()
	  end
    | Parse_line.Sched_process_fork(_,parent_pid,cmd,pid) ->
	if !live = None
	then check_relevant cmd pid commands
	else if Hashtbl.mem livepids parent_pid
	then Hashtbl.replace livepids pid ()
    | Parse_line.Sched_switch(fromcmd,frompid,Parse_line.Terminate,tocmd,topid) ->
	(match !live with
	  Some cmd ->
	    Hashtbl.remove livepids frompid;
	    if Hashtbl.length livepids = 0
	    then
	      begin
		live := None;
		let cell = Hashtbl.find tbl cmd in
		(match !cell with
		  (s,e,cmd,pid,index)::rest -> cell := (s,time -. start,cmd,pid,index)::rest
		| _ -> failwith "not possible")
	      end
	| _ -> ());
	(if !live = None
	then check_relevant tocmd topid commands)
    | _ -> ()

(* return a table of start and end points that match a command *)
let interpret_show_commands commands ctx startpoint endpoint =
  let commands =
    let commands = List.map String.trim commands in
    List.filter (fun x -> x <> "" && String.get x 0 <> '#') commands in
  let events = ["sched_switch";"sched_process_exec";"sched_process_fork"] in
  let tbl = Hashtbl.create 101 in
  let live = ref None in
  let livepids = Hashtbl.create 101 in
  let index = ref 0 in
  let ctx = { ctx with after_sleep = false } in (* start point may be before after_sleep *)
  let (success,start,last,(commands,live,livepids,tbl,_,_)) =
    iparse_loop [ctx] startpoint endpoint events (commands,live,livepids,tbl,ctx.start,index)
      process_line in
  (match !live with
    None -> ()
  | Some cmd ->
      let cell = Hashtbl.find tbl cmd in
      (match !cell with
	(s,e,cmd,pid,index)::rest -> cell := (s,last -. ctx.start,cmd,pid,index)::rest
      | _ -> failwith "not possible"));
  let res = Hashtbl.fold (fun k v r -> !v @ r) tbl [] in
  let res =
    List.rev
      (List.fold_left
        (fun prev (s,e,cmd,pid,index) ->
	  if e <= 0.
	  then prev
	  else (max 0. s,e,cmd,pid,index) :: prev)
	[] res) in
  List.sort compare res

(* -------------------------------------------------------------------- *)
(* Phoronix *)

type cmdres = Cmd of string | Pids of string * int list

let find_cmd file =
  let fork_lines =
    if Filename.extension file = ".dat"
    then cmd_to_list ("trace-cmd report -F sched_process_fork "^file^" | grep fork")
    else cmd_to_list ("grep sched_process_fork "^file) in
  let fork_lines =
    List.map
      (fun line ->
	match String.split_on_char '=' line with
	  _::parent_comm::parent_pid::child_comm::_ ->
	    (String.sub parent_comm 0 (String.length parent_comm - 4),
	     String.sub child_comm 0 (String.length child_comm - 10),
	     ios 7 (String.sub parent_pid 0 (String.length parent_pid - 11)))
	| _ -> failwith ("bad fork line: "^line))
      fork_lines in
  try
    let file = Filename.basename file in
    let front = String.sub file 0 6 in
    if front = "trace_"
    then
      let rest = String.sub file 6 (String.length file - 6) in
      let opt = List.hd (Str.split (Str.regexp "\\-[0-9]") rest) in
      if List.exists (fun (a,b,_) -> b = opt) fork_lines
      then Some (Cmd opt)
      else
	begin
	  (* two successive cases with the same parent and child *)
	  let tbl = Hashtbl.create 101 in
	  let ptbl = Hashtbl.create 101 in
	  let rec loop = function
	      (a,b,apid)::(((c,d,cpid)::_) as rest) ->
		(if a = b && a = c && a = d
		then
		  begin
		    hashinc tbl a 1;
		    hashadd ptbl a apid
		  end);
		loop rest
	    | _ -> () in
	  loop fork_lines;
	  let l = Hashtbl.fold (fun k v r -> (!v,k)::r) tbl [] in
	  let l = List.rev(List.sort compare l) in
	  match l with
	    (_,a)::_ -> Some (Pids(a,!(Hashtbl.find ptbl a)))
	  | _ -> None
	end
    else None
  with _ -> None

(* comm=java pid=4470 child_comm=java child_pid=4498 *)

(* -------------------------------------------------------------------- *)
(* virtualization mappings *)

let guestid file =
  match Filename.extension file with
    ".dat" ->
      let cmd =
	Printf.sprintf "trace-cmd dump --options %s | grep -A1 \"Option TRACEID\" | tail -1" file in
      Some (List.hd(cmd_to_list cmd))
  | _ -> None

let core_mapping hostfile guestfile =
  match Filename.extension hostfile with
    ".dat" ->
      let tmp = Filename.temp_file (Filename.basename hostfile) ".opt" in
      ignore
	(sys_command
	   (Printf.sprintf "trace-cmd dump --options %s > %s" hostfile tmp));
      let i = open_in tmp in
      let readto str =
	let re = Str.regexp_string str in
	let rec loop _ =
	  let l = input_line i in
	  try
	    begin
	      ignore(Str.search_forward re l 0);
	      l
	    end
	  with
	    Not_found -> loop()
	  | End_of_file ->
	      failwith (Printf.sprintf "%s not found in .dat file" str) in
	loop() in
      let cpuline =
	match guestid guestfile with
	  Some id ->
	    ignore(readto id);
	    input_line i
	| None -> (* take the first mapping *)
	    readto "[Guest CPUs]" in
      let number_cpus =
	int_of_string (List.hd(String.split_on_char ' ' cpuline)) in
      let rec loop ct =
	let l = input_line i in
	let l = String.trim l in
	if l = "" || ct = 0
	then []
	else
	  match Str.split (Str.regexp " +") l with
	    core::pid::_ -> (ios 8 core,ios 9 pid) :: loop(ct-1)
	  | _ -> failwith ("core_mapping: bad line: "^l) in
      let infos = List.sort compare (loop number_cpus) in
      let rec check n = function
	  [] -> ()
	| (x,_)::xs ->
	    if x = n
	    then check (n+1) xs
	    else failwith "not all cores provided" in
      check 0 infos;
      Array.of_list(List.map snd infos)
  | _ -> failwith "need .dat file for host and guests"

(* ----------------------------------------------------- *)
(* Some other utility functions *)

(* reorder cores for socketorder *)
let reorder numcores c =
  let sockets =
    match numcores with
      160 -> 4
    | 128 -> 4
    | 64 -> 2
    | 20 | 36 (* gros *) -> 1
    | 96 -> 16 (* neowise *)
    | _ -> failwith "reorder: unknown number of cores" in
  let cores_per_socket = numcores / sockets in
  let socket_number = c mod sockets in
  let start = socket_number * cores_per_socket in
  let offset = c / sockets in
  start + offset

(* manage requested commands and pids *)
let update_requested_pids pid cmd requested_pids =
  if not(!O.cmds = [] && !O.pids = [] && not (Hashtbl.mem requested_pids pid))
  then
    begin
      let len = String.length cmd in
      let matches =
	List.exists
	  (fun (clen,c) ->
	    clen <= len && String.sub cmd 0 clen = c)
	  !O.cmds in
      if matches
      then Hashtbl.add requested_pids pid ()
    end

let check_requested_pids pid cmd requested_pids =
  if !O.cmds = [] && !O.pids = []
  then true
  else if Hashtbl.mem requested_pids pid
  then true
  else
    begin
      update_requested_pids pid cmd requested_pids;
      Hashtbl.mem requested_pids pid
    end
