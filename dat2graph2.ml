(* SPDX-License-Identifier: GPL-2.0 *)
(* ------------------------------------------------------------------------ *)
(* configuration options *)

module O = Options

let exec = ref false
let freq = ref false
let fast_freq = ref false
let freqgraph = ref false
let mfreq = ref false
let color_by_command = ref false
let color_by_top_command = ref false
let color_by_pid = ref false
let color_by_top_pid = ref false
let color_by_parent = ref false
let events = ref false
let waking_events = ref false
let waking_waker_events = ref false
let wakeup_events = ref false
let sockets = ref false
let showcores = ref false
let socket_order = ref false
let show = ref []
let target = ref []
let forked = ref false
let kvm = ref ""
let big = ref false
let find_cmd = ref false
let endoffset = ref None
let miny = ref 0.
let waking_edge = ref false
let ty = ref Printer.Jgraph

let numcores = ref 0
let minsleep = ref 0.0001

type 'a res =
    Open of float * float * 'a
  | Closed of float * float * 'a

let res2c = function
    Open(t0,tm,_) -> Printf.sprintf "open at time %f-%f\n" t0 tm
  | Closed(t0,tm,_) -> Printf.sprintf "closed for time %f-%f\n" t0 tm

let runtime s f =
  if !O.debug
  then
    begin
      let t = Sys.time() in
      let res = f() in
      let u = Sys.time() in
      Printf.eprintf "%s: %f\n" s (u -. t);
      flush stderr;
      res
    end
  else f()

(* ------------------------------------------------------------------------ *)

let tracking pid cmd requested_pids time firstmatch parents =
  let res =
    (not !color_by_parent || Hashtbl.mem parents pid) &&
    (pid > 0 && Util.check_requested_pids pid cmd requested_pids) in
  (if res && !O.cmds <> []
  then
    match !firstmatch with
      None -> firstmatch := Some time
    | _ -> ());
  res

let pid_transition oldpid cmd pid requested_pids =
  if Hashtbl.mem requested_pids oldpid
  then Hashtbl.add requested_pids pid ()
  else if !O.cmds <> []
  then Util.update_requested_pids pid cmd requested_pids

let iskvm s =
  let re = Str.regexp "CPU \\([0-9]+\\)/KVM" in
  if Str.string_match re s 0
  then
    let core = int_of_string(Str.matched_group 1 s) in
    Some core
  else None

type core = Core of int | Corepid of int

let get_core inkvm index core hoststate mapping =
  if inkvm
  then
    let corepid = Array.get (Array.get mapping (index-1)) core in
    try
      match Hashtbl.find hoststate corepid with
	Some core -> Core core
      | None -> Corepid corepid
    with Not_found -> Corepid corepid
  else Core core

let tmatches cmd =
  let len = String.length cmd in
  List.exists
    (fun (clen,c) ->
      clen <= len && String.sub cmd 0 clen = c)
    !target

(* ---------------------------------------------------------------------------- *)

let switchfrom_corestate corestate freqtrace time core startpoint v =
  Array.set corestate core v;
  if !freq
  then
    match Array.get freqtrace core with
      Open(t0,_,frq)::rest ->
	if fst frq < 0 (* no freq info yet *)
	then Array.set freqtrace core rest
	else
	  Array.set freqtrace core (Closed(max startpoint t0,time,frq)::rest)
    | _ -> ()

let switchfrom_guest_running base time core frompid fromcmd corestate freqtrace
    startpoint =
  if time < startpoint
  then
    begin
      Array.set corestate core [];
      if !freq then Array.set freqtrace core []
    end
  else
    (match Array.get corestate core with
      Open(t0,_,(pid,cmd))::rest ->
	if frompid = pid
	then
	  switchfrom_corestate corestate freqtrace time core startpoint
	    (Closed(t0,time,(frompid,fromcmd)) :: rest)
	else
	  failwith
	    (Printf.sprintf "1: %s: %f:%d: inconsistent action %d:%s -> %d:%s\n"
	       base time core pid cmd frompid fromcmd)
    | [] ->
	switchfrom_corestate corestate freqtrace time core startpoint
	  [Closed(startpoint,time,(frompid,fromcmd))]
    | x::_ ->
	failwith
	  (Printf.sprintf "2: %s: %f:%d: inconsistent action for pid %d: %s\n"
	     base time core frompid (res2c x)))

let switchfrom_guest_pending base time core frompid fromcmd pending
    startpoint =
  try
    match Hashtbl.find pending core with
      Open(t0,_,(pid,cmd))::rest ->
	if frompid = pid
	then Hashtbl.replace pending core rest
	else
	  failwith
	    (Printf.sprintf "3: %s: %f:%d: inconsistent action %d:%s -> %d:%s\n"
	       base time core pid cmd frompid fromcmd)
    | [] -> ()
    | x::_ ->
	failwith
	  (Printf.sprintf "4: %s: %f:%d: inconsistent action for pid %d: %s\n"
	     base time core frompid (res2c x))
  with Not_found -> ()

let switchfrom_host time core frompid fromcmd corestate freqtrace
    hoststate pending startpoint =
  try
    match Hashtbl.find hoststate frompid with
      Some pcpu ->
	Hashtbl.replace hoststate frompid None;
	(match Array.get corestate core with
	  Open(t0,_,v)::rest ->
	    switchfrom_corestate corestate freqtrace time pcpu startpoint
	      (Closed(t0,time,v)::rest);
	    let current_pending =
	      try Hashtbl.find pending frompid with _ -> [] in
	    Hashtbl.replace pending frompid (Open(time,time,v)::current_pending)
	| _ -> (* nothing here currently *) ())
    | None -> (* nothing running if there is no pcpu *) ()
  with Not_found -> (* not a cpu *) ()

let switchfrom base inkvm index time core frompid fromcmd corestate freqtrace
    hoststate pending mapping startpoint =
  if inkvm && index = 0
  then (* host *)
    switchfrom_host time core frompid fromcmd corestate freqtrace
      hoststate pending startpoint
  else (* guest or no virtualization *)
    match get_core inkvm index core hoststate mapping with
      Core core ->
	switchfrom_guest_running base time core frompid fromcmd corestate freqtrace
	  startpoint
    | Corepid corepid ->
	Printf.eprintf "%f: how can we be running if the vcpu has no core?\n"
	  time;
	switchfrom_guest_pending base time corepid frompid fromcmd pending
	  startpoint

(* ---------------------------------------------------------------------------- *)

let switchto_guest inkvm index time core topid tocmd corestate
    freqstate freqtrace first_appearance startpoint =
  (if !freq (* has to be before update of corestate! *)
  then
    let curfreq =
      let (curfreq,t0) = Array.get freqstate core in
      if time -. 0.004 <= t0 (* ensure the freq is not out of date *)
      then curfreq
      else (-1,-1) in
    let fail l =
      (match l with
	Open(t0,t1,(frqmin,frqmax))::_ ->
	  failwith
	    (Printf.sprintf "at %f, freqtrace already open, since %f: %d-%d\n"
	       time t0 frqmin frqmax)
      | _ -> ());
      Array.set freqtrace core (Open(time,time,curfreq) :: l) in
    (if !O.relaxed
    then
      match Array.get freqtrace core with
	Closed(t0,tm,oldfrq)::rest
	when curfreq = oldfrq &&  time -. tm < !minsleep ->
	  Array.set freqtrace core (Open(t0,time,oldfrq)::rest)
      | l -> fail l
    else fail (Array.get freqtrace core)));
  let fail l =
    (match l with
      Open(t0,_,(top,toc))::_ ->
	failwith
	  (Printf.sprintf
	     "previous open: previous %f - %f (%d,%s), now: %f (%d,%s)."
	     t0 time top toc time topid tocmd)
    | _ -> ());
    Array.set corestate core (Open(time,time,(topid,tocmd)) :: l) in
  (if !O.relaxed
  then
    match Array.get corestate core with
      Closed(t0,tm,(pid,cmd))::rest
      when pid = topid && cmd = tocmd && time -. tm < !minsleep ->
	Array.set corestate core (Open(t0,time,(pid,cmd))::rest)
    | l ->
      fail l
  else fail (Array.get corestate core));
  if !events && time >= startpoint
  then
    let entry = (topid,tocmd) in
    (if not (Hashtbl.mem first_appearance entry)
    then
      Hashtbl.add first_appearance entry (core,time)
    else
      let (_,t) = Hashtbl.find first_appearance entry in
      if time < t then
        Hashtbl.replace first_appearance entry (core,time))

let switchto_host inkvm index time core topid tocmd corestate
    freqstate freqtrace hoststate pending
    first_appearance startpoint =
  try
    match Hashtbl.find hoststate topid with
      None ->
	Hashtbl.replace hoststate topid (Some core);
	(try
	  (match Hashtbl.find pending topid with
	    Open(t0,_,v)::rest ->
	      Hashtbl.replace pending topid (Closed(t0,time,v)::rest);
	      Array.set corestate core
		(Open(time,time,v)::(Array.get corestate core))
	  | _ -> ())
	with Not_found -> ())
    | Some _ -> failwith "switchto when running"
  with Not_found -> (* not a cpu *) ()

let switchto inkvm index time core topid tocmd corestate
    freqstate freqtrace hoststate pending mapping
    first_appearance startpoint =
  if inkvm && index = 0
  then (* host *)
    switchto_host inkvm index time core topid tocmd corestate
      freqstate freqtrace hoststate pending
      first_appearance startpoint
  else (* guest or no virtualization *)
    match get_core inkvm index core hoststate mapping with
      Core core ->
	switchto_guest inkvm index time core topid tocmd corestate
	  freqstate freqtrace first_appearance startpoint
    | Corepid corepid ->
	Printf.eprintf "%f: how can we be running if the vcpu has no core?\n"
	  time;
	let current_pending =
	  try Hashtbl.find pending corepid with _ -> [] in
	Hashtbl.replace pending corepid
	  (Open(time,time,(topid,tocmd))::current_pending)

let freq_translate machine_info frq =
  let cls =
    List.fold_left (* normalize to the top end of the interval *)
      (fun c1 (_,_,low,up,c2) -> if frq > low && frq <= up then up else c1)
      frq
      (snd machine_info) in
  if !freqgraph
  then (frq,cls)
  else (cls,cls)

let record_event inkvm index time core cmd pid l hoststate mapping
    sched_events =
  match get_core inkvm index core hoststate mapping with
    Core core ->
      sched_events := (time,core,cmd,pid,l) :: !sched_events
  | _ -> ()

let process_line base inkvm index time core
    (corestate,freqstate,freqtrace,hoststate,pending,mapping,sched_events,
     requested_pids,first_appearance,machine_info,firstmatch,wakings,waking_edges,parents) startpoint l =
  (match !firstmatch, !endoffset with
    Some x, Some y -> if time > x +. y then raise End_of_file
  | _ -> ());
  match l with
    Parse_line.Sched_switch(fromcmd,frompid,reason,tocmd,topid) ->
      (if not !fast_freq && tracking frompid fromcmd requested_pids time firstmatch parents
      then
	switchfrom base inkvm index time core frompid fromcmd corestate
	  freqtrace hoststate pending mapping
	  startpoint);
      (if not !fast_freq && tracking topid tocmd requested_pids time firstmatch parents
      then
	switchto inkvm index time core topid tocmd corestate
	  freqstate freqtrace hoststate pending mapping
	  first_appearance startpoint);
  | Parse_line.Sched_waking(actor_cmd,actor_pid,cmd,pid,cpu,interrupt) ->
      let (relevant_pid,relevant_cmd) =
        if !waking_waker_events then (actor_pid,actor_cmd) else (pid,cmd) in
      if (!events || !waking_events) && time >= startpoint &&
        tracking relevant_pid relevant_cmd requested_pids time firstmatch parents &&
	(not inkvm || index > 0)
      then
	begin
	  record_event inkvm index time core cmd relevant_pid
	    l hoststate mapping sched_events;
	  if !waking_edge
	  then Hashtbl.replace wakings pid (time,actor_pid,core)
	end
  | Parse_line.Sched_wakeup(cmd,pid,prevcpu,cpu) ->
      if (!events || !wakeup_events) && time >= startpoint && tracking pid cmd requested_pids time firstmatch parents &&
	(not inkvm || index > 0)
      then
        begin
	  record_event inkvm index time core cmd pid l hoststate mapping
	    sched_events;
	  if !waking_edge
	  then
	    try
	      let (wtime,actor_pid,wcore) = Hashtbl.find wakings pid in
	      waking_edges := (pid,time,cpu,wtime,actor_pid,wcore) :: !waking_edges;
	      Hashtbl.remove wakings pid
	    with Not_found -> ()
	end
  | Parse_line.Sched_wakeup_new(cmd,pid,parent,cpu) ->
      if !events && time >= startpoint && tracking pid cmd requested_pids time firstmatch parents &&
	(not inkvm || index > 0)
      then
	record_event inkvm index time core cmd pid l hoststate mapping
	  sched_events
  | Parse_line.Sched_process_exec(cmd,oldcmd,pid,oldpid) ->
      (if !color_by_parent
      then Hashtbl.add parents pid oldpid);
      (if tracking oldpid oldcmd requested_pids time firstmatch parents
      then (* pid as is before exec *)
	switchfrom base inkvm index time core oldpid oldcmd corestate
	  freqtrace hoststate pending mapping startpoint);
      (if !forked
      then Hashtbl.add requested_pids pid ()
      else pid_transition oldpid cmd pid requested_pids);
      if tracking pid cmd requested_pids time firstmatch parents
      then
	switchto inkvm index time core pid cmd corestate
	  freqstate freqtrace hoststate pending mapping
	  first_appearance startpoint
  | Parse_line.Sched_process_fork(_,parent_pid,cmd,pid) ->
      (if !color_by_parent
      then
	try
	  let p = Hashtbl.find parents parent_pid in
	  Hashtbl.add parents pid p
	with _ -> ());
      (if !forked
      then Hashtbl.add requested_pids pid ()
      else pid_transition parent_pid cmd pid requested_pids);
      if !events && tracking pid cmd requested_pids time firstmatch parents && time >= startpoint &&
	(not inkvm || index > 0)
      then
	record_event inkvm index time core cmd pid l hoststate mapping
	  sched_events
  | Parse_line.Sched_migrate_task(cmd,pid,oldcpu,newcpu,state) when !events ->
      if time >= startpoint && tracking pid cmd requested_pids time firstmatch parents &&
	(not inkvm || index > 0)
      then
	record_event inkvm index time core cmd pid l hoststate mapping
	  sched_events
  | Parse_line.Sched_wake_idle_without_ipi cpu when !events ->
      if time >= startpoint && (not inkvm || index > 0)
      then
	record_event inkvm index time core "" cpu l hoststate mapping
	  sched_events
  | Parse_line.Sched_tick(frq) when !fast_freq -> (* freq by tick *)
      let frq = freq_translate machine_info frq in
      Array.set freqstate core (frq,time);
      if time >= startpoint
      then
	begin
	  match Array.get freqtrace core with
	    [] -> Array.set freqtrace core [Open(time,time,frq)]
	  | Open(t0,tm,oldfreq) :: rest ->
	      if tm +. 0.00425 < time || frq <> oldfreq (* gap *)
	      then
		Array.set freqtrace core
		  (Open(time,time,frq) :: Closed(t0,tm +. 0.004,oldfreq) ::
		   rest)
	      else Array.set freqtrace core (Open(t0,time,oldfreq) :: rest)
	  | _ -> failwith "not possible 1"
	end
  | Parse_line.Sched_tick(frq) when !freq ->
      let frq = freq_translate machine_info frq in
      Array.set freqstate core (frq,time);
      if time >= startpoint
      then
	begin
	  match Array.get freqtrace core with
	    Open(t0,_,oldfreq) :: rest when frq <> oldfreq ->
	      if fst oldfreq < 0
	      then Array.set freqtrace core (Open(time,time,frq) :: rest)
	      else
		Array.set freqtrace core
		  (Open(time,time,frq) :: Closed(t0,time,oldfreq) :: rest)
	  | _ -> ()
	end
  | _ -> ()

let parse_all inkvm files startpoint endpoint events mapping =
  (* just take info from the host *)
  let cores = (List.hd files).Util.cores in
  numcores := cores;
  let machine_info =
    let file = (List.hd files).Util.text_file_name in
    fst(Machines.get_freqtbl cores file) in
  let corestate = Array.make cores [] in
  let freqstate = Array.make cores ((-1,-1),0.0) in
  let freqtrace = Array.make cores [] in
  let hoststate = Hashtbl.create 101 in
  Array.iter (Array.iter (fun pid -> Hashtbl.add hoststate pid None)) mapping;
  let pending = Hashtbl.create 101 in
  let sched_events = ref [] in
  let requested_pids = Hashtbl.create 101 in
  List.iter (fun pid -> Hashtbl.add requested_pids pid ()) !O.pids;
  let parents = Hashtbl.create 101 in
  (*child pid -> (first core,time)*)
  let first_appearance = Hashtbl.create 101 in
  let firstmatch = ref None in
  let wakings = Hashtbl.create 101 in
  let waking_edges = ref [] in
  Util.iparse_loop files startpoint endpoint events
    (corestate,freqstate,freqtrace,hoststate,pending,mapping,sched_events,
     requested_pids,first_appearance,machine_info,firstmatch,wakings,waking_edges,
     parents)
    (process_line
       (String.concat "---" (List.map (fun ctx -> ctx.Util.text_file_name) files))
       inkvm)

(* ------------------------------------------------------------------------ *)
(* helpers *)

let close_everything ar endpoint check =
  Array.iteri
    (fun i ->
      function
	  Open(t0,_,v)::rest ->
	    if check v
	    then Array.set ar i (Closed(t0,endpoint,v)::rest)
	    else Array.set ar i rest
	| _ -> ())
    ar

let close_everything_hash tbl endpoint check =
  Hashtbl.iter
    (fun pid ->
      function
	  Open(t0,_,v)::rest ->
	    if check v
	    then Hashtbl.replace tbl pid (Closed(t0,endpoint,v)::rest)
	    else Hashtbl.replace tbl pid rest
	| _ -> ())
    tbl

let reorder c =
  if !socket_order && not !showcores && not !sockets(*no point in these cases*)
  then Util.reorder !numcores c
  else c

let start_graph ysize xlabel ylabel base file start startpoint endpoint
    duration =
  let file = String.concat "_" file in
  let base = String.concat " " base in
  let xlabel =
    Printf.sprintf "%s%s, duration: %f seconds%s%s%s\\\n\n"
      base
      (if !O.cmds = []
      then ""
      else Printf.sprintf " (%s)" (String.concat ", " (List.map snd !O.cmds)))
      duration xlabel
      (if !O.relaxed then ", relaxed" else "")
      (if !color_by_top_command then ", top 25 commands" else "") in
  let (ymin,ymax) =
    if !showcores || !sockets
    then (None,Printer.Nomax)
    else (Some 0,Printer.Ymax (float_of_int !numcores)) in
  Printer.startgraph file !ty start
    (if !big then 80. else !O.standard_xsize)
    (Some startpoint) (Some endpoint)
    xlabel ysize ymin ymax ylabel

(* ------------------------------------------------------------------------ *)
(* color by command: assign colors to commands, collect stats,
   collect interesting command list *)

let normalize_memo = Hashtbl.create 101
let normalize cmd = (* replace numbers by X *)
  try Hashtbl.find normalize_memo cmd
  with Not_found ->
    begin
      let cmd =
	(*try String.sub cmd 0 (String.index cmd '/')
	with Not_found ->*) cmd in
      let normalizable s =
	not(List.exists (fun (_,c) -> s = c) !O.cmds) &&
	(let res = ref false in
	String.iter (fun c -> if '0' <= c && c <= '9' then res := true) s;
	!res) in
      let res =
	if normalizable cmd && not(tmatches cmd)
	then
	  let pieces = Str.split_delim (Str.regexp "[0-9]+") cmd in
	  String.concat "X" pieces
	else cmd in
      Hashtbl.add normalize_memo cmd res;
      res
    end
      
let command_colors = Hashtbl.create 11
let used_colors = Hashtbl.create 11

(* collect number of activations, duration of execution, first and last execution time *)
let command_count ar1 iterator1 ar2 iterator2 need_colors parents =
  let tbl = Hashtbl.create 101 in
  let collect ar iterator =
    iterator
      (fun core line ->
      	List.iter
	  (function
	      Closed(starttime,endtime,(pid,cmd)) ->
	      	let cmd =
		  if !color_by_pid || !color_by_top_pid
		  then string_of_int pid
		  else if !color_by_parent
		  then string_of_int (Hashtbl.find parents pid)
		  else normalize cmd in
		(try
		  let (mintime,maxtime,count,duration,pids,cores) =
		    Hashtbl.find tbl cmd in
		  mintime := min !mintime starttime;
		  maxtime := max !maxtime endtime;
		  count := !count + 1;
		  duration := !duration +. (endtime -. starttime);
		  (if not(List.mem pid !pids) then pids := pid :: !pids);
		  (if List.length !cores < 4 && not (List.mem core !cores)
		  then cores := core :: !cores)
		with Not_found ->
		  Hashtbl.add tbl cmd
		    (ref starttime,ref endtime,ref 1,
		     ref (endtime -. starttime),
		     ref [pid], ref [core]))
	    | r -> failwith (Printf.sprintf "not possible 2: %s" (res2c r)))
	  line (*backwards*))
      ar in
  collect ar1 iterator1;
  collect ar2 iterator2;
  let clist =
    Hashtbl.fold
      (fun cmd (mintime,maxtime,count,duration,pids,cores) r ->
	(!duration,cmd) :: r)
      tbl [] in
  let clist = List.rev(List.sort compare clist) in
  let clist =
    if !color_by_top_command || !color_by_top_pid
    then Util.take 25 clist
    else clist in
  let clist =
    if !O.nograph
    then
      List.map
	(fun (dur,cmd) -> (dur,cmd,Printer.RGB(0.0,0.0,1.0)))
	clist
    else
      let rec have_colors have_color need_color colors = function
	  (dur,cmd) :: rest ->
	    let c =
	      try Some (Hashtbl.find command_colors cmd)
	      with Not_found -> None in
	    (match c with
	      Some c ->
		have_colors ((dur,cmd,c) :: have_color) need_color (c::colors) rest
	    | None -> have_colors have_color ((dur,cmd) :: need_color) colors rest)
	| [] -> (List.rev have_color,List.rev need_color,colors) in
      let (have_color,need_color,colors) = have_colors [] [] [] clist in
      let available_colors =
	List.filter
	  (function c -> not(List.mem c colors))
	  (Array.to_list (snd !(Printer.colors))) in
      let rec assign_colors colors = function
	  (dur,cmd) :: rest ->
	    (match colors with
	      [] -> (dur,cmd,Printer.Color "black") :: assign_colors colors rest
	    | c::cs ->
		(if not(Hashtbl.mem used_colors c)
		then
		  begin
		    Hashtbl.add used_colors c ();
		    Hashtbl.add command_colors cmd c
		  end);
		(dur,cmd,c) :: assign_colors cs rest)
	| [] -> [] in
      List.rev
	(List.sort compare
	   (have_color @ assign_colors available_colors need_color)) in
  List.map
    (function (dur,cmd,color) ->
      (cmd,(color,Hashtbl.find tbl cmd)))
    clist

(* ------------------------------------------------------------------------ *)
(* managing events *)

type event_counts =
    { sched_switch : int ref;
      sched_waking : int ref;
      sched_wakeup : int ref;
      sched_wakeup_idle_without_ipi : int ref;
      sched_wake_migrate_task : int ref;
      sched_lb_migrate_task : int ref;
      sched_numa_migrate_task : int ref }

let count_events events =
  let event_counts =
    { sched_switch = ref 0;
      sched_waking = ref 0;
      sched_wakeup = ref 0;
      sched_wakeup_idle_without_ipi = ref 0;
      sched_wake_migrate_task = ref 0;
      sched_lb_migrate_task = ref 0;
      sched_numa_migrate_task = ref 0 } in
  let inc x = x := !x + 1 in
  List.iter
    (function
	(time,core,cmd,pid,
	 Parse_line.Sched_switch(fromcmd,frompid,reason,tocmd,topid))
	when reason = Parse_line.Block 'D' ->
	  inc event_counts.sched_switch
      | (time,core,cmd,pid,Parse_line.Sched_waking _) ->
	  inc event_counts.sched_waking
      | (time,core,cmd,pid,Parse_line.Sched_wakeup(_,_,prevcpu,cpu)) ->
	  inc event_counts.sched_wakeup
      | (time,core,cmd,pid,Parse_line.Sched_wake_idle_without_ipi cpu) ->
	  inc event_counts.sched_wakeup_idle_without_ipi
      | (time,core,cmd,pid,
	 Parse_line.Sched_migrate_task(_,_,oldcpu,newcpu,state)) ->
	  (match state with
	    Parse_line.Waking _ -> inc event_counts.sched_wake_migrate_task
	  | Parse_line.Woken -> inc event_counts.sched_lb_migrate_task
	  | Parse_line.Numa _ -> inc event_counts.sched_numa_migrate_task)
      | _ -> ())
    events;
  event_counts

let evt_to_color pid evt =
  match evt with
    Parse_line.Sched_wakeup(_,_,_,_) ->         Printer.Choose 0 (* blue *)
  | Parse_line.Sched_wakeup_new(_,_,_,_) ->     Printer.Choose 4 (* yellow *)
  | Parse_line.Sched_waking(actor_cmd,actor_pid,cmd,_pid,cpu,_)
    when pid = actor_pid ->
      Printer.Choose 2 (* red *)
  | Parse_line.Sched_waking(actor_cmd,actor_pid,cmd,pid,cpu,_) ->
      Printer.Choose 8 (* ??? *)
  | Parse_line.Sched_process_fork(_,_,_,_) ->   Printer.Choose 3 (* purple *)
  | Parse_line.Sched_wake_idle_without_ipi _ -> Printer.Choose 1 (* green *)
  | Parse_line.Sched_migrate_task(_,_,_,_,_) -> Printer.Choose 5 (* cyan *)
  | Parse_line.Sched_switch(_,_,_,_,_) ->       Printer.Choose 6 (* pale blue*)
  | Parse_line.Not_supported _ ->               Printer.Choose 7(*deep purple*)
  | _ ->                                        Printer.Choose 9

let evt_to_string pid evt event_counts =
  match evt with
    Parse_line.Sched_wakeup(_,_,_,_) ->
      Printf.sprintf "wakeup (%d)" !(event_counts.sched_wakeup)
  | Parse_line.Sched_wakeup_new(_,_,_,_) ->     "wakeup_new"
  | Parse_line.Sched_waking(actor_cmd,actor_pid,cmd,_pid,cpu,_)
    when pid = actor_pid ->
      Printf.sprintf "waking (waker, %d)" !(event_counts.sched_waking)
  | Parse_line.Sched_waking(actor_cmd,actor_pid,cmd,pid,cpu,_) ->
      Printf.sprintf "waking (wakee, %d)" !(event_counts.sched_waking)
  | Parse_line.Sched_process_fork(_,_,_,_) ->   "fork"
  | Parse_line.Sched_wake_idle_without_ipi _ ->
      Printf.sprintf "wake_idle_without_ipi (%d)"
	!(event_counts.sched_wakeup_idle_without_ipi)
  | Parse_line.Sched_migrate_task(_,_,_,_,_) ->
      Printf.sprintf "migrate_task \\\n(wake %d, lb %d%s)"
	!(event_counts.sched_wake_migrate_task)
	!(event_counts.sched_lb_migrate_task)
	(if !(event_counts.sched_numa_migrate_task) > 0
	then
	  (Printf.sprintf ", numa %d" !(event_counts.sched_numa_migrate_task))
	else "")
  | Parse_line.Sched_switch(_,_,_,_,_) ->
      (Printf.sprintf "i/o sleep (%d)" !(event_counts.sched_switch))
  | Parse_line.Sched_swap_numa _ -> "numa migration"
  | Parse_line.Sched_move_numa _ -> "numa migration"
  | Parse_line.Sched_stick_numa _ -> "numa migration"
  | Parse_line.Not_supported _ ->               "unknown"
  | _ ->                                        "unknown"

let migrate_color oldcpu newcpu skfn = function
    Parse_line.Waking _ ->
      if skfn oldcpu = skfn newcpu
      then Printer.RGB(1.0,0.6,0.8)
      else Printer.RGB(0.0,0.75,1.0)
  | Parse_line.Woken    ->
      if skfn oldcpu = skfn newcpu
      then Printer.RGB(1.0,0.8,0.0)
      else Printer.RGB(0.0,0.46,0.0)
  | Parse_line.Numa _   -> Printer.RGB(0.2,0.6,0.4)

let make_event_graph first_appearance sched_events notch
    yfn yfn2 yfn3 skfn waking_edges go =
  let event_counts = count_events sched_events in
  List.iter
    (fun (time,core,cmd,pid,l) ->
      let core =
	match l with
	  Parse_line.Sched_wakeup_new(cmd,pid,parent,cpu) -> cpu
	| _ -> core in
      notch go [(time,yfn core pid)] (evt_to_color pid l)
	(Printer.Label (evt_to_string pid l event_counts))
	(Some (Printf.sprintf "event: %s" cmd));
      match l with
	Parse_line.Sched_process_fork(_,_,child_cmd,child_pid) ->
	  (try
	    let (child_core,child_time) =
	      Hashtbl.find first_appearance (child_pid,child_cmd) in
	    Printer.dots go
	      (List.map (fun c -> (time,c)) (yfn2 core child_core))
	      (Printer.Color "black") (Printer.Label "process fork")
	      (if !O.save_tmp
	      then
		Some
		  (Printf.sprintf "child_pid: %d child_core: %d child_time: %f"
		     child_pid child_core child_time)
	      else None)
	  with _ -> ())
      | Parse_line.Sched_migrate_task(cmd,mpid,oldcpu,newcpu,state) ->
	  let message =
	    Printer.Label
	      (match state with
		Parse_line.Woken ->
		  if skfn oldcpu = skfn newcpu
		  then "on-socket \\\nload balance"
		  else "off-socket \\\nload balance"
	      | Parse_line.Waking _ ->
		  if skfn oldcpu = skfn newcpu
		  then "on-socket \\\nunblock placement"
		  else "off-socket \\\nunblock placement"
	      | Parse_line.Numa _ -> "numa balancing") in
	  let comment =
	    Some(Printf.sprintf "migrate pid %d : core %d -> core %d on core %d"
		   mpid oldcpu newcpu core) in
	  Printer.arrow go (List.map (fun c -> (time,c)) (yfn3 oldcpu newcpu pid))
	    (migrate_color oldcpu newcpu skfn state) message comment
      | _ -> ())
    sched_events;
    List.iter
      (fun (pid,time,core,wtime,actor_pid,wcore) ->
        Printer.arrow go [(wtime,yfn wcore actor_pid);(time,yfn core pid)]
          (Printer.Color "red") (Printer.Label "wakeup")
	  (Some
	    (Printf.sprintf "waker: %d, waking time: %f, pid: %d, wakeup time: %f\n"
	      actor_pid wtime pid time)))
      waking_edges

(* ------------------------------------------------------------------------ *)
(* managing execution traces *)

type rev = Rev | Normal | NoSort
type sortcolor = Float of float | Int of int | String of string

let black = Printer.Color "black"

let corestate_iterate_other go corestate get_color get_y =
  Array.iteri
    (fun core line ->
      let core = reorder core in
      List.iter
	(function
	    Closed(t0,tm,(pid,cmd)) ->
	      let color =
	      	if !target = [] || tmatches cmd
		then
		  try let (index,color,label) = get_color core pid cmd in color
		  with _ -> black
		else black in
	      let y = get_y core pid in
	      Printer.edge go [(t0,y);(tm,y)] color Printer.NoLabel
		(if !O.save_tmp then (Some (Printf.sprintf "%s:%d" cmd pid)) else None)
	  | x -> failwith (Printf.sprintf "not possible 3: %s" (res2c x)))
	(List.rev line))
    corestate

let corestate_iterate_implot go corestate get_color get_y rev =
  let tbl = Hashtbl.create 101 in
  Array.iteri
    (fun core line ->
      let core = reorder core in
      List.iter
	(function
	    Closed(t0,tm,(pid,cmd)) ->
	      let color =
	      	if !target = [] || tmatches cmd
		then
		  try get_color core pid cmd
		  with _ -> (Int 0,black,Printer.NoLabel)
		else (Int 0,black,Printer.NoLabel) in
	      let y = get_y core pid in
	      Hashtbl.replace tbl color
		((t0,y)::(tm,y)::(try Hashtbl.find tbl color with _ -> []))
	  | x -> failwith (Printf.sprintf "not possible 3: %s" (res2c x)))
	(List.rev line))
    corestate;
  let entries = Hashtbl.fold (fun color line r -> (color,List.rev line)::r) tbl [] in
  List.iter
    (fun ((_,color,label), line) -> Printer.segments go line color label None)
    (match rev with
      Rev -> List.rev(List.sort compare entries)
    | Normal -> List.sort compare entries
    | NoSort -> entries)

let corestate_iterate go corestate get_color get_y rev =
  match !ty with
    Printer.ImPlot _ -> corestate_iterate_implot go corestate get_color get_y rev
  | _ -> corestate_iterate_other go corestate get_color get_y

let process_color_info go start (cmd,(color,(mintime,maxtime,count,duration,pids,cores))) =
  let duration2c d =
    if d > 1.
    then Printf.sprintf "%0.2f" d
    else Printf.sprintf "%0.4f" d in
  let dur = !duration in
  let duration = duration2c dur in
  let count = string_of_int !count in
  let lbl =
    Printf.sprintf "%s: %0.4f-%0.4f: %s (%s, %d pids)%s"
      cmd (!mintime -. start) (!maxtime -. start)
      duration count (List.length !pids)
      (if List.length !cores > 3
      then ""
      else
	let cores = List.sort compare (List.map reorder !cores) in
	" "^(String.concat ", " (List.map string_of_int cores))) in
  (match !ty with
    Printer.ImPlot _ -> ()
  | _ -> Printer.edge go [] color (Printer.Label lbl) None);
  (dur,lbl)

let make_execution_text corestate pending first_appearance sched_events
    base startpoint endpoint start last_time duration xlabel parents =
  let base = String.concat "_" base in
  let onm =
    if !O.outdir = ""
    then Printf.sprintf "%s.exec" base
    else Printf.sprintf "%s/%s.exec" !O.outdir (Filename.basename base) in
  let o = open_out onm in
  let infos =
    command_count corestate Array.iteri pending Hashtbl.iter false parents in
  Printf.fprintf o "cmd,count,duration,pids\n";
  List.iter
    (function (cmd,(color,(mintime,maxtime,count,duration,pids,cores))) ->
      Printf.fprintf o "%s,%d,%f,%d\n" cmd !count !duration (List.length !pids))
    infos;
  close_out o

let make_execution_graph corestate pending first_appearance sched_events waking_edges
    base startpoint endpoint start last_time duration xlabel parents =
  let ylabel =
    if !socket_order
    then "core (socket order)"
    else "core" in
  let go =
    start_graph !O.standard_ysize xlabel ylabel base base start
      startpoint endpoint duration in
  runtime ("choose colors 1 "^(String.concat "_" base))
    (fun _ ->
      let (rev,get_color) =
	if !color_by_command || !color_by_top_command || !color_by_pid || !color_by_top_pid || !color_by_parent
	then
	  begin
	    let infos =
	      command_count corestate Array.iteri pending Hashtbl.iter true parents in
	    let labels =
	      let rec loop = function
		  [] -> []
		| ((cmd,_) as x)::xs -> (* order matters *)
		    let lbl = process_color_info go start x in
		    (cmd,lbl) :: loop xs in
	      loop infos in
	    let get_color =
		match !ty with
		  ImPlot _ ->
		    (Rev,
		     fun core pid cmd ->
		      let cmd =
			if !color_by_pid || !color_by_top_pid
			then string_of_int pid
			else if !color_by_parent
			then string_of_int (Hashtbl.find parents pid)
			else normalize cmd in
		      let (dur,lbl) = List.assoc cmd labels in
		      (Float dur, Printer.SChoose lbl,Printer.Label lbl))
		| _ ->
		    (NoSort,
		     fun core pid cmd ->
		      let cmd =
			if !color_by_pid || !color_by_top_pid
			then string_of_int pid
			else if !color_by_parent
			then string_of_int (Hashtbl.find parents pid)
			else normalize cmd in
		      (Int 0,fst(List.assoc cmd infos),Printer.NoLabel)) in
	    get_color
          end
	else
	  let get_color core pid cmd =
	    (Int pid, Printer.Choose pid, Printer.Label(Printf.sprintf "%d: %s" pid cmd)) in
          (Rev,get_color) in
      let get_y core pid = core in
      corestate_iterate go corestate get_color get_y rev);
  (if !events || !waking_events || !wakeup_events
  then
    make_event_graph first_appearance sched_events Printer.notch
      (fun core pid -> reorder core)
      (fun core childcore -> [reorder core;reorder childcore])
      (fun oldcore newcore pid -> [reorder oldcore;reorder newcore])
      (snd(Machines.get_freqtbl !numcores (String.concat " " base))) waking_edges
      go);
  Printer.endgraph go

let make_core_graph corestate pending first_appearance sched_events waking_edges
    base startpoint endpoint start last_time duration xlabel parents =
  let xlabel =
    Printf.sprintf ", %s%s" (if !sockets then "socket" else "core") xlabel in
  let go =
    start_graph !O.standard_ysize xlabel "pid" base base start
      startpoint endpoint duration in
  let (rev,get_color) =
    if !color_by_command || !color_by_top_command || !color_by_pid || !color_by_top_pid || !color_by_parent
    then failwith "color by command etc not compatible with the --cores option"
    else
      (Normal,
       if !sockets
       then
	 let skfn = snd(Machines.get_freqtbl !numcores (String.concat " " base)) in
	 (fun core pid cmd ->
	   let soc = skfn core in
	   (Int soc,Printer.Choose soc,Printer.NoLabel))
       else (fun core pid cmd -> (Int core,Printer.Choose core,Printer.NoLabel))) in
  let get_y core pid = pid in
  runtime ("choose colors 2 "^(String.concat "_" base))
    (fun _ ->
      corestate_iterate go corestate get_color get_y rev);
  (if !events || !waking_events || !wakeup_events
  then
    make_event_graph first_appearance sched_events Printer.smallnotch
      (fun core pid -> pid)
      (fun core childcore -> [])
      (fun oldcore newcore pid -> [pid])
      (snd(Machines.get_freqtbl !numcores (String.concat " " base))) waking_edges
      go);
  Printer.endgraph go

(* ------------------------------------------------------------------------ *)
(* managing frequency traces *)

(* collect number of activations, duration of execution, first and last
   execution time *)
let freq_count freqtrace =
  let running_time = ref 0.0 in
  let tbl = Hashtbl.create 101 in
  Array.iter
    (List.iter
       (function
	   Closed(starttime,endtime,(_,frq)) ->
	     let len = endtime -. starttime in
	     running_time := !running_time +. len;
	     (try
	       let duration = Hashtbl.find tbl frq in
	       duration := !duration +. len
	     with Not_found ->
	       Hashtbl.add tbl frq (ref len))
	 | _ -> failwith "freq_count: not closed"))
    freqtrace;
  (tbl,!running_time)

let make_freq_graph machine_info freqtrace (base : string list)
    startpoint endpoint start last_time duration xlabel =
  let xlabel =
    if !fast_freq
    then ", by interval"^xlabel
    else xlabel in
  let ylabel =
    if !socket_order
    then "core (socket order)"
    else "core" in
  let go =
    start_graph !O.standard_ysize xlabel ylabel base base start
      startpoint endpoint duration in
  let (tbl,running_time) = freq_count freqtrace in
  List.iter
    (function (low,up,_,realup,c) -> (*up is the representative, so tbl index*)
      let duration = try !(Hashtbl.find tbl realup) with _ -> 0. in
      let lbl =
	Printf.sprintf "(%0.1f %0.1f] GHz (%0.2f = %0.2f%%)"
	  (float_of_int low /. 1000000.) (float_of_int up /. 1000000.)
	  duration ((duration *. 100.) /. running_time) in
      Printer.edge go [] c (Printer.Label lbl) None)
    (snd machine_info);
  Array.iteri
    (fun core line ->
      let core = reorder core in
      List.iter
	(function
	    Closed(t0,tm,(frq,_)) ->
	      let color = Machines.freq_to_color frq machine_info in
	      Printer.edge go [(t0,core);(tm,core)] color Printer.NoLabel None
	  | _ -> failwith "not possible 7")
	(List.rev line))
    freqtrace;
  Printer.endgraph go

let make_freq_text machine_info freqtrace base =
  let onm =
    let base = List.filter (fun x -> x <> "sockorder") base in
    let base = String.concat "_" base in
    if !O.outdir = ""
    then Printf.sprintf "%s.freq" base
    else Printf.sprintf "%s/%s.freq" !O.outdir (Filename.basename base) in
  let o = open_out onm in
  Printf.fprintf o "low,high,count,pct\n";
  let (tbl,running_time) = freq_count freqtrace in
  List.iter
    (function (_,_,low,up,c) -> (* up is the representative, so tbl index *)
      let duration = try !(Hashtbl.find tbl up) with _ -> 0. in
      Printf.fprintf o "%d,%d,%f,%f\n" low up duration
	((duration *. 100.) /. running_time))
    (snd machine_info);
  close_out o

let make_mfreq_graph = ()

let make_freqgraph_graph machine_info freqtrace base startpoint endpoint
    start last_time duration xlabel =
  let xlabel =
    Printf.sprintf "%s%s, duration: %f, time (seconds)%s"
      (String.concat " " base)
      (if !O.cmds = []
      then ""
      else Printf.sprintf " (%s)" (String.concat ", " (List.map snd !O.cmds)))
      (last_time -. start) (", freq"^xlabel) in
  let mf = (float_of_int (fst machine_info)) /. 1000000. in
  let go =
    Printer.startgraph (String.concat "_" base) !ty start
      (if !big then 80. else !O.standard_xsize)
      (Some startpoint) (Some endpoint) xlabel !O.standard_ysize
      (Some 2) (Printer.Yminmax (!miny, mf)) "frequency" in
  let (tbl,running_time) = freq_count freqtrace in
  List.iter
    (function (low,up,_,realup,c) ->
      let duration = try !(Hashtbl.find tbl realup) with _ -> 0. in
      let lf = (float_of_int low) /. 1000000. in
      if low > 0
      then
	let data = [(startpoint+.start,lf);(endpoint+.start,lf)] in
	Printer.fdots go data c
	  (Printer.Label
	     (Printf.sprintf "(%d.%d %d.%d] GHz (%0.2f = %0.2f%%)"
		(low/1000000) ((low/100000) mod 10) (up/1000000)
		((up/100000) mod 10) duration
		((duration *. 100.) /. running_time)))
	  None
      else ())
    (snd machine_info);
  Array.iteri
    (fun core line (*line is reversed*) ->
      let rec loop cur all = function
	  [] ->
	    if cur = []
	    then all
	    else cur :: all
	| [Closed(t0,tm,(frq,_))] ->
	    let ffreq = (float_of_int frq) /. 1000000. in
	    ((t0,ffreq)::(tm,ffreq)::cur)::all
	| Closed(t0,tm,(frq,_)) :: ((Closed(prevt0,prevtm,_)::_) as rest) ->
	    let ffreq = (float_of_int frq) /. 1000000. in
	    if t0 = prevtm
	    then loop ((t0,ffreq)::(tm,ffreq)::cur) all rest
	    else loop [] (((t0,ffreq)::(tm,ffreq)::cur)::all) rest
	| _ -> failwith "unexpected" in
      let lines = loop [] [] line in
      List.iter
	(fun line ->
	  Printer.fedge go line (Printer.Choose core) Printer.NoLabel None)
	lines)
    freqtrace;
  Printer.endgraph go

(* ------------------------------------------------------------------------ *)
(* main function *)

let main elements mapping files =
  let inkvm = !kvm <> "" in
  let ctx =
    let (base,ctx) = List.hd files in
    ctx in
  let base =
    let (base,ctx) = List.hd files in
    if !O.modifiers = []
    then [base]
    else
      let endstr = List.sort compare !O.modifiers in
      base :: endstr in
  let (success,start,last,
       (corestate,freqstate,freqtrace,hoststate,pending,mapping,
	sched_events,requested_pids,first_appearance,machine_info,
	firstmatch,wakings,waking_edges,parents)) =
    runtime ("parse_all "^(String.concat "_" base))
      (fun _ ->
	try
	  let files = List.map snd files in
	  parse_all inkvm files !O.min !O.max elements mapping
	with e ->
	  begin
	    List.iter (fun (_base,ctx) -> Util.remove 1 ctx) files;
	    raise e
	  end) in
  let last_time =
    if !O.max > 0. then min !O.max ctx.endoffset else last -. start in
  runtime ("clean up "^(String.concat "_" base))
    (fun _ ->
      let last = last_time +. start in
      close_everything corestate last (fun _-> true);
      close_everything_hash pending last (fun _-> true);
      close_everything freqtrace last (fun (v,_) -> v >= 0));
  let last_time =
    if !O.max > 0. then !O.max else last -. start in
  let (startpoint,endpoint) = (* startpoint and endpoint are offsets *)
    match !O.cmds, !firstmatch with
      [],_ -> !O.min,last_time
    | _, Some fm -> (* fm is a time *)
	let fm = fm -. start in (* fm is an offset *)
	(match !endoffset with
	  None -> max !O.min fm,last_time
	| Some offset -> max !O.min fm, fm +. offset)
    | _, None -> !O.min,last_time in
  let xlabel = if success then "" else ", FAILED" in
  let ranges =
    if !show = []
    then [(startpoint,endpoint,String.concat "_" base,[],0)]
    else
      if inkvm
      then failwith "show not supported"
      else
	let (base,ctx) = List.hd files in
	Util.interpret_show_commands !show ctx startpoint endpoint in
  List.iter (fun (base,ctx) -> Util.remove 2 ctx) files;
  let unspace s =
    let s = String.concat "" (String.split_on_char ' ' s) in
    String.concat "" (String.split_on_char '/' s) in
  List.iter
    (function (startpoint,endpoint,cmd,_pid,index) ->
      let base =
	if cmd = String.concat "_" base
	then base
	else base @ [unspace cmd; string_of_int index] in
      let duration =
	if !show = [] then (last -. start) else endpoint -. startpoint in
      (if !freqgraph
      then
	let base = base@["freqgraph"] in
	make_freqgraph_graph machine_info freqtrace base
	  startpoint endpoint start last_time duration xlabel);
      (if !fast_freq
      then
	let base = base@["freq"] in
	(if !O.nograph
	then make_freq_text machine_info freqtrace base
	else
	  make_freq_graph machine_info freqtrace base
	    startpoint endpoint start last_time duration xlabel));
      (if !mfreq
      then failwith "not yet supporting a mix of execution and frequency");
      (if !freq
      then
	let base = base@["freq"] in
	(if !O.nograph
	then make_freq_text machine_info freqtrace base
	else
	  make_freq_graph machine_info freqtrace base
	    startpoint endpoint start last_time duration xlabel));
      (if !showcores
      then
	let base =
          if (!color_by_command || !color_by_top_command ||
          !color_by_pid || !color_by_top_pid || !color_by_parent)
          then base@["cores";"color"]
          else base@["cores"] in
	make_core_graph corestate pending first_appearance !sched_events
	  !waking_edges base startpoint endpoint start
	  last_time duration xlabel parents);
      (if !sockets
      then
	let base = base@["sockets"] in
	make_core_graph corestate pending first_appearance !sched_events
	  !waking_edges base startpoint endpoint start
	  last_time duration xlabel parents);
      (if (!color_by_command || !color_by_top_command ||
          !color_by_pid || !color_by_top_pid || !color_by_parent) && not !showcores
      then
	let base = base@["color"] in
	(if !O.nograph
	then
	  make_execution_text corestate pending first_appearance !sched_events
	    base startpoint endpoint
	    start last_time duration xlabel parents
	else
	  make_execution_graph corestate pending first_appearance !sched_events
	    !waking_edges base startpoint endpoint
	    start last_time duration xlabel parents));
      (if !exec
      then
	begin
	  let c1 = !color_by_command in
	  let c2 = !color_by_top_command in
	  let c1a = !color_by_pid in
	  let c2a = !color_by_top_pid in
	  let c3 = !color_by_parent in
	  color_by_command := false;
	  color_by_top_command := false;
	  color_by_pid := false;
	  color_by_top_pid := false;
	  color_by_parent := false;
	  make_execution_graph corestate pending first_appearance !sched_events
	    !waking_edges base startpoint endpoint
	    start last_time duration xlabel parents;
	  color_by_command := c1;
	  color_by_top_command := c2;
	  color_by_pid := c1a;
	  color_by_top_pid := c2a;
	  color_by_parent := c3
	end))
    ranges

(* ------------------------------------------------------------------------ *)

let files = ref []

let set_and_mextension cell tag =
  Arg.Unit
    (fun _ ->
      cell := true;
      O.modifiers := tag :: !O.modifiers)

let set_and_extension2 cell cell2 tag =
  Arg.Unit
    (fun _ ->
      cell := true;
      cell2 := true;
      O.modifiers := tag :: !O.modifiers)

let set_and_vextension cell tag =
    (fun v ->
      cell := v;
      O.modifiers := tag :: !O.modifiers)

let options =
  O.generic_options @
  ["--exec", Arg.Set exec, "  execution (default)";
    "--freq", set_and_mextension freq "freq", "  frequency information";
    "--fast-freq", set_and_mextension fast_freq "freq",
    "  frequency information per tick";
    "--freqgraph",
    set_and_extension2 (if !fast_freq then fast_freq else freq) freqgraph
      "freqgraph",
    "  precise frequency information";
    "--mfreq", set_and_extension2 freq  mfreq "freq",
    "  mix execution and frequency information";
    "--color-by-command", set_and_mextension color_by_command "color",
    "  color by command";
    "--color-by-top-command", set_and_mextension color_by_top_command "color",
    "  color by 25 most frequently activated commands";
    "--color-by-pid", set_and_mextension color_by_pid "color",
    "  color by pid";
    "--color-by-top-pid", set_and_mextension color_by_top_pid "color",
    "  color by 25 most frequently activated pids";
    "--color-by-parent", set_and_mextension color_by_parent "color",
    "  color by parent (exec ancestor)";
    "--target", Arg.String (fun c -> target := (String.length c,c) :: !target),
    "  command or command prefix to show in color";
    "--events", Arg.Set events, "  notches and arrows for events";
    "--waking-events", Arg.Set waking_events, "  notches for waking events";
    "--waking-waker-events",
    Arg.Unit (fun _-> waking_events := true; waking_waker_events := true),
    "  notches for waking events on the waker core";
    "--wakeup-events", Arg.Set wakeup_events, "  notches for wakeup events";
    "--waking-edges", Arg.Unit (fun _ -> events := true; waking_edge := true),
    "  edges from waking to wakeup (can be voluminous)";
    "--kvm", Arg.String (set_and_vextension kvm "kvm"), "  kvm host trace";
    "--miny", Arg.Float (fun s -> miny := s),
    "  min value to show on the y axis for freqgraph";
    "--endoffset",
    Arg.String
      (fun s ->
	O.modifiers := Printf.sprintf "until_%s" s :: !O.modifiers;
	endoffset := Some (float_of_string s)),
    "  endpoint offset from starting point, eg specified with --cmd";
    "--sockets", set_and_mextension sockets "sockets",
    "  pids on y axis and color by socket";
    "--cores", set_and_mextension showcores "cores",
    "  pids on y axis and color by core";
    "--socket-order", set_and_mextension socket_order "socketorder",
    "  reorganize cores to put cores on the same socket adjacent";
    "--colors", Arg.Int Printer.ncolors, "  number of colors";
    "--show", Arg.String (fun s -> show := s :: !show),
    "  focus on the execution parallel with a specific command - when used with freq only shows that command and its children, when used without freq shows everything in the time range";
    "--show-list",
    Arg.String (fun s -> show := (Util.cmd_to_list ("cat "^s)) @ !show),
    "  focus on the execution parallel with the list of commands in a file";
    "--forked", Arg.Unit (fun _ -> forked := true; O.pids := (-1) :: !O.pids), "  forked tasks only";
    "--big", Arg.Set big, "  big graph";
    "--find-cmd", Arg.Set find_cmd, "  use heuristics to try to find the phoronix command of interest";
    "--ps", Arg.Unit (fun _ -> ty := JgraphPs),
    "  generate graphs in postscript (faster, but bigger files)";
    "--implot", Arg.Unit (fun _ -> ty := Printer.ImPlot(3)),
    "  generate graphs for implot";]

let anonymous s = files := Util.cmd_to_list (Printf.sprintf "ls %s" s) @ !files

let usage = "dat2graph file.dat [options]"

(* ------------------------------------------------------------------------ *)

let _ =
  Arg.parse (Arg.align options) anonymous usage;
  (if not (List.mem "freq" !O.modifiers) && not (List.mem "color" !O.modifiers) &&
      not (List.mem "cores" !O.modifiers)
  then exec := true); (* default case *)
  (if !find_cmd
  then
    match Util.find_cmd (List.hd !files) with
      None -> Printf.eprintf "Warning: no command for %s\n" (List.hd !files)
    | Some (Util.Cmd c) -> O.cmds := (String.length c,c) :: !O.cmds
    | Some (Util.Pids (c,l)) ->
	O.cmds := (String.length c,c) :: !O.cmds;
	O.pids := l @ !O.pids);
  let elements =
    let e1 =
      if !events
      then
	["sched_switch";"sched_process_exec";"sched_process_exit";
	  "sched_waking";"sched_wakeup";
          "sched_process_fork";"sched_wakeup_new";
	  "sched_migrate_task";"function";
	  "sched_wake_idle_without_ipi";
	  "sched_swap_numa";"sched_move_numa";"sched_stick_numa"]
      else [] in
    let e1a = if !waking_events then ["sched_waking"] else [] in
    let e1b = if !wakeup_events then ["sched_wakeup"] else [] in
    let e2 =
      if !freq || !fast_freq
      then ["bprint"]
      else [] in
    let e3 =
      if !show <> [] || !O.pids <> [] || !O.cmds <> []
      then ["sched_switch";"sched_process_exec";"sched_process_fork"]
      else [] in
    let e4 =
      if !exec || !freq || !color_by_command || !color_by_top_command ||
         !color_by_pid || !color_by_top_pid || !color_by_parent
      then ["sched_switch";"sched_process_exec"]
      else [] in
    let e5 =
      if !color_by_command || !color_by_top_command || !color_by_pid ||
         !color_by_top_pid || !color_by_parent
      then ["sched_waking"]
      else [] in
    let e6 =
      if !mfreq
      then ["bprint"]
      else [] in
    let e7 =
      if !color_by_parent
      then ["sched_process_fork"]
      else [] in
    Util.union e7
      (Util.union e6
	 (Util.union e5
	    (Util.union e4
	       (Util.union e3
		  (Util.union e2 (Util.union e1b (Util.union e1a e1))))))) in
  let core_mapping =
    match !kvm with
      "" -> Array.make 0 (Array.make 0 0)
    | k ->
	Array.of_list
	  (List.map
	     (fun file -> Util.core_mapping k file)
	     !files) in
  let files =
    match !kvm with
      "" -> !files
    | k -> k :: !files in
  let files =
    runtime "get_files"
      (fun _ -> (* not collecting info on interrupts; see dat2graph *)
	Util.get_files_and_endpoint files !O.after_sleep false elements) in
  (if !O.relaxed
  then
    if !O.max > 0. (* always true? *)
    then minsleep := (!O.max -. !O.min) /. 5000.);
  (if !O.debug
  then Printf.eprintf "elements: %s\n" (String.concat " " elements));
  if !kvm = ""
  then List.iter (fun f -> main elements core_mapping [f]) files
  else main elements core_mapping files
