(* SPDX-License-Identifier: GPL-2.0 *)
(* Parsing lines of the output of trace-cmd report.  Useful for many tools *)

type cmd = string
type pid = int
type cpu = int
type state = Terminate | Block of char | Yield
type wstate =
    Waking of cpu (*old cpu*) * cpu (* target *)
  | Woken | Numa of cpu (*src*) * cpu (*dst*)
let state = Hashtbl.create 101
let commands = Hashtbl.create 101

type events =
    (* unblock - exec *)
    Sched_waking of cmd * pid (*waker*) * cmd * pid (*wakee*) * cpu *
	bool (* in interrupt *)
  | Sched_wakeup of cmd * pid * cpu option (* old cpu *) * cpu
  | Sched_wakeup_new of cmd * pid * cpu (*parent*) * cpu (*self*)
  | Sched_wake_idle_without_ipi of cpu
  | Sched_migrate_task of cmd * pid * cpu * cpu * wstate
  | Sched_switch of cmd * pid (*from*) * state * cmd * pid (*to*)
  | Sched_tick of int (*freq*)
    (* process lifetime *)
  | Sched_process_fork of cmd * pid (*parent*) * cmd * pid (*child*)
  | Sched_process_exec of cmd (* new name*) * cmd (* old name*) *
	pid (*new pid*) * pid (*old pid*)
  | Sched_process_exit of cmd * pid
    (* numa balancing *)
  | Sched_swap_numa of pid (*src pid*) * cpu (*from*) *
	pid (*dst pid*) * cpu (*to*)
  | Sched_move_numa of pid * cpu (*from*) * cpu (*to*)
  | Sched_stick_numa of pid * cpu (*from*) * cpu (*to*)
    (* other *)
  | Sched_wait_task of cmd * pid
  | Sched_process_free of cmd * pid
  | Traced_function of string * int * string (*function name*)
  | Cpu_frequency of int (* requested frequency *)
  | Cpu_idle of int (* state *)
  | Util of int (* util *)
  | Page_fault_user of string (* address *) *
	string (* last three bytes of the instruction pointer *)
  | Other of string * int * string * string list option
  | Not_supported of string * string * string * string

let hashadd tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref [] in
      Hashtbl.add tbl k cell;
      cell in
  cell := v :: !cell

let timeacc = Hashtbl.create 101
let acctime key f =
      let t = Sys.time() in
      let res = f() in
      let u = Sys.time() in
      hashadd timeacc key (u -. t);
      res
let dumptime _ =
  Hashtbl.iter
    (fun key v ->
      let sum = List.fold_left (+.) 0. !v in
      let len = float_of_int(List.length !v) in
      Printf.printf "%s: %fs, ie %fs/call\n"
	key sum (sum /. len))
    timeacc

let ios n s =
  try int_of_string s
  with _ -> failwith (Printf.sprintf "%d: ios %s failed" n s)

let split_at_last c s =
  let mx = String.length s in
  let rec loop n =
    if String.get s n = c then n else loop (n-1) in
  let n = loop (mx-1) in
  (String.sub s 0 n,String.sub s (n+1) (mx - n - 1))

let get_after_last c s =
  let mx = String.length s in
  let rec loop n =
    if String.get s n = c then n else loop (n-1) in
  let n = loop (mx-1) in
  String.sub s (n+1) (mx - n - 1)

let getargs args =
  List.map (fun x -> get_after_last '=' x) args

let getcpu s =
  ios 1 (get_after_last ':' s)

let expand cmd index =
  if index > 0
  then Printf.sprintf "%s=%d" cmd index
  else cmd

let getcmd s index =
  let (cmd,pid) = split_at_last ':' s in
  (expand cmd index,ios 2 pid)

let trim str = (* space at front *)
  let len = String.length str in
  let rec loop n =
    if n = len
    then ""
    else
      if String.get str n <> ' '
      then (if n = 0
      then str
      else String.sub str n (len - n))
      else loop (n+1) in
  loop 0

let overall_parse infos =
  let infos =
    let fst = String.get infos 0 in
    if not ('0' <= fst && fst <= '9')
    then 
      try
	let newstart = String.index_from infos 0 ' ' in
	String.sub infos newstart (String.length infos - newstart)
      with _ -> failwith ("expected space in: "^infos)
    else infos in
  let i1 =
    try String.index_from infos 0 ':'
    with _ -> failwith ("expected colon in: "^infos) in
  let i2 =
    try String.index_from infos (i1+1) ':'
    with _ -> failwith ("expected colon after i1+1 in: "^infos) in
  let time = String.sub infos 0 i1 in
  let command =
    String.sub infos (i1+2) (i2 - i1 - 2) in
  let len = String.length infos in
  let rec loop n =
    if n = len
    then ""
    else
      if String.get infos n <> ' '
      then String.sub infos n (len - n)
      else loop (n+1) in
  let args = loop (i2+1) in
  (* old trace-cmd *)
  (time,command,args)

let parse_equal_args args =
  let pieces = String.split_on_char '=' args in
  (* before = is a single word; after may contain spaces *)
  let split_pieces =
    let rec loop = function
	[vl] -> [(vl,"")]
      | vl_kwd::others ->
	  let (rest,kwd) = (* first has no space *)
	    try split_at_last ' ' vl_kwd with _ -> ("",vl_kwd) in
	  (rest,kwd) :: loop others
      | [] -> failwith "no args" in
    loop pieces in
  let rec get_values = function
      (prev,kwd)::(((vl,next)::_) as rest) ->
	vl :: get_values rest
    | _ -> [] in
  get_values split_pieces

let parse_wakeup_args args =
  try
    let left = String.index_from args 0 '[' in
    let front = String.sub args 0 (left - 1) in
    let right = String.index_from args left ']' in
    let rest = String.sub args (right + 2) (String.length args - right - 2) in
    let rest = String.split_on_char ' ' rest in
    front :: rest
  with _ -> (* try raw format *)
    (* comm=ThreadPoolSingl pid=2984 prio=120 target_cpu=043 *)
    match parse_equal_args args with
      [comm;pid;prio;cpu] -> [comm^":"^pid;"CPU:"^cpu]
    | _ -> failwith "bad wakeup"

let parse_switch_args args =
  try
    let rec get_before i =
      if String.get args i = ' ' && String.get args (i+1) = '['
      then (String.sub args 0 i, i+2)
      else get_before (i+1) in
    let rec get_state i =
      if String.get args i = ']'
      then (String.get args (i+2), i+8)
      else get_state (i+1) in
    let rec get_after start i =
      if String.get args i = ' ' && String.get args (i+1) = '['
      then String.sub args start (i-start)
      else get_after start (i+1) in
    let (before,i) = get_before 0 in
    let (state,i) = get_state i in
    let after = get_after i i in
    (before,state,after)
  with _ -> (* try raw format *)
    (* prev_comm=ThreadPoolSingl prev_pid=2984 prev_prio=120 prev_state=S ==> next_comm=swapper/43 next_pid=0 next_prio=120 *)
    match Str.split (Str.regexp_string " ==> ") args with
      [prev;next] ->
	(match (parse_equal_args prev,parse_equal_args next) with
	  ([comm;pid;prio;state],[ncomm;npid;nprio]) ->
	    (comm^":"^pid,String.get state 0,ncomm^":"^npid)
	| _ -> failwith "bad sched switch")
    | _ -> failwith "bad sched switch"

let actor_core_and_infos s =
  let mx = String.length s in
  let lb = ref 0 in
  let rb = ref 0 in
  let rec loop n =
    if String.get s n = '['
    then (lb := n; loop (n+1))
    else if String.get s n = ']'
    then rb := n
    else loop (n+1) in
  loop 0;
  (String.sub s 0 !lb,
   String.sub s (!lb+1) (!rb - !lb - 1), false,
   String.sub s (!rb+1+1) (mx - (!rb+1) - 1)) (* rb + 1 to drop a space also *)

let actor_core_and_infos_interrupt s =
  let (command,s) =
    match String.split_on_char '-' s with
      s1::s2::s3 when String.length s2 = 3 && String.get s2 1 = ' ' ->
	(s1^"-"^s2,String.concat "-" s3)
    | s1::s2 -> (s1,String.concat "-" s2)
    | [] -> failwith ("actor_core_and_infos_interrupt: bad line: "^s) in
  let mx = String.length s in
  let rec loop n =
    if String.get s n = ':' &&  String.get s (n+1) = ' '
    then n
    else loop (n+1) in
  let n = loop 0 in
  let front = String.sub s 0 n in
  let space = [' ';'\t'] in
  let rec first_space n =
    if List.mem (String.get front n) space
    then n
    else first_space (n+1) in
  let rec last_space n =
    if List.mem (String.get front n) space
    then n
    else last_space (n-1) in
  let len = String.length front in
  let first_space = first_space 0 in
  let last_space = last_space (len - 1) in
  let pid = String.sub front 0 first_space in
  let time = String.sub front (last_space+1) (len-last_space-1) in
  let core_interrupt =
    String.trim (String.sub front first_space (last_space-first_space)) in
  let in_interrupt s = List.mem (String.get s 2) ['h';'H';'s'] in
  let rec loop n =
    let c = String.get core_interrupt n in
    if '0' <= c && c <= '9'
    then loop (n+1)
    else n in
  let after_core = loop 0 in
  let core = String.sub core_interrupt 0 after_core in
  let interrupt =
    String.sub core_interrupt after_core (String.length core_interrupt - after_core) in
  let interrupt = in_interrupt interrupt in
  let actor = command^"-"^pid in
  let rest = time ^ String.sub s n (mx - n) in
  (actor,core,interrupt,rest)

let trim_exec_command s =
  let s = Filename.basename s in
  try String.sub s 0 15 (* names seem to be truncated at 15 characters *)
  with _ -> s

let migrate_list =
  ["sched_waking";"sched_wakeup";"sched_wakeup_new";"sched_process_fork"]

let iparse_tbl = Hashtbl.create 101

let iparse_line index s interrupt valid =
  let (actor,core,interrupt,infos) =
    try
      if interrupt
      then actor_core_and_infos_interrupt s
      else actor_core_and_infos s
    with _ -> failwith (Printf.sprintf "Problem parsing: %s\n" s) in
  let (time,comm,args) = overall_parse infos in
  let return e =
    let core = ios 3 core in
    (core, float_of_string time, e) in
  if valid = [] || List.mem comm valid ||
  (comm = "sched_process_fork" && List.mem "sched_process_exec" valid) ||
  (List.mem "sched_migrate_task" valid && List.mem comm migrate_list)
  then
    (match comm with
      "sched_switch" ->
	let (c1,st,c2) = parse_switch_args args in
	let st =
	  if List.mem st ['Z';'X'] then Terminate
	  else if List.mem st ['D';'S';'W';'T';'I'] then Block st
	  else Yield in
	let (cmd1,pid1) = getcmd c1 index in
	let (cmd2,pid2) = getcmd c2 index in
	(if st = Yield &&
            (try match Hashtbl.find state (index,pid1) with Numa _ -> false | _ -> true with _ -> false)
        then Hashtbl.remove state (index,pid1)); (* not waking if non-numa yielded *)
	Hashtbl.remove state (index,pid2);
	return(Sched_switch(cmd1,pid1,st,cmd2,pid2))
    | "sched_waking" ->
	let actor = String.trim actor in
	let (actor_cmd,actor_pid) = split_at_last '-' actor in
	let actor_pid = ios 20 actor_pid in
	(match parse_equal_args args with
	  [cmd;pid;prio;tgt] ->
	    let pid = ios 4 pid in
	    let tgt = ios 5 tgt in
	    Hashtbl.replace state (index,pid) (Waking (ios 4 core,tgt));
	    return
	      (Sched_waking(expand actor_cmd index,actor_pid,expand cmd index,pid,tgt,interrupt))
	| _ -> failwith "bad sched_waking")
    | "sched_wakeup" ->
        (match parse_wakeup_args args with
	  [cmd;_;core] (*5.9*) | [cmd;core] (*5.15*) ->
	    let (cmd,pid) = getcmd cmd index in
	    let cpu = getcpu core in
	    let prevcpu =
	      try
		match Hashtbl.find state (index,pid) with
		  Waking(oldcpu,_) -> Some oldcpu
		| _ -> None
	      with Not_found -> None in
	    Hashtbl.replace state (index,pid) Woken;
	    return(Sched_wakeup(cmd,pid,prevcpu,cpu))
	| l ->
	    failwith
	      (Printf.sprintf
		 "bad sched_wakeup: %s\nargs: %s\narg pieces: %s\n" s args
		 (String.concat "|" l)))
    | "sched_wakeup_new" ->
        (match parse_wakeup_args args with
	  [cmd;_;core] | [cmd;core] (*see above*) ->
	    let (cmd,pid) = getcmd cmd index in
	    let cpu = getcpu core in
	    let parent =
	      try
		match Hashtbl.find state (index,pid) with
		  Waking (_,c) -> c
		| _ ->
		    failwith
		      (Printf.sprintf "%s: wakeup_new %d but not following a fork"
			 time pid)
	      with Not_found -> cpu in
	    Hashtbl.replace state (index,pid) Woken;
	    return(Sched_wakeup_new(cmd,pid,parent,cpu))
	| _ -> failwith ("bad sched_wakeup_new: "^args))
    | "sched_wake_idle_without_ipi" ->
	(match parse_equal_args args with
	  [cpu] ->
	    let cpu = ios 6 cpu in
	    return(Sched_wake_idle_without_ipi cpu)
	| _ -> failwith "bad sched_wake_idle_without_ipi")
    | "sched_migrate_task" ->
	(match parse_equal_args args with
	  [cmd;pid;prio;orig;dst] ->
	    let pid = ios 7 pid in
	    let orig = ios 8 orig in
	    let dst = ios 9 dst in
	    let st =
	      try Hashtbl.find state (index,pid) with Not_found -> Woken in
	    let state =
	      match st with
		Numa(c1,c2) ->
		  Hashtbl.replace state (index,pid) Woken;
		  if orig <> c1 || dst <> c2
		  then Woken (* Numa abandoned *)
		  else st
	      | _ -> st in
	    return(Sched_migrate_task(expand cmd index,pid,orig,dst,state))
	| _ -> failwith "bad sched_migrate_task")
    | "bprint" ->
	return
	  (match String.split_on_char ' ' args with
	    ["arch_scale_freq_tick:";"freq";v] -> Sched_tick(ios 11 v)
	  | ["aperfmperf_snapshot_khz:";"freq";v] -> Sched_tick(ios 11 v)
	  | [_;"util";v] | [_;"util:";v] -> Util(ios 11 v)
	  | pieces ->
	      let actor = String.trim actor in
	      let (actor_cmd,actor_pid) = split_at_last '-' actor in
	      let actor_pid = ios 20 actor_pid in
	      Other (expand actor_cmd index, actor_pid, args, Some pieces))
    | "bputs" ->
    	let actor = String.trim actor in
	let (actor_cmd,actor_pid) = split_at_last '-' actor in
	let actor_pid = ios 20 actor_pid in
	return(Other(expand actor_cmd index,actor_pid,args,None))
    | "sched_process_fork" ->
	(match parse_equal_args args with
	  [cmd;pid;child_cmd;child_pid] ->
	    let pid = ios 13 pid in
	    let child_pid = ios 14 child_pid in
	    let core = ios 5 core in
	    Hashtbl.replace state (index,child_pid) (Waking (core,core));
	    Hashtbl.replace commands child_pid child_cmd;
	    return(Sched_process_fork(expand cmd index,pid,expand child_cmd index,child_pid))
	| _ -> failwith "bad sched_process_fork")
    | "sched_process_exec" ->
	(match parse_equal_args args with
	  [cmd;pid;oldpid] ->
	    (* discard the complete path name, which is not used elsewhere *)
	    let cmd = trim_exec_command cmd in
	    let pid = ios 15 pid in
	    let oldpid = ios 16 oldpid in
	    let oldcmd =
	      try Hashtbl.find commands oldpid
	      with Not_found -> "unknown" in
	     (* consistent with fork *)
	    Hashtbl.replace commands pid (Filename.basename cmd);
	    return
	      (Sched_process_exec(expand cmd index,expand oldcmd index,pid,oldpid))
	| _ -> failwith "bad sched_process_exec")
    | "sched_process_exit" ->
	(match parse_equal_args args with
	  [cmd;pid;_prio] ->
	    return(Sched_process_exit(expand cmd index,ios 17 pid))
	| _ -> failwith "bad sched_process_exit")
    | "sched_swap_numa" ->
	(match parse_equal_args args with
	  [src_pid;_;_;src_cpu;_;dst_pid;_;_;dst_cpu;_] ->
	    let src_pid = ios 18 src_pid in
	    let src_cpu = ios 19 src_cpu in
	    let dst_pid = ios 20 dst_pid in
	    let dst_cpu = ios 21 dst_cpu in
	    Hashtbl.replace state (index,src_pid) (Numa(src_cpu,dst_cpu));
	    Hashtbl.replace state (index,dst_pid) (Numa(dst_cpu,src_cpu));
	    return(Sched_swap_numa(src_pid,src_cpu,dst_pid,dst_cpu))
	| _ -> failwith "bad sched_swap_numa")
    | "sched_move_numa" ->
	(match parse_equal_args args with
	  [pid;_;_;src_cpu;_;dst_cpu;_] ->
	    let pid = ios 22 pid in
	    let src_cpu = ios 23 src_cpu in
	    let dst_cpu = ios 24 dst_cpu in
	    Hashtbl.replace state (index,pid) (Numa(src_cpu,dst_cpu));
	    return(Sched_move_numa(pid,src_cpu,dst_cpu))
	| _ -> failwith "bad sched_move_numa")
    | "sched_stick_numa" ->
	(match parse_equal_args args with
	  [pid;_;_;src_cpu;_;dst_cpu;_] ->
	    return(Sched_stick_numa(ios 25 pid,ios 26 src_cpu,ios 27 dst_cpu))
	| [src_pid;_;_;src_cpu;_;dst_pid;_;_;dst_cpu;_] ->
	    return(Sched_stick_numa(ios 25 src_pid,ios 26 src_cpu,
				    ios 27 dst_cpu))
	| _ -> failwith "bad sched_stick_numa")
    | "sched_wait_task" ->
	(match parse_equal_args args with
	  [cmd;pid;_] ->
	    return(Sched_wait_task(expand cmd index,ios 28 pid))
	| _ -> failwith "bad sched_wait_task")
    | "sched_process_free" ->
	(match parse_equal_args args with
	  [cmd;pid;_] ->
	    return(Sched_process_free(expand cmd index,ios 28 pid))
	| _ -> failwith "bad sched_process_free")
    | "page_fault_user" ->
	(match parse_equal_args args with
	  [address;ip;_] ->
	    return(Page_fault_user(address,String.sub ip (String.length ip - 3) 3))
	| _ -> failwith "bad page_fault_user")
    | "function" ->
	let function_name = args in
	let actor = String.trim actor in
	let (actor_cmd,actor_pid) = split_at_last '-' actor in
	let actor_pid = ios 20 actor_pid in
        return(Traced_function(expand actor_cmd index, actor_pid, function_name))
    | "cpu_frequency" ->
	(match parse_equal_args args with
	  [freq;_] -> return(Cpu_frequency (int_of_string freq))
	| _ -> failwith "bad cpu_frequency")
    | "cpu_idle" ->
	(match parse_equal_args args with
	  [state;_] -> return(Cpu_idle (int_of_string state))
	| _ -> failwith "bad cpu_idle")
    | sys ->
        let args = sys ^ " " ^ args in
	let pieces = String.split_on_char ' ' args in
        let actor = String.trim actor in
	let (actor_cmd,actor_pid) = split_at_last '-' actor in
	let actor_pid = ios 20 actor_pid in
        return(Other (expand actor_cmd index, actor_pid, args, Some pieces)))
  else (-1,(-1.0),Not_supported (core,time,comm,args))

let parse_line = iparse_line 0

let make_text files =
  List.map
    (function file ->
      if Filename.extension file = ".dat"
      then
	begin
	  let base = Filename.chop_extension file in
	  let fl = base ^ ".txt"  in
	  ignore (Sys.command(Printf.sprintf "trace-cmd report %s > %s" file fl));
	  fl
	end
      else file)
    files
    
let pcc x y =
  let sum l = List.fold_left (+.) 0.0 l in
  let avg l = (sum l) /. (float_of_int (List.length l)) in
  let avgx = avg x in
  let avgy = avg y in
  let num =
    List.map2
      (fun x y -> (x -. avgx) *. (y -. avgy))
      x y in
  let denleft =
    sqrt
      (sum
	 (List.map
	    (fun x -> let d = x -. avgx in d *. d)
	    x)) in
  let denright =
    sqrt
      (sum
	 (List.map
	    (fun x -> let d = x -. avgy in d *. d)
	    y)) in
  (sum num) /. (denleft *. denright)
