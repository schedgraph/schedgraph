(* SPDX-License-Identifier: GPL-2.0 *)
val sys_command : string -> int
val cmd_to_list : string -> string list
val hashadd : ('a,'b list ref) Hashtbl.t -> 'a -> 'b -> unit
val hashadd_set : ('a,'b list ref) Hashtbl.t -> 'a -> 'b -> unit
val hashadd_unique : ('a,'b list ref) Hashtbl.t -> 'a -> 'b -> unit
val hashinc : ('a,int ref) Hashtbl.t -> 'a -> int -> unit
val hashincf : ('a,float ref) Hashtbl.t -> 'a -> float -> unit

val mean : float list -> float
val geomean : float list -> float
val stddev : float list -> float
val geosd : float list -> float
val pearson : (float * float) list -> float
val speedup_and_std : bool -> float -> float list -> float * float

val iota : int -> int list

val union : 'a list -> 'a list -> 'a list

val get_front : char -> string -> string

val get_minline : string -> float
val get_maxline : string -> float

type file_context =
    { text_file_name : string;
      after_sleep : bool;
      interrupt : bool;
      cores : int;
      start : float;
      endoffset : float;
      drop : bool (* drop txt file? *) }

val skip_front_continue : file_context -> in_channel

val get_files_and_endpoint :
    string list -> bool -> bool -> string list ->
      (string * file_context) list (* file infos *)

val synchronize_max : string list -> unit

val iparse_loop :
      file_context list -> float -> float -> string list -> 'a ->
	(int -> float -> int -> 'a -> float -> Parse_line.events -> unit) ->
	  (bool * float * float * 'a)

val parse_loop :
    file_context -> float -> float -> string list -> 'a ->
      (float -> int -> 'a -> float -> Parse_line.events -> unit) ->
	(bool * float * float * 'a)

val remove : int -> file_context -> unit

val choose_target : string list ref -> string list -> unit

val take : int -> 'a list -> 'a list (* first n elements *)

val mask2cores : string -> int list

val matches : string -> bool ref -> string -> bool

val interpret_show_commands :
    string list -> file_context -> float -> float ->
      (float * float * string * int list * int) list

type cmdres = Cmd of string | Pids of string * int list
val find_cmd : (* for phoronix *)
    string -> cmdres option

(* -------------------------------------------------------------------- *)
(* virtualization mappings *)

val core_mapping : string -> string -> int array

(* -------------------------------------------------------------------- *)
(* Some other utility functions *)

val reorder : int -> int -> int
val update_requested_pids : int -> string -> (int,unit) Hashtbl.t -> unit
val check_requested_pids : int -> string -> (int,unit) Hashtbl.t -> bool
