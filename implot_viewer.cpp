// SPDX-License-Identifier: GPL-2.0
// This file is adapted from the file linked to here: https://thescienceofcode.com/imgui-quickstart/
// That page has license  CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)

// Dear ImGui: standalone example application for GLFW + OpenGL 3, using programmable pipeline
// (GLFW is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan/Metal graphics context creation, etc.)
// If you are new to Dear ImGui, read documentation from the docs/ folder + read the top of imgui.cpp.
// Read online: https://github.com/ocornut/imgui/tree/master/docs

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "implot.h"
#include "dlfcn.h"
#include <stdio.h>
#include <stdlib.h>
#define GL_SILENCE_DEPRECATION
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <GLES2/gl2.h>
#endif
#include <GLFW/glfw3.h> // Will drag system OpenGL headers

// [Win32] Our example includes a copy of glfw3.lib pre-compiled with VS2010 to maximize ease of testing and compatibility with old VS compilers.
// To link with VS2010-era libraries, VS2015+ requires linking with legacy_stdio_definitions.lib, which we do using this pragma.
// Your own project should not be affected, as you are likely to link with a newer binary of GLFW that is adequate for your version of Visual Studio.
#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

// This example can also compile and run with Emscripten! See 'Makefile.emscripten' for details.
#ifdef __EMSCRIPTEN__
#include "../libs/emscripten/emscripten_mainloop_stub.h"
#endif

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "GLFW Error %d: %s\n", error, description);
}

typedef void (*module_setup_f)(void (*setup)(const char *test, const char *toplabel, const char *xlabel, const char *ylabel, int ymin, double ymax, ImPlotLocation legendplace, int lineweight));
typedef void (*module_data_f)(void (*setcolor)(float r, float g, float b), void (*setmark)(ImPlotMarker marker),
			      void (*plotline)(const char* label_id, const double* xs, const double* ys, int count, ImPlotLineFlags flags),
			      void (*plotscatter)(const char* label_id, const double* xs, const double* ys, int count, ImPlotLineFlags flags),
			      void (*plotstairs)(const char* label_id, const double* xs, const double* ys, int count, ImPlotStairsFlags flags));

const char *setup_test;
const char *setup_toplabel;
const char *setup_xlabel;
const char *setup_ylabel;
int setup_ymin;
double setup_ymax;
ImPlotLocation setup_legendplace;
int setup_lineweight;
void setup(const char *test, const char *toplabel, const char *xlabel, const char *ylabel, int ymin, double ymax, ImPlotLocation legendplace, int lineweight) {
	setup_test = test;
	setup_toplabel = toplabel;
	setup_xlabel = xlabel;
	setup_ylabel = ylabel;
	setup_ymin = ymin;
	setup_ymax = ymax;
	setup_legendplace = legendplace;
	setup_lineweight = lineweight;
}

void setcolor(float r, float g, float b) {
	ImPlot::SetNextLineStyle(ImVec4(r, g, b, 1.00f));
}

void setmark(ImPlotMarker marker) {
	ImPlot::SetNextMarkerStyle(marker);
}

void plotline(const char* label_id, const double* xs, const double* ys, int count, ImPlotLineFlags flags) {
	ImPlotRect lim = ImPlot::GetPlotLimits();
	if (count && lim.X.Max >= xs[0] && lim.X.Min <= xs[count-1])
		ImPlot::PlotLine(label_id, xs, ys, count, flags);
	else printf("discarding the line starting with %f %f because not(%f >= %f) or not(%f <= %f)\n",xs[0],ys[0],lim.X.Max,xs[0],lim.X.Min,xs[count-1]);
}

void plotscatter(const char* label_id, const double* xs, const double* ys, int count, ImPlotLineFlags flags) {
	ImPlotRect lim = ImPlot::GetPlotLimits();
	if (count && lim.X.Max >= xs[0] && lim.X.Min <= xs[count-1]) {
		ImPlot::PlotScatter(label_id, xs, ys, count, flags);
		if (ImPlot::IsPlotHovered()) {
			ImPlotPoint pos = ImPlot::GetPlotMousePos();
			ImVec2 pixpos = ImPlot::PlotToPixels(pos);
			ImPlotPoint widepos1 = ImPlot::PixelsToPlot(pixpos.x-3,pixpos.y);
			ImPlotPoint widepos2 = ImPlot::PixelsToPlot(pixpos.x+3,pixpos.y);
			double x1 = widepos1.x;
			double x2 = widepos2.x;
			int y = (int)pos.y;
			if (count && x2 >= xs[0] && x1 <= xs[count-1])
				for (int i=0; i != count; i++) {
					if (x1 <= xs[i] && x2 >= xs[i] && y == (int)ys[i]) {
						ImGui::BeginTooltip();
						ImGui::Text("%s", label_id);
						ImGui::EndTooltip();
					}
				}
		}
	}
}

void plotstairs(const char* label_id, const double* xs, const double* ys, int count, ImPlotStairsFlags flags) {
	ImPlot::PlotStairs(label_id, xs, ys, count, flags);
}

// Main code
int main(int argc, char** argv)
{
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    // Decide GL+GLSL versions
#if defined(IMGUI_IMPL_OPENGL_ES2)
    // GL ES 2.0 + GLSL 100
    const char* glsl_version = "#version 100";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
#elif defined(__APPLE__)
    // GL 3.2 + GLSL 150
    const char* glsl_version = "#version 150";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only
#endif

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(1280, 720, "Dear ImGui GLFW+OpenGL3 example", nullptr, nullptr);
    if (window == nullptr)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImPlot::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    ImGui::StyleColorsLight();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
    // - If the file cannot be loaded, the function will return a nullptr. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Use '#define IMGUI_ENABLE_FREETYPE' in your imconfig file to use Freetype for higher quality font rendering.
    // - Read 'docs/FONTS.md' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    // - Our Emscripten build process allows embedding fonts to be accessible at runtime from the "fonts/" folder. See Makefile.emscripten for details.
    //io.Fonts->AddFontDefault();
    //io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\segoeui.ttf", 18.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, nullptr, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(font != nullptr);

    // Our state
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    // Main loop
#ifdef __EMSCRIPTEN__
    // For an Emscripten build we are disabling file-system access, so let's not attempt to do a fopen() of the imgui.ini file.
    // You may manually call LoadIniSettingsFromMemory() to load settings from your own storage.
    io.IniFilename = nullptr;
    EMSCRIPTEN_MAINLOOP_BEGIN
#else
    while (!glfwWindowShouldClose(window))
#endif
    {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application, or clear/overwrite your copy of the mouse data.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application, or clear/overwrite your copy of the keyboard data.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

	for(int i = 1; i != argc; i++) {
		char *result;
		void *plugin = dlopen(argv[i], RTLD_NOW);
		if (!plugin) {
			printf("Cannot load %s: %s", argv[i], dlerror ());
			exit(1);
		}
		module_setup_f module_setup = (module_setup_f)dlsym(plugin, "module_setup");
		result = dlerror();
		if (result) {
			printf("Cannot find module_setup in %s: %s", argv[i], result);
			dlclose(plugin);
			exit(1);
		}
		module_data_f module_data = (module_data_f)dlsym(plugin, "module_data");
		result = dlerror();
		if (result) {
			printf("Cannot find module_data in %s: %s", argv[i], result);
			dlclose(plugin);
			exit(1);
		}
		module_setup(setup);
		ImGui::StyleColorsLight();
		ImGui::Begin(setup_test);
		if (ImPlot::BeginPlot(setup_toplabel, ImVec2(-1,-1))) {
			ImPlot::PushStyleVar(ImPlotStyleVar_LineWeight, setup_lineweight);
			ImPlot::SetupAxis(ImAxis_X1, setup_xlabel,ImPlotAxisFlags_NoGridLines);
			ImPlot::SetupAxis(ImAxis_Y1, setup_ylabel, ImPlotAxisFlags_Lock|ImPlotAxisFlags_NoGridLines);
			ImPlot::SetupAxisLimits(ImAxis_Y1, setup_ymin, setup_ymax);
			ImPlot::SetupLegend(setup_legendplace,ImPlotLegendFlags_Outside);
			module_data(setcolor,setmark,plotline,plotscatter,plotstairs);
			ImPlot::ShowStyleSelector("ImPlot Style");
			ImPlot::ShowColormapSelector("ImPlot Colormap");
			ImPlot::EndPlot();
		}
		ImGui::End();
		dlclose(plugin);
	}

        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }
#ifdef __EMSCRIPTEN__
    EMSCRIPTEN_MAINLOOP_END;
#endif

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImPlot::DestroyContext();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
