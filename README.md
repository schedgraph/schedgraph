# schedgraph

These are some tools for making graphs from scheduling traces created using
the following command:

trace-cmd record -e sched -v -e sched_stat_runtime {program to run}

All files in this directory are GPLv2 and are
(C) 2022, 2023, 2024 Julia Lawall <Julia.Lawall@inria.fr>

This software requires the installation of OCaml, opam, and jgraph. Note that the jgraph required is the one from Jim Plank (http://web.eecs.utk.edu/~jplank/plank/jgraph/jgraph.html) and not a Java tool.  Plank's jgraph is available in Ubuntu.

Using the ImPlot backend requires installing libglfw3-dev.  It also requires:

```
git submodule init
git submodule update
```

Some help can be obtained for each program using the --help option.

Some local adaptation of the software, particularly the file machines.ml,
may be needed.

A talk about this work containing usage examples was presented at FOSDEM 2023: https://fosdem.org/2023/schedule/event/sched_tracing/
A research report following the FOSDEM talk, and suitable for citing `dat2graph` and `running_waiting`, is also available: https://hal.inria.fr/hal-04001993

# Options common to dat2graph2 and running_waiting

Any number of trace files can be provided on the command line as anonymous arguments.

`--save-tmp`: Save the generated jgraph (.jgr) file.

`--min N`: Start the graph at offset N from the start of the run.

`--max N`: end the graph at offset N from the start of the run.

`--after-sleep`: Start the graph at the end of the initial sleep command.  Adding a sleep of 1 second before an application may help avoid side effects from the initialization of trace-cmd.

`--tmp`: Directory in which to store temporary files.

`--debug`: Various debugging information.

`--forked`: Show only tasks that are forked within the run.

# Options specific to dat2graph2

`--exec`: Horizontal lines indicate the starting and stopping point of the execution of a task on a core.  The color of a task is chosen according to its PID.  This is the default option.

`--color-by-command`: As for `--exec`, but the color is chosen according to the command name.  Numbers are replaced by X, unless a command name specified with the `--target` option is matched.

`--color-by-top-command`: As for `--color-by-command`, but only the top 25 commands are shown.

`--target comm`: `comm` is the prefix of the name of a command of interest.  If this option is used, then the line for the execution of other commands will be shown in black.

`--socket-order`: Number the cores such that cores with adjacent numbers are on the same socket.  Using this option requires modifying the function reorder in the file dat2graph2.ml, to take into account the topology of the machine.

`--ps`: A lot of the time required for dat2graph is to wait for the
postscript produced by jgraph to be converted to pdf.  This option stops at
generating postscript.  Output will be generated faster, but the postscript
files are much larger and are thus time consuming for a viewer to decode.

# Options specific to running_waiting

`--rw`: Print some statistics in file.rw rather than creating a graph.

`--waiting-only`: Show only waiting tasks.

`--overload`: Show the number of threads running on each core (1 thread means no overload).
