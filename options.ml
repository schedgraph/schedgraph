(* SPDX-License-Identifier: GPL-2.0 *)
(* generic options *)

let save_tmp = ref false
let min = ref 0.0
let max = ref (-1.0)
let ranges = ref []
let after_sleep = ref false
let nograph = ref false
let outdir = ref ""
let debug = ref false
let tmpdir = ref "/tmp"
let relaxed = ref false
let standard_xsize = ref 8.
let standard_ysize = ref 3.
let modifiers = ref []
let nanoseconds = ref false
let syncstart = ref false
let cmds = ref []
let pids = ref []

let generic_options =
    ["--debug", Arg.Set debug, "  giving timing info";
      "--after-sleep", Arg.Set after_sleep,
      "  skip the first sleep in the execution";
      "--min",
      Arg.String
	(fun s ->
	  (if !ranges <> [] then failwith "--min not compatible with --range");
	  min := float_of_string s;
	  modifiers := Printf.sprintf "from_%s" s :: !modifiers),
      "  min time to show on the x axis";
      "--max",
      Arg.String
	(fun s ->
	  (if !ranges <> [] then failwith "--max not compatible with --range");
	  max := float_of_string s;
	  modifiers := Printf.sprintf "upto_%s" s :: !modifiers),
      "  max time to show on the x axis";
      "--range",
      Arg.String
	(fun s ->
	  (if !min > 0. || !max > 0.
	  then failwith "--range not compatible with --min/max");
	  match String.split_on_char '-' s with
	    [a;b] ->
	      ranges :=
		(float_of_string a,float_of_string b,["from_"^a;"upto_"^b]) ::
		!ranges
	  | _ -> failwith "bad range"),
      "  time range, xxx-yyy";
      "--save-tmp", Arg.Set save_tmp, "  save .jgr files";
      "--nograph", Arg.Set nograph, "  no output graph";
      "--relaxed",
      Arg.Unit (fun _ -> relaxed := true; modifiers := "relaxed" :: !modifiers),
      "  no break for short pauses";
      "--tmpdir", Arg.Set_string tmpdir, "  directory for temporary files";
      "--outdir", Arg.Set_string outdir, "  directory for output files";
      "--xsize", Arg.Set_float standard_xsize, "  size of x axis (default 8in)";
      "--ysize", Arg.Set_float standard_ysize, "  size of y axis (default 3in)";
      "--nanoseconds", Arg.Set nanoseconds, "  nanoseconds in trace";
      "--syncstart", Arg.Set syncstart, "  all traces start at the earliest time (for vms)";
      "--cmd",
      Arg.String
	(fun c ->
	  modifiers := Printf.sprintf "from_%s" c :: !modifiers;
	  cmds := (String.length c,c) :: !cmds),
      "  command that is a starting point for showing colors";
      "--pid", Arg.String (fun p -> pids := (int_of_string p) :: !pids),
      "  pid that is a starting point for showing colors";
]

let fixmin _ =
  match !ranges with
    [] -> ranges := [(!min,!max,[])]
  | (mn,mx,_)::rest ->
      let (mn,mx) =
	List.fold_left
	  (fun (mn,mx) (curmn,curmx,_) ->
	    ((if curmn < mn then curmn else mn),
	     (if curmx > mx then curmx else mx)))
	  (mn,mx) rest in
      min := mn;
      max := mx
