(* SPDX-License-Identifier: GPL-2.0 *)
(* LaTeX tables and graphs *)

type column = Left | Center | Right | RightLeft | RightCenterLeft | Sized of float

let build_columns l =
  Printf.sprintf "|%s"
    (String.concat ""
       (List.map
	  (function col ->
	    match col with
	      Left -> "l|"
	    | Sized f -> Printf.sprintf "p{%fin}|" f
	    | Center -> "c|"
	    | Right -> "r|"
	    | RightLeft -> "r@{\\,\\,}l|"
	    | RightCenterLeft -> "r@{\\,\\,}c@{\\,\\,}l|")
	  l))

let clean debug s =
  let s = String.concat "\\_" (Str.split_delim (Str.regexp "_") s) in
  let s = String.concat "\\#" (Str.split_delim (Str.regexp "#") s) in
  let s = String.concat "\\%" (Str.split_delim (Str.regexp "%") s) in
  let s = String.concat "\\&" (Str.split_delim (Str.regexp "&") s) in
  let s = String.concat "$>$" (Str.split_delim (Str.regexp ">") s) in
  let s = String.concat "$<$" (Str.split_delim (Str.regexp "<") s) in
  let s = String.concat "$\\mid$" (Str.split_delim (Str.regexp "|") s) in
  if String.length s > 50
  then Printf.sprintf "{\\footnotesize %s}" s
  else s

let print_titles l titles =
  (if List.length l <> List.length titles
  then failwith "number of columns is differeng than the number of titles");
  String.concat "&"
    (List.map2
       (fun col title ->
	 let title = clean "a" title in
	 match col with
	   Left | Center | Sized _ -> title
	 | Right -> Printf.sprintf "\\multicolumn{1}{c|}{%s}" title
	 | RightLeft -> Printf.sprintf "\\multicolumn{2}{c|}{%s}" title
	 | RightCenterLeft -> Printf.sprintf "\\multicolumn{3}{c|}{%s}" title)
       l titles)

type value =
    Color of string * value
  | Bold of value
  | FloatV2 of float
  | FloatV4 of float
  | IntV of int
  | StringV of string
  | Pct of value * value option
  | PM of value * float * value

let print_values l values =
  let multi n s =
    if n > 1
    then Printf.sprintf "\\multicolumn{%d}{c|}{%s}" n s
    else s in
  let rec interp value =
    match value with
      Color (c,v) -> Printf.sprintf "\\textcolor{%s}{%s}" c (interp v)
    | Bold v -> Printf.sprintf "{\\bf %s}" (interp v)
    | FloatV2 f ->  Printf.sprintf "%0.2f" f
    | FloatV4 f ->  Printf.sprintf "%0.4f" f
    | IntV i -> string_of_int i
    | StringV s -> clean "b" s
    | _ -> failwith "bad term" in
  let topinterp col value =
    let n =
      match col with
	RightCenterLeft -> 3
      | RightLeft -> 2
      | _ -> 1 in
    match value with
      Color (c,v) -> multi n (Printf.sprintf "\\textcolor{%s}{%s}" c (interp v))
    | Bold v -> multi n (Printf.sprintf "{\\bf %s}" (interp v))
    | FloatV2 _ | FloatV4 _  | IntV _ | StringV _ ->  multi n (interp value)
    | Pct(leftv,None) when col = RightLeft -> Printf.sprintf "%s &" (interp leftv)
    | Pct(leftv,Some pct) when col = RightLeft ->
	Printf.sprintf "%s & (%s)" (interp leftv) (interp pct)
    | PM(leftv,rightv,pct) when col = RightCenterLeft ->
	Printf.sprintf "%s &$\\pm$& %0.4f (%s)" (interp leftv) rightv (interp pct)
    | _ -> failwith "bad column" in
  String.concat "&" (List.map2 topinterp l values)

let print_one_table o cols titles values long =
  Printf.fprintf o "\\begin{center}\\setlength{\\tabcolsep}{1pt}\\begin{tabular}{%s}\\hline\n" (build_columns cols);
  List.iter
    (function titles -> Printf.fprintf o "%s\\\\\n" (print_titles cols titles))
    titles;
  Printf.fprintf o "\\hline\n";
  List.iter
    (function values ->
      Printf.fprintf o "%s\\\\%s\n" (print_values cols values)
	(if long <> None then "\\hline" else ""))
    values;
  Printf.fprintf o "%s\\end{tabular}\\end{center}\n\n"
    (if long <> None then "" else "\\hline")

let pack_of_four l mx =
  let rec loop cur acc n = function
      [] ->
	if cur = []
	then List.rev acc
	else List.rev ((List.rev cur) :: acc)
    | x::xs ->
	if n = mx
	then loop [] ((List.rev cur) :: acc) 0 (x::xs)
	else loop (x::cur) acc (n+1) xs in
  loop [] [] 0 l

let print_table o cols titles values long =
  let rows =
    match long with
      Some n -> n
    | None -> 36 in
  let values = pack_of_four values rows in
  List.iter
    (function values ->
      print_one_table o cols titles values long)
    values
