(* SPDX-License-Identifier: GPL-2.0 *)
(* extract from events the actual event parsing code so that it can be used by other tools *)

let target = ref []
let ignored = ref []
let nox = ref []
let sedout = ref []
let normeq = ref false
let noargs = ref false

let generic_options =
  ["--target", Arg.String (fun s -> target := s :: !target),
    "  kernel functions to trace";
    "--ignore", Arg.String (fun s -> ignored := s :: !ignored),
    "  kernel functions to not trace";
    "--nonorm", Arg.String (fun s -> nox := (Str.regexp_string s) :: !nox),
    "  event that should not be normalized";
    "--normeq", Arg.Set normeq, "  = considered as space for normalization";
    "--noargs", Arg.Set noargs, "  drop all arguments";
    "--sedout", Arg.String (fun s -> sedout := (Str.regexp s) :: !sedout),
    "  strings to remove from event names (regexp)"]

(* criteria for showing an event *)

let contains str container =
  try ignore(Str.search_forward (Str.regexp_string str) container 0); true
  with _ -> false

let event_ok s =
  (!target = [] || (List.exists (fun tgt -> contains tgt s) !target)) &&
  (List.for_all (fun tgt -> not(contains tgt s)) !ignored)

(* ------------------------------------------------------------------------------- *)
(* normalization of events *)

(* helper function to find numbers *)
let isnum s =
  let num s =
    (String.length s > 2 && String.get s 0 = '0' && String.get s 1 = 'x') ||
    try ignore(int_of_string s); true
    with _ ->
      String.length s > 1 &&
      (try ignore(int_of_string ("0x"^s)); true
      with _ -> false) in
  match Str.split_delim (Str.regexp "[:,]") s with
    [n;""] -> num n
  | [n] -> num n
  | _ -> false

(* events that should be left completely unchanged *)
let no_normalize s =
  List.exists
    (fun re -> List.length (Str.split_delim re s) > 1)
    !nox

let normalize_event s pieces =
  let ns =
    if no_normalize s
    then s
    else if !noargs
    then
      (* extract the event name only *)
      let pieces = String.split_on_char ' ' s in
      match pieces with
	a::b::_ ->
	  if String.get a (String.length a-1) = ':' then a^" "^b else a
      | a::_ -> a
      | _ -> s
    else
      (* replace numbers by X *)
      let pieces =
	if !normeq
	then List.concat (List.map (String.split_on_char '=') pieces)
	else pieces in
      String.concat " " (List.map (fun x -> if isnum x then "X" else x) pieces) in
  List.fold_left (fun prev cur -> String.concat "" (Str.split cur prev))
    ns !sedout

(* ------------------------------------------------------------------------------- *)
(* adjust core number *)

let core_from_event s core =
  let rec loop = function
      "cpu:"::((x::_) as xs) ->
	let x = List.hd (String.split_on_char ',' x) in
	(try int_of_string x with _ -> loop xs)
    | x::xs -> loop xs
    | [] -> core in
  loop (String.split_on_char ' ' s)

(* ------------------------------------------------------------------------------- *)
(* event mark *)

let marktbl = Hashtbl.create 101
let marks = ["box";"x";"diamond";"triangle";"circle";"cross"]
let getmark c =
  try Hashtbl.find marktbl c
  with Not_found ->
    let mk = List.nth marks (c mod (List.length marks)) in
    Hashtbl.add marktbl c mk;
    mk
