all: dat2graph2 running_waiting events stepper implot_viewer

SYS=str.cma unix.cma -I +str -I +unix
LIB=options.mli options.ml parse_line.mli parse_line.ml manage_runqueues.mli manage_runqueues.ml \
util.mli util.ml parse_events.mli parse_events.ml printer.mli printer.ml machines.mli machines.ml

implot_viewer: implot_viewer.cpp
	make -f ImplotMakefile

dat2graph2: dat2graph2.ml ${LIB}
	ocamlc -g -o dat2graph2 ${SYS} ${LIB} dat2graph2.ml
	cp dat2graph2 dat2graph

running_waiting: running_waiting.ml ${LIB} print_latex.ml print_latex.mli
	ocamlc -g -o running_waiting ${SYS} ${LIB} print_latex.mli print_latex.ml running_waiting.ml

events: events.ml ${LIB}
	ocamlc -g -o events ${SYS} ${LIB} events.ml

stepper: stepper.ml ${LIB}
	opam install ocamlfind
	opam install curses
	ocamlc -g -o stepper `ocamlfind query curses`/curses.cma -I `ocamlfind query curses` ${SYS} ${LIB} stepper.ml
