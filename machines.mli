(* SPDX-License-Identifier: GPL-2.0 *)
type color_table = int * (int * int * int * int * Printer.color) list

val i80_freq_intervals : color_table
val yeti_freq_intervals : color_table
val troll_freq_intervals : color_table
val yeti_freq_intervals2 : color_table
val troll_freq_intervals2 : color_table
val gros_freq_intervals2 : color_table
val amd4650g_freq_intervals2 : color_table
val w2155_freq_intervals : color_table
val neowise_freq_intervals : color_table

val get_freqtbl : int -> string -> color_table * (int -> int)

val freq_to_color : int -> int * (int * int * int * int * Printer.color) list -> Printer.color
