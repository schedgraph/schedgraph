(* SPDX-License-Identifier: GPL-2.0 *)
type cmd = string
type pid = int
type cpu = int
type state = Terminate | Block of char | Yield
type wstate = Waking of cpu (*old cpu*) * cpu (* target *) | Woken | Numa of cpu (*src*) * cpu (*dst*)

type events =
    (* unblock - exec *)
    Sched_waking of cmd * pid (*waker*) * cmd * pid (*wakee*) * cpu * bool (* in interrupt *)
  | Sched_wakeup of cmd * pid * cpu option (* old cpu *) * cpu
  | Sched_wakeup_new of cmd * pid * cpu (*parent*) * cpu (*self*)
  | Sched_wake_idle_without_ipi of cpu
  | Sched_migrate_task of cmd * pid * cpu * cpu * wstate
  | Sched_switch of cmd * pid (*from*) * state * cmd * pid (*to*)
  | Sched_tick of int (*freq*)
    (* process lifetime *)
  | Sched_process_fork of cmd * pid (*parent*) * cmd * pid (*child*)
  | Sched_process_exec of cmd (* new name*) * cmd (* old name*) * pid (*new pid*) * pid (*old pid*)
  | Sched_process_exit of cmd * pid
    (* numa balancing *)
  | Sched_swap_numa of pid (*src pid*) * cpu (*from*) * pid (*dst pid*) * cpu (*to*)
  | Sched_move_numa of pid * cpu (*from*) * cpu (*to*)
  | Sched_stick_numa of pid * cpu (*from*) * cpu (*to*)
    (* other *)
  | Sched_wait_task of cmd * pid
  | Sched_process_free of cmd * pid
  | Traced_function of string * int * string (*function name*)
  | Cpu_frequency of int (* requested frequency *)
  | Cpu_idle of int (* state *)
  | Util of int (* util *)
  | Page_fault_user of string (* address *) * string (* last three bytes of the instruction pointer *)
  | Other of string * int * string * string list option
  | Not_supported of string * string * string * string

val pcc : float list -> float list -> float

val dumptime : unit -> unit

val make_text : string list -> string list

val iparse_line : int -> string -> bool -> string list -> int * float * events
val parse_line : string -> bool -> string list -> int * float * events
