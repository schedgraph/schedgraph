(* SPDX-License-Identifier: GPL-2.0 *)
(* print the timestamps of events generated using trace_printk. *)

module O = Options
module PE = Parse_events

let allevents = ref []
let zoom = ref false
let interval = ref (-1.)
let interval_string = ref ""
let core_interval = ref false
let numev = ref [](*[Str.regexp_string "select_task_rq_fair: mask size: X"]*)
let nograph = ref false
let cores = ref 160
let requested_something = ref false
let requested_live_pids = Hashtbl.create 101
let cmd_pids = Hashtbl.create 101
let cpus = ref []
let ty = ref Printer.Jgraph
let zoom_events = ref []
let zoom_event_cores = ref []
let zoom_pre_interval = ref 0.004
let zoom_post_interval = ref 0.004
let exec = ref false
let alpha = ref false
let usecores = ref false
let socket_order = ref false
let latency = ref []

(* --------------------------------------------------------- *)

type 'a openclosed = Open of float * 'a | Closed of float * float * 'a

let res2c = function
    Open(t0,_) -> Printf.sprintf "open at time %f\n" t0
  | Closed(t0,tm,_) -> Printf.sprintf "closed for time %f-%f\n" t0 tm

let do_open trace time y v =
  let find trace y = try Array.get trace y with _ -> [] in
  match find trace y with
    Closed(t1,t2,v')::rest when time = t2 && v = v' ->
      Array.set trace y (Open(t1,v)::rest)
  | Open (t,_) :: _ ->
      failwith (Printf.sprintf "%f: already open since %f" time t)
  | l -> Array.set trace y (Open(time,v)::l)

let do_close trace time y v startpoint =
  let find trace y = try Array.get trace y with _ -> [] in
  match find trace y with
    Open(t1,v') :: rest when v = v' ->
      if time < startpoint
      then Array.set trace y rest
      else if t1 < startpoint
      then Array.set trace y (Closed(startpoint,time,v) :: rest)
      else Array.set trace y (Closed(t1,time,v) :: rest)
  | [] ->
      if time >= startpoint
      then Array.set trace y [Closed(startpoint,time,v)]
  | _ -> failwith "not open"

(* --------------------------------------------------------- *)

let in_numev s = List.exists (fun x -> Str.string_match x s 0) !numev

let relevant live  =
  not !requested_something ||
  List.exists (Hashtbl.mem requested_live_pids) live

let process_line time core (events,latency_events,exec_trace,live_pids) startpoint l =
  let latency_start s =
    if List.for_all (fun tgt -> not(PE.contains tgt s)) !PE.ignored
    then
      try
	let (tgt1,tgt2,range,range_str) =
	  List.find (fun (tgt1,tgt2,range,range_str) -> PE.contains tgt1 s) !latency in
	Some (tgt1,range,range_str)
      with _ -> None
    else None in
  let latency_end s =
    if List.for_all (fun tgt -> not(PE.contains tgt s)) !PE.ignored
    then
      try
	let (tgt1,tgt2,range,range_str) =
	  List.find (fun (tgt1,tgt2,range,range_str) -> PE.contains tgt2 s) !latency in
	Some tgt1
      with _ -> None
    else None in
  let update_event ns time cr =
    Util.hashadd events ns (time,core,cr);
    if not (List.mem ns !allevents) then allevents := ns :: !allevents in
  let timeok = time > startpoint in
  if relevant !live_pids
  then
  match l with
    ((Parse_line.Traced_function(actor_cmd,actor_pid,s))
     |(Parse_line.Other(actor_cmd,actor_pid,s,None)))
    when timeok && Util.check_requested_pids actor_pid actor_cmd cmd_pids ->
      let yvalue = if !usecores then actor_pid else core in
      if !latency = [] && PE.event_ok s
      then update_event s time yvalue
      else
	(match latency_start s with
	  Some (tgt1,range,range_str) ->
	    Util.hashadd latency_events (core,actor_pid,tgt1) (time,yvalue,range,range_str)
	| None ->
	    (match latency_end s with
	      None -> ()
	    | Some tgt1 ->
		(match Hashtbl.find_opt latency_events (core,actor_pid,tgt1) with
		  Some({contents = (t1,yvalue,range,range_str)::rest}) ->
                    Hashtbl.remove latency_events (core,actor_pid,tgt1);
                    let ns =
		      Printf.sprintf "%s_2^%dx%s" tgt1 (int_of_float(log ((time -. t1) /. range) /. log 2.)) range_str in
		    update_event ns time yvalue
		| _ -> ())))

  | Parse_line.Other(actor_cmd,actor_pid,s,Some pieces) when timeok ->
      let latst = latency_start s in
      let lated = latency_end s in
      if ((!latency = [] && PE.event_ok s) || latst <> None || lated <> None)
	  && Util.check_requested_pids actor_pid actor_cmd cmd_pids
      then
	begin
	  let core =
	    let rec loop = function
		"cpu:"::((x::_) as xs) ->
		  let x = List.hd (String.split_on_char ',' x) in
		  (try int_of_string x with _ -> loop xs)
	      | x::xs -> loop xs
	      | [] -> core in
	    loop (String.split_on_char ' ' s) in
	  if in_numev s
	  then
	    begin
	      let v =
		try List.find PE.isnum pieces
		with Not_found -> failwith "num event must contain a number" in
	      let v = List.hd(Str.split_delim (Str.regexp "[:,]") v) in
	      let ns = (List.hd (String.split_on_char 'X' (PE.normalize_event s pieces))) ^ "X" in
	      update_event ns time (int_of_string v)
	    end
	  else
	    begin
	      let yvalue = if !usecores then actor_pid else core in
	      (match (latst,lated) with
		(Some (tgt1,range,range_str),_) ->
		  Util.hashadd latency_events (core,actor_pid,tgt1) (time,yvalue,range,range_str)
	      | (None,Some tgt1) ->
		  (match Hashtbl.find_opt latency_events (core,actor_pid,tgt1) with
		    Some({contents = (t1,yvalue,range,range_str)::rest}) ->
                      Hashtbl.remove latency_events (core,actor_pid,tgt1);
		      let ns =
			Printf.sprintf "%s_2^%dx%s" (PE.normalize_event tgt1 pieces)
			  (int_of_float (log ((time -. t1) /. range) /. log 2.))
			  range_str in
		      update_event ns time yvalue
		  | _ -> ())
	      | _ -> update_event (PE.normalize_event s pieces) time yvalue)
	    end
	end
  | Parse_line.Sched_move_numa(pid,src,dst) ->
      let s = "sched_move_numa" in
      if List.mem "sched_move_numa" !PE.target && Util.check_requested_pids pid "unknown" cmd_pids
      then
	begin
	  (if not (List.mem s !allevents)
	  then allevents := s :: !allevents);
	  Util.hashadd events s (time,core,if !usecores then pid else core)
	end
  | Parse_line.Sched_process_fork(_,parent_pid,_,pid) ->
      live_pids := pid :: !live_pids;
      (if Hashtbl.mem requested_live_pids parent_pid
      then Hashtbl.replace requested_live_pids pid ())
  | Parse_line.Sched_process_exec(cmd,oldcmd,pid,oldpid) ->
      (if pid <> oldpid then live_pids := pid :: !live_pids);
      (if Hashtbl.mem requested_live_pids oldpid && pid <> oldpid
      then Hashtbl.replace requested_live_pids pid ());
      (if !exec
      then
	begin
	  (if oldpid > 0 && Util.check_requested_pids oldpid oldcmd cmd_pids
	  then do_close exec_trace time core oldpid startpoint);
	  (if pid > 0 && Util.check_requested_pids pid cmd cmd_pids
	  then do_open exec_trace time core pid);
	end)
  | Parse_line.Sched_switch(fromcmd,frompid,state,tocmd,topid) ->
      (if !exec
      then
	begin
	  (if frompid > 0 && Util.check_requested_pids frompid fromcmd cmd_pids
	  then do_close exec_trace time core frompid startpoint);
	  (if topid > 0 && Util.check_requested_pids topid tocmd cmd_pids
	  then do_open exec_trace time core topid);
	end);
      (if state = Parse_line.Terminate
      then live_pids := List.filter (fun x -> x <> frompid) !live_pids)
  | _ -> ()
	
let parse_all info startpoint endpoint =
  cores := info.Util.cores;
  let events = Hashtbl.create 101 in
  let latency_events = Hashtbl.create 101 in
  let exec_trace = Array.make !cores [] in
  let live_pids = ref [] in
  let e = [] (*
    if !requested_something
    then ["bprint";"bputs";"sched_process_exec";"sched_process_fork";"sched_switch"]
    else ["bprint";"bputs"]*) in
  Util.parse_loop info startpoint endpoint e
    (events,latency_events,exec_trace,live_pids)
    process_line

(* ----------------------------------------------------- *)

let infos_to_interval start l =
  let rec loop mx cur acc = function
      [] -> List.rev ((mx -. !interval,cur) :: acc)
    | ((time,_)::rest) as all ->
	if time >= mx
	then loop (mx +. !interval) 1 ((mx -. !interval,cur) :: acc) all
	else loop mx (cur+1) acc rest in
  loop (start +. !interval) 0 [] l

let inrange l start startpoint endpoint =
  let startpoint = startpoint +. start in
  let endpoint = endpoint +. start in
  let rec loop acc = function
      (t,v)::rest ->
	if startpoint <= t && t <= endpoint
	then loop ((t,v) :: acc) rest
	else loop acc rest
    | _ -> List.rev acc in
  List.map
    (fun (ct,ev,l) ->
      let l = loop [] l in
      (List.length l,ev,l))
    l

let inrange_exec trace start startpoint endpoint =
  let tracetimes l =
    let res =
      List.map
	(function
	    Closed(t1,t2,v) -> t1
	  | Open(t1,_) -> t1)
	l in
    if not (List.sort compare res = (List.rev res))
    then failwith "fails on inrange exec" in
  let startpoint = startpoint +. start in
  let endpoint = endpoint +. start in
  (* trace is reversed *)
  let rec loop y acc = function
      Closed(t1,t2,v)::rest when t1 <= endpoint && t2 >= startpoint ->
	if !usecores
	then loop y (([(max startpoint t1,v);(min endpoint t2,v)],y) :: acc) rest
	else loop y (([(max startpoint t1,y);(min endpoint t2,y)],v) :: acc) rest
    | Open(t1,v)::rest when t1 <= endpoint ->
	if !usecores
	then loop y (([(max startpoint t1,v);(endpoint,v)],y) :: acc) rest
	else loop y (([(max startpoint t1,y);(endpoint,y)],v) :: acc) rest
    | _::rest -> loop y acc rest
    | [] -> acc in
  snd
    (Array.fold_left (fun (y,prev) l -> tracetimes l; (y+1,loop y prev l))
       (0,[]) trace)

let infos_to_core_interval start l =
  let rec loop mx cur acc = function
      [] -> List.rev ((mx -. !interval,List.length cur) :: acc)
    | ((time,core)::rest) as all ->
	if time >= mx
	then loop (mx +. !interval) [core] ((mx -. !interval,List.length cur) :: acc) all
	else
	  let cur =
	    if List.mem core cur
	    then cur
	    else core :: cur in
	  loop mx cur acc rest in
  loop (start +. !interval) [] [] l

let do_zoom events start base startpoint endpoint =
  let times =
    List.concat
      (List.map
	 (function (ct,ev,infos) ->
	   let ok =
	     List.exists
	       (function (str,re) ->
		 List.length (Str.split_delim re ev) > 1)
	       !zoom_events in
	   if ok
	   then
	     List.fold_left
	       (fun prev (t,core,_) ->
		 if !zoom_event_cores = [] || List.mem core !zoom_event_cores
		 then t::prev
		 else prev)
	       [] infos
	   else [])
	 events) in
  List.map
    (fun t ->
      (max startpoint (t -. start -. !zoom_pre_interval),
       min endpoint (t -. start +. !zoom_post_interval),
       Some t))
    (List.sort compare times)

let do_reorder l =
  if !socket_order
  then
    List.rev(List.fold_left (fun prev (v,c) -> (v,Util.reorder !cores c):: prev) [] l)
  else l

let main ((base,ctx),start,last,events,latency_events,exec_trace) =
  Hashtbl.clear cmd_pids;
  let base =
    if !O.modifiers = []
    then [base]
    else
      let endstr = List.sort compare !O.modifiers in
      base :: endstr in
  let base = String.concat "_" base in
  let base = if !socket_order then base ^ "_socketorder" else base in
  let fl =
    if !interval < 0.0
    then Printf.sprintf "%s_events" base
    else if !core_interval
    then Printf.sprintf "%s_events_core_interval_%s" base !interval_string
    else Printf.sprintf "%s_events_interval_%s" base !interval_string in
  let startpoint = !O.min in
  let endpoint = if !O.max < 0.0 then (last -. start) else !O.max in
  let events_list =
    List.map
      (function ev ->
	let infos =
	  try List.rev !(Hashtbl.find events ev)
	  with Not_found -> [] in
	let infos =
	  if !cpus = []
	  then infos
	  else
	    List.rev
	      (List.fold_left
		 (fun prev (tm,core,y) ->
		   if List.mem y !cpus then (tm,core,y)::prev else prev)
		 [] infos) in
	(List.length infos,ev,infos))
      !allevents in
  let core_events_list =
    if !alpha
    then
      List.sort (fun (_,ev1,_) (_,ev2,_) -> compare ev1 ev2) events_list
    else
      List.rev(List.sort compare events_list) in (* most occurrences first *)
  let events_list =
    List.map
      (function (len,ev,infos) ->
	(len,ev,
	 List.rev
	   (List.fold_left (fun prev (tm,core,y) -> (tm,y) :: prev) [] infos)))
      core_events_list in
  List.iteri
    (fun i (startpoint,endpoint,redline) ->
      let fl =
	if !zoom_events = []
	then fl
	else
	  begin
	    O.min := startpoint;
	    O.max := endpoint;
	    Printf.sprintf "%s_min_%f_max%f" fl startpoint endpoint
	  end in
      let o =
	let endpoint =
	  if !zoom
	  then
	    let mostend =
	      Hashtbl.fold
		(fun ev l mostend ->
		  let (tm,core,y) = List.hd !l in
		  max mostend tm)
		events 0.0 in
	    mostend -. start
	  else endpoint in
	let printer = if !nograph then Printer.Nograph else !ty in
	let (intstr,ymx,ylabel) =
	  if !usecores
	  then ("",Printer.Nomax,"pid")
	  else if !interval < 0.0
	  then ("",Printer.Ymax (float_of_int !cores),"core")
	  else if !core_interval
	  then (" cores in interval: " ^ !interval_string,
		Printer.Ymax (float_of_int !cores),"cores")
	  else (" interval: " ^ !interval_string,Printer.Nomax,"number") in
	let ymn = if !usecores then None else Some 0 in
	Printer.startgraph fl printer start 8. (Some startpoint) (Some endpoint)
	  (Printf.sprintf "%s, time (sec), duration: %f%s" base (last -. start) intstr)
	  3. ymn ymx ylabel in
      (match redline with
	None -> ()
      | Some n ->
	  Printer.dots o [(n,0);(n,!cores)] (Printer.Color "red")
	    Printer.NoLabel None);
      let ctr = ref 0 in
      let ofile = open_out(Printf.sprintf "%s.events" fl) in
      let events_list = inrange events_list start startpoint endpoint in
      (if !exec
      then
	let exec_list = inrange_exec exec_trace start startpoint endpoint in
	match !ty with
	  Printer.ImPlot _ ->
	    let tbl = Hashtbl.create 101 in
	    List.iter
	      (fun (edge,pid) ->
		Hashtbl.replace tbl pid ((do_reorder edge) :: (try Hashtbl.find tbl pid with _ -> [])))
	      exec_list;
	    let exec_list =
	      Hashtbl.fold (fun pid edges r -> (pid,List.rev edges) :: r) tbl [] in
	    let exec_list = List.rev (List.sort compare exec_list) in
	    List.iter
	      (function (pid,edge) ->
		let concatenated = List.concat edge in
		(if List.sort compare concatenated <> concatenated
		then failwith (Printf.sprintf "not good for %d\n" pid));
		Printer.segments o concatenated (Printer.Choose pid) Printer.NoLabel
		  None)
	      exec_list
	| _ ->
	    List.iter
	      (function (edge,pid) ->
		Printer.edge o (do_reorder edge) (Printer.Choose pid) Printer.NoLabel
		  (if !O.save_tmp then (Some (string_of_int pid)) else None))
	      exec_list);
      (if !interval < 0.0
      then
	begin
	  let (numevents,events) =
	    List.partition (fun (ct,ev,infos) -> in_numev ev) events_list in
	  let total_events = List.fold_left (fun prev (ct,ev,infos) -> ct + prev) 0 events in
	  List.iter
	    (fun (ct,ev,infos) ->
	      let c = !ctr in
	      ctr := !ctr + 1;
	      let fn = Printer.markonly (PE.getmark c) in
	      let pct =
		if total_events = 0
		then ""
		else Printf.sprintf " = %d%%" ((ct * 100) / total_events) in
	      let str = Printf.sprintf "%s (%d%s)" ev ct pct in
	      Printf.fprintf ofile "%s\n" str;
	      if (ct > 0)
	      then fn o (do_reorder infos) (Printer.Choose c) (Printer.Label str) None)
	    events_list;
	  List.iter (* numevents come later, so superimposed *)
	    (fun (ct,ev,infos) ->
	      let c = !ctr in
	      ctr := !ctr + 1;
	      let fn = Printer.edge in
	      let str = Printf.sprintf "%s (%d)" ev ct in
	      Printf.fprintf ofile "%s\n" str;
	      if (ct > 0)
	      then
		let infos = List.sort compare infos in
		let rec loop acc = function
		    (t1,v1)::(t2,v2)::(t3,v3)::rest when v1 = v2 && v2 = v3 ->
		      loop acc ((t1,v1)::(t3,v3)::rest)
		  | x ::xs -> loop (x :: acc) xs
		  | [] -> List.rev acc in
		fn o (do_reorder (loop [] infos)) (Printer.Choose c) (Printer.Label str) None)
	    numevents
	end
      else
	List.iter
	  (fun (ct,ev,infos) ->
	    let c = !ctr in
	    ctr := !ctr + 1;
	    let l =
	      if !core_interval
	      then infos_to_core_interval start infos
	      else infos_to_interval start infos in
	    let str = Printf.sprintf "%s (%d)" ev ct in
	    Printf.fprintf ofile "%s\n" str;
	    if (ct > 0)
	    then Printer.edge o (do_reorder l) (Printer.Choose c) (Printer.Label str) None)
	  events_list);
      close_out ofile;
      Printer.endgraph o)
    (if !zoom_events = []
    then [(startpoint,endpoint,None)]
    else do_zoom core_events_list start base startpoint endpoint)
	
(* ----------------------------------------------------- *)
	
let endpoint = ref ("",0.0,-1.0)
let files = ref []

let parse_latency s =
  match String.split_on_char '-' s with
    [bef;aft] ->
      (match String.split_on_char ':' aft with
	[aft;range] -> (bef,aft,float_of_string range,range)
      | _ -> failwith "expect range for latency values")
  | _ -> failwith "expect start and end event for latency values"

let options =
  O.generic_options @
  PE.generic_options @
  ["--exec", Arg.Set exec, "  show execution lines";
    "--cores", Arg.Set usecores, "  y axis is pids, requires exec is false and numevents (-n) is false";
    "--zoom", Arg.Set zoom, "  show time up to last mark";
    "--interval",
    Arg.String
      (fun f ->
	interval := float_of_string f; interval_string := f; core_interval := false),
    "  count events per interval";
    "--core-interval",
    Arg.String
      (fun f ->
	interval := float_of_string f; interval_string := f; core_interval := true),
    "  count cores performing the event per interval";
    "-n", Arg.String (fun s -> numev := (Str.regexp_string s) :: !numev),
    "  event where the y coordinate is the provided numerical value";
    "--nograph", Arg.Set nograph, "  don't print a graph";
    "--live-pid",
    Arg.Int
      (function n ->
	requested_something := true;
	Hashtbl.add requested_live_pids n ();
	let (str,mn,mx) = !endpoint in
	endpoint := (Printf.sprintf "_%d%s" n str,mn,mx)),
    "  show everything starting when this pid is live";
    "--cpu", Arg.Int (fun n -> cpus := n :: !cpus), "  cpus of interest";
    "--ps", Arg.Unit (fun _ -> ty := JgraphPs),
    "  generate graphs in postscript (faster, but bigger files)";
    "--zoom-event",
    Arg.String
      (fun e -> zoom_events := (e,Str.regexp_string e) :: !zoom_events),
    "  zoom around an event";
    "--zoom-event-core",
    Arg.Int (fun e -> zoom_event_cores := e :: !zoom_event_cores),
    "  core to consider for event zoom (can be used more than once)";
    "--zoom-interval",
    Arg.Float (fun i -> zoom_pre_interval := i; zoom_post_interval := i),
    "  distance around an event at which to zoom";
    "--zoom-pre-interval",
    Arg.Float (fun i -> zoom_pre_interval := i),
    "  distance before an event at which to zoom";
    "--zoom-post-interval",
    Arg.Float (fun i -> zoom_post_interval := i),
    "  distance after an event at which to zoom";
    "--latency", Arg.String (fun s -> latency := parse_latency s :: !latency),
    "  latency between two events, modulo some value: eg --latency e1-e2:0.001";
    "--alpha", Arg.Set alpha, "  sort events alphabetically";
    "--socket-order", Arg.Set socket_order,
    "  reorganize cores to put cores on the same socket adjacent";
    "--implot", Arg.Unit (fun _ -> ty := Printer.ImPlot(3)),
    "  generate graphs for implot";
]

let anonymous s = files := Util.cmd_to_list (Printf.sprintf "ls %s" s) @ !files

let usage = "events file.dat [options]"

let _ =
  Arg.parse (Arg.align options) anonymous usage;
  (if !usecores && (!numev <> [] || !core_interval || !interval >= 0.)
  then
    failwith
      (Printf.sprintf
	 "--cores is not compatible with -n (%d), core interval (%b) and interval (%f)"
         (List.length !numev) !core_interval !interval));
  let events =
    if !requested_something || !exec
    then ["bprint";"bputs";"sched_process_exec";"sched_move_numa";
	   "sched_process_fork";"sched_switch";"syscalls";"function"]
    else ["nosched"] in
  let files =
    Util.get_files_and_endpoint !files !O.after_sleep false events in
  let data =
    List.map
      (function ((base,info) as fl) ->
	let (success,start,last,(events,latency_events,exec_trace,_)) = parse_all info !O.min !O.max in
	Util.remove 1 info;
	(fl,start,last,events,latency_events,exec_trace))
      files in
  List.iter main data
