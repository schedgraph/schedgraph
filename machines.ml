type color_table = int * (int * int * int * int * Printer.color) list

let mkfreq tbl =
  let (_,n,_) = List.hd (List.rev tbl) in
  let recode =
    List.map
      (function (low,high,color) ->
	(low,high,low + 1000,high + 1000,color))
      tbl in
  (n,recode)

(* orange is the range of 5+ cores per socket *)

let i80_freq_intervals = (* min_freq, max_freq, color *)
  mkfreq
    [(0,        1200000, Printer.RGB(0.549,0.815,0.960));
      (1200000, 1700000, Printer.RGB(0.607,0.937,0.498));
      (1700000, 2100000, Printer.RGB(0.839,0.929,0.427));
      (2100000, 2600000, Printer.RGB(0.878,0.619,0.141));
      (2600000, 3000000, Printer.RGB(0.878,0.141,0.141))]

let yeti_freq_intervals = (* min_freq, max_freq, color *)
  mkfreq
    [(0,        1000000, Printer.RGB(0.549,0.815,0.960));
      (1000000, 1600000, Printer.RGB(0.607,0.937,0.498));
      (1600000, 2100000, Printer.RGB(0.839,0.929,0.427));
      (2100000, 3400000, Printer.RGB(0.878,0.619,0.141));
      (3400000, 3700000, Printer.RGB(0.878,0.141,0.141))]

let troll_freq_intervals = (* min_freq, max_freq, color *)
  mkfreq
    [(0,        1000000, Printer.RGB(0.549,0.815,0.960));
      (1000000, 1600000, Printer.RGB(0.607,0.937,0.498));
      (1600000, 2300000, Printer.RGB(0.839,0.929,0.427));
      (2300000, 3600000, Printer.RGB(0.878,0.619,0.141));
      (3600000, 3900000, Printer.RGB(0.878,0.141,0.141))]

let w2155_freq_intervals = (* min_freq, max_freq, color *)
  mkfreq
    [(0,        1200000, Printer.RGB(0.549,0.815,0.960));
      (1200000, 2700000, Printer.RGB(0.607,0.937,0.498));
      (2700000, 3300000, Printer.RGB(0.839,0.929,0.427));
      (3300000, 4200000, Printer.RGB(0.878,0.619,0.141));
      (4200000, 4500000, Printer.RGB(0.878,0.141,0.141))]

let yeti_freq_intervals2 = (* min_freq, max_freq, color *)
  mkfreq
    [(0,        1000000, Printer.RGB(0.549,0.815,0.960));
      (1000000, 1600000, Printer.RGB(0.607,0.937,0.498));
      (1600000, 2100000, Printer.RGB(0.839,0.929,0.427));
      (2100000, 2800000, Printer.RGB(0.878,0.619,0.141));
      (2800000, 3100000, Printer.RGB(0.396,0.165,0.055));
      (3100000, 3400000, Printer.RGB(0.643,0.368,0.898));
      (3400000, 3700000, Printer.RGB(0.878,0.141,0.141))]

let troll_freq_intervals2 = (* min_freq, max_freq, color *)
  mkfreq
    [(0,        1000000, Printer.RGB(0.549,0.815,0.960));
      (1000000, 1600000, Printer.RGB(0.607,0.937,0.498));
      (1600000, 2300000, Printer.RGB(0.839,0.929,0.427));
      (2300000, 2800000, Printer.RGB(0.878,0.619,0.141));
      (2800000, 3100000, Printer.RGB(0.396,0.165,0.055));
      (3100000, 3600000, Printer.RGB(0.643,0.368,0.898));
      (3600000, 3900000, Printer.RGB(0.878,0.141,0.141))]

let gros_freq_intervals2 = (* min_freq, max_freq, color *)
  mkfreq
    [(0,        1000000, Printer.RGB(0.549,0.815,0.960));
      (1000000, 1600000, Printer.RGB(0.607,0.937,0.498));
      (1600000, 2200000, Printer.RGB(0.839,0.929,0.427));
      (2200000, 2800000, Printer.RGB(0.878,0.619,0.141));
      (2800000, 3100000, Printer.RGB(0.396,0.165,0.055));
      (3100000, 3600000, Printer.RGB(0.643,0.368,0.898));
      (3600000, 3900000, Printer.RGB(0.878,0.141,0.141))]

let amd4650g_freq_intervals2 = (* min_freq, max_freq, color *)
  mkfreq
    [(0,        1400000, Printer.RGB(0.549,0.815,0.960));
      (1400000, 1700000, Printer.RGB(0.607,0.937,0.498));
      (1700000, 3700000, Printer.RGB(0.839,0.929,0.427));
      (3700000, 4300000, Printer.RGB(0.878,0.619,0.141))] (* max should be 42, but numbers are well over 42 *)

let neowise_freq_intervals = (* min_freq, max_freq, color *)
  mkfreq
    [(0,        1500000, Printer.RGB(0.549,0.815,0.960));
      (1500000, 1700000, Printer.RGB(0.607,0.937,0.498));
      (1700000, 2300000, Printer.RGB(0.839,0.929,0.427));
      (2300000, 2600000, Printer.RGB(0.878,0.619,0.141));
      (2600000, 3300000, Printer.RGB(0.878,0.141,0.141))]

let contains_troll s =
  match Str.split_delim (Str.regexp "troll") s with
    a::b::c -> true
  | _ -> false

let four n = n mod 4
let two n = n mod 2
let one n = 0
let neosock n = failwith "not supported"

let get_freqtbl cores file =
  let yeti = ("yeti",(yeti_freq_intervals2,four)) in
  let dahu = ("dahu",(yeti_freq_intervals2,two)) in
  let troll = ("troll",(troll_freq_intervals2,two)) in
  let gros = ("gros",(gros_freq_intervals2,one)) in
  let ryzen = ("4650g",(amd4650g_freq_intervals2,one)) in
  let w2155 = ("w2155",(w2155_freq_intervals,one)) in
  let neowise = ("neowise",(neowise_freq_intervals,neosock)) in
  let servan = ("servan",(neowise_freq_intervals,two)) in
  let i80 = ("i80",(i80_freq_intervals,four)) in
  let all = [yeti;dahu;troll;gros;ryzen;w2155;neowise;servan;i80] in
  let choices =
    match cores with
      160 -> [i80]
    | 128 -> [yeti]
    | 96  -> [neowise]
    | 64  -> [dahu;troll]
    | 36  -> [gros]
    | 20  -> [w2155]
    | _   -> all in
  let rec loop = function
      [] -> snd(List.hd choices)
    | (machine,table)::rest ->
	try
	  ignore(Str.search_forward (Str.regexp machine) file 0);
	  table
	with _ -> loop rest in
  loop choices

let freq_to_color freq freq_intervals =
  List.fold_left
    (fun c1 (_,_,low,up,c2) -> if freq > low && freq <= up then c2 else c1)
    (Printer.Color "black")
    (snd freq_intervals)

let freq_to_hexcolor freq freq_intervals =
  let c =
    List.fold_left
      (fun c1 (_,_,low,up,c2) -> if freq > low && freq <= up then c2 else c1)
      (Printer.Color "black")
      (snd freq_intervals) in
  let hexcolor v =
    let v = v *. 255. in
    int_of_float v in
  match c with
    Printer.RGB(r,g,b) ->
      Printf.sprintf "#%02x%02x%02x" (hexcolor r) (hexcolor g) (hexcolor b)
  | Printer.Color("black") ->
      Printf.sprintf "#%02x%02x%02x" (hexcolor 0.) (hexcolor 0.) (hexcolor 0.)
  | _ -> failwith "not defined for hexcolor"
