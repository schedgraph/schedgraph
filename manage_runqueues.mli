(* SPDX-License-Identifier: GPL-2.0 *)
type states = Running | RunningSleep | RunningYield | Waiting | Pending
type direction = Src of states | Dst of states

val init :
    int ->
      (states list * int list array) * (states list * int list array) *
	(states list * int list array)

val process_line :
    float -> int -> (direction, float -> int -> int -> unit) Hashtbl.t list ->
      (states list * int list array) * (states list * int list array) *
	(states list * int list array) ->
	  Parse_line.events -> unit
