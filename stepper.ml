(* SPDX-License-Identifier: GPL-2.0 *)
(* Step through an execution starting from some point.  Requires the ocaml
curses library, available via opam. *)

module O = Options

let cores = ref 160
let sockets = ref 4
let cps _ = !cores / !sockets
let width = ref 0
let freerun = ref false
let step = ref 0
type dir = FWD | BACK

let core_width = 4
let pid_width = 8+3

(* ------------------------------------------------------------------ *)

let red = 1
let green = 2
let blue = 6
let white = 7

let c2s = function
    1 -> "red"
  | 2 -> "green"
  | 6 -> "blue"
  | 7 -> "white"
  | _ -> failwith "not a color"

let next_color = function
    Some 2 -> Some(Some 7)
  | Some 6 -> Some(Some 7)
  | Some 1 -> Some None
  | Some 7 -> None
  | None -> None
  | _ -> failwith "not possible"

let fail s =
  Curses.endwin();
  failwith s

let curses ok =
  if ok
  then ()
  else fail "curses failed"

let setup_colors _ =
  curses(Curses.start_color());
  List.iter
    (fun color -> curses(Curses.init_pair color color 0))
    [red;green;blue;white]

let core2coord core =
  (core / !sockets, core mod !sockets * (!width / !sockets))

let todo = ref []

let write_at o core offset color s =
  let (ypos,xpos) = core2coord core in
  let xpos = xpos + offset in
  match color with
    Some n ->
      Curses.attron (Curses.A.color_pair n);
      (if xpos <= !width then
      curses(Curses.mvaddstr ypos xpos s));
      Curses.attroff (Curses.A.color_pair n)
  | None ->
      let s = String.make (String.length s) ' ' in
      (if xpos <= !width then
      curses(Curses.mvaddstr ypos xpos s))

let do_write_at o core offset color s =
  List.iter (fun x -> x()) !todo;
  todo := [];
  write_at o core offset color s;
  match next_color color with
    Some c -> [fun _ -> write_at o core offset c s]
  | None -> []

(* ------------------------------------------------------------------ *)

let tracking pid = pid > 0

let string_of_pid pid cmd =
  if String.length cmd < 2
  then cmd^(string_of_int pid)
  else (String.sub cmd 0 2)^(string_of_int pid)

let add_running_string o pid cmd time core corestate startpoint =
  let (running,waiting,offset) = Array.get corestate core in
  (match running with
    None -> Array.set corestate core (Some (pid,cmd),waiting,offset)
  | Some _-> fail "overwrite running");
  if time >= startpoint
  then
    begin
      (* restore the core number and |, just in case *)
      let (ypos,xpos) = core2coord core in
      curses(Curses.mvaddstr ypos xpos (string_of_int core));
      curses(Curses.mvaddstr ypos (xpos+core_width+pid_width-1) "|");
      (* print the actual pid *)
      let s = string_of_pid pid cmd in
      do_write_at o core core_width (Some green) s
    end
  else []

let remove_running_string o pid cmd time core corestate startpoint =
  let (running,waiting,offset) = Array.get corestate core in
  (match running with
    Some (p,_) when p = pid ->
      Array.set corestate core (None,waiting,offset)
  | _ -> ());
  if time >= startpoint
  then
    let s = string_of_pid pid cmd in
    do_write_at o core core_width (Some red) s
  else []

(* offset at top level is the end of the previous string
   offset at the pid level is the start of that pid *)

let shift_by pid offset delta waiting =
  let filter_pid fn waiting =
    List.filter (fun (p,(s,o)) -> fn p o) waiting in
  let bef = filter_pid (fun p o -> p <> pid && o < offset) waiting in
  let aft = filter_pid (fun p o -> p <> pid && o > offset) waiting in
  let newaft =
    List.map (fun (p,(str,o)) -> (p,(str,o-delta))) aft in
  (bef,newaft)

let add_waiting_string o pid s color time core corestate startpoint =
  let (running,waiting,offset) = Array.get corestate core in
  if time < startpoint
  then
    begin
      let waiting = List.filter (fun (p,_) -> p <> pid) waiting in
      Array.set corestate core (running,(pid,(s,-1)) :: waiting,offset);
      []
    end
  else
    begin
      let info =
	try Some(List.assoc pid waiting) (* already present due to migration *)
	with Not_found -> None in (* not yet present due to direct arrival *)
      match info with
	Some (str,myoffset) ->
	  if str = s
	  then do_write_at o core myoffset (Some color) s
	  else
	    begin
	      let delta = String.length str - String.length s in
	      let (bef,aft) = shift_by pid myoffset delta waiting in
	      let new_waiting = bef @ [(pid,(s,myoffset))] @ aft in
	      Array.set corestate core (running,new_waiting,offset - delta);
	      ignore(do_write_at o core myoffset None (String.make (offset-myoffset) ' '));
	      List.iter (fun (p,(s,offset)) -> write_at o core offset (Some white) s) aft;
	      do_write_at o core myoffset (Some color) s
	    end
      | None -> (* start waiting from scratch *)
	  let myoffset = offset + 1 in
	  let new_offset = myoffset + String.length s in
	  Array.set corestate core (running,(pid,(s,myoffset)) :: waiting,new_offset);
	  do_write_at o core myoffset (Some color) s
    end

let remove_waiting_string o pid color time core corestate startpoint =
  let (running,waiting,offset) = Array.get corestate core in
  let info =
    try Some(List.assoc pid waiting)
    with Not_found -> None in (* missing for some reason *)
  match info with
    None -> []
  | Some (s,myoffset) when time >= startpoint ->
      let delta = String.length s + 1 in
      let (bef,aft) = shift_by pid myoffset delta waiting in
      let new_waiting = bef @ aft in
      Array.set corestate core (running,new_waiting,offset - delta);
      ignore(do_write_at o core myoffset color s);
      if color = None
      then
	begin
	  write_at o core myoffset None (String.make (offset-myoffset) ' ');
	  List.iter (fun (p,(s,offset)) -> write_at o core offset (Some white) s) aft;
	  []
	end
      else
	[fun _ ->
	  write_at o core myoffset None (String.make (offset-myoffset) ' ');
	  List.iter (fun (p,(s,offset)) -> write_at o core offset (Some white) s) aft]
  | Some _ ->
      let new_waiting =
	List.filter (fun (p,_) -> p <> pid) waiting in
      Array.set corestate core (running,new_waiting,offset);
      []

type aa = AARun of int | AAWait of int * string * int | NA
let already_awake corestate pid =
    let aa = ref NA in
    Array.iteri
      (fun i (running,waiting,_) ->
	(if !aa = NA
	then
	  (match running with
	    Some(r,_) ->
	      if r = pid
	      then aa := AARun i
	  | _ -> ()));
	(if !aa = NA
	then
	  try
	    let (str,offset) = List.assoc pid waiting in
	    if String.get str 0 = '('
	    then () (* not a real wait, just a wake *)
	    else aa := AAWait(i,str,offset)
	  with Not_found -> ()))
      corestate;
    !aa

let process_line o dir time core corestate startpoint l =
  match l with
    Parse_line.Sched_process_exec(cmd,oldcmd,pid,oldpid) ->
      (if dir = BACK then fail "back not supported");
      if tracking pid
      then
	begin
	  let cmd = Filename.basename cmd in
	  let f1 = remove_running_string o oldpid oldcmd time core corestate startpoint in
	  let f2 = add_running_string o pid cmd time core corestate startpoint in
	  f1 @ f2
	end
      else []
  | Parse_line.Sched_switch(fromcmd,frompid,state,tocmd,topid) ->
      let (fromcmd,frompid,state,tocmd,topid) =
	if dir = FWD
	then (fromcmd,frompid,state,tocmd,topid)
	else (tocmd,topid,Parse_line.Yield,fromcmd,frompid) in
      let f1 =
	if tracking frompid
	then
	  let f1 = remove_running_string o frompid fromcmd time core corestate startpoint in
	  let f2 =
	    if state = Parse_line.Yield
	    then
	      add_waiting_string o frompid (string_of_pid frompid fromcmd) green
		time core corestate startpoint
	    else [] in
	  f1 @ f2
	else [] in
      let f2 =
	if tracking topid
	then
	  begin
	    let f1 = remove_waiting_string o topid (Some red) time core corestate startpoint in
	    let f2 = add_running_string o topid tocmd time core corestate startpoint in
	    f1 @ f2
	  end
	else [] in
      f1 @ f2
  | Parse_line.Sched_waking(_actor_cmd,_actor_pid,cmd,pid,cpu,_) when time > startpoint ->
      (match already_awake corestate pid with
	AARun i -> (* already running, so nothing to do *)
	  if time >= startpoint
	  then
	    let s = string_of_pid pid cmd in
	    do_write_at o i core_width (Some green) s
	  else []
      | AAWait(i,str,offset) ->
	  if time >= startpoint
	  then do_write_at o i offset (Some green) str
	  else []
      | NA ->
	  if dir = FWD
	  then
	    let s = Printf.sprintf "(wake %s -> %d)" (string_of_pid pid cmd) cpu in
	    add_waiting_string o pid s blue time core corestate startpoint
	  else remove_waiting_string o pid None time core corestate startpoint)
  | Parse_line.Sched_wakeup(cmd,pid,prevcpu,cpu) ->
      (match already_awake corestate pid with
	AARun i -> (* already running, so nothing to do *)
	  if time >= startpoint
	  then
	    let s = string_of_pid pid cmd in
	    do_write_at o i core_width (Some green) s
	  else []
      | AAWait(i,str,offset) ->
	  if time >= startpoint
	  then do_write_at o i offset (Some green) str
	  else []
      | NA ->
	  if dir = FWD
	  then
	    let f1 =
	      match prevcpu with
		None -> []
	      | Some core ->
		  remove_waiting_string o pid None time core corestate startpoint in
	    let s = string_of_pid pid cmd in
	    let f2 =  add_waiting_string o pid s green time cpu corestate startpoint in
	    f1 @ f2
	  else (* don't know where the thread was; use prev cpu -
		  it was there at some time in the past *)
	    let f1 =  remove_waiting_string o pid (Some red) time cpu corestate startpoint in
	    let f2 =
	      match prevcpu with
		None -> []
	      | Some prevcpu ->
		  let s = Printf.sprintf "(wake %s -> %d)" (string_of_pid pid cmd) cpu in
		  add_waiting_string o pid s green time prevcpu corestate startpoint in
	    f1 @ f2)
  | Parse_line.Sched_process_fork(_,_parent_pid,cmd,pid) when time > startpoint ->
      if dir = FWD
      then
	let s = Printf.sprintf "(fork %s -> %d)" (string_of_pid pid cmd) core in
	add_waiting_string o pid s blue time core corestate startpoint
      else remove_waiting_string o pid None time core corestate startpoint
  | Parse_line.Sched_wakeup_new(cmd,pid,parent,cpu) ->
      if dir = FWD
      then
	let f1 = remove_waiting_string o pid None time parent corestate startpoint in
	let s = string_of_pid pid cmd in
	let f2 = add_waiting_string o pid s green time cpu corestate startpoint in
	f1 @ f2
      else
	let f1 = remove_waiting_string o pid (Some red) time cpu corestate startpoint in
	let s = Printf.sprintf "(fork %s -> %d)" (string_of_pid pid cmd) core in
	let f2 = add_waiting_string o pid s green time parent corestate startpoint in
	f1 @ f2
  | Parse_line.Sched_migrate_task(cmd,pid,oldcpu,newcpu,state) ->
      if dir = FWD
      then
	let (oldcpu,color) =
	  match state with
	    Waking _ ->
	      (match already_awake corestate pid with
		AARun i | AAWait(i,_,_) -> (i,Some red)
	      | _ -> (core,None)) (* oldcpu is the desired one, not the actual one *)
	  | _ -> (oldcpu,Some red) in
	let f1 = remove_waiting_string o pid color time oldcpu corestate startpoint in
	let f2 =
	  add_waiting_string o pid (string_of_pid pid cmd) green time newcpu
	    corestate startpoint in
	f1 @ f2
      else
	let (str,oldcpu,color) =
	  match state with
	    Waking _ -> (* unreliable *)
	      (Printf.sprintf "(wake %s -> %d)" (string_of_pid pid cmd) oldcpu,core,blue)
	  | _ -> (string_of_pid pid cmd,oldcpu,green) in
	let f1 = remove_waiting_string o pid (Some red) time newcpu corestate startpoint in
	let f2 =
	  add_waiting_string o pid str color time oldcpu
	    corestate startpoint in
	f1 @ f2
  | _ -> []

let initialize o corestate =
  Array.iteri
    (fun core (running,waiting,_) ->
      (match running with
	None -> ()
      | Some (pid,cmd) ->
	  let s = string_of_pid pid cmd in
	  ignore(do_write_at o core core_width (Some white) s));
      let (offset,waiting) =
	List.fold_left
	  (fun (offset,infos) (pid,(cmd,_)) ->
	    let s = string_of_pid pid cmd in
	    let myoffset = offset + 1 in
	    ignore(do_write_at o core myoffset (Some white) s);
	    (myoffset + String.length s,(pid,(s,myoffset))::infos))
	  (core_width+pid_width,[]) waiting in
      Array.set corestate core (running,waiting,offset))
    corestate

let validate time corestate mintime =
  let seen = ref [] in
  let dofail str pid core pcore =
    let str =
      Printf.sprintf "%f %s pid %d on core %d already seen on %d"
	time str pid core pcore in
(*    if !freerun
    then fail str
    else*)
      begin
	curses(Curses.move (cps()+3) 0);
	curses(Curses.deleteln());
	curses(Curses.addstr str);
	if (time >= mintime) then ignore(Curses.getch())
      end in
  Array.iteri
    (fun core (running,waiting,_) ->
      (match running with
	None -> ()
      | Some (pid,cmd) ->
	  try
	    let pcore = List.assoc pid !seen in
	    dofail "running" pid core pcore
	  with Not_found -> seen := (pid,core) :: !seen);
      List.iter
	(fun (pid,_) ->
	  try
	    let pcore = List.assoc pid !seen in
	    dofail "waiting" pid core pcore
	  with Not_found -> seen := (pid,core) :: !seen)
	waiting)
    corestate

(* ------------------------------------------------------------------ *)

let print_cores o =
  for i = 0 to !cores - 1 do
    let (ypos,xpos) = core2coord i in
    curses(Curses.mvaddstr ypos xpos (string_of_int i));
    curses(Curses.mvaddstr ypos (xpos+core_width+pid_width-1) "|")
  done

let parse_all o w ctx startpoint =
  cores := ctx.Util.cores;
  let start = ctx.Util.start in
  let corestate = Array.make !cores (None,[],core_width+pid_width) in
  let elements =
    ["sched_switch";"sched_process_exec";"sched_process_exit";
      "sched_waking";"sched_wakeup";"sched_process_fork";"sched_wakeup_new";
      "sched_migrate_task"] in
  let initialized = ref false in
  let i = Util.skip_front_continue ctx in (* # cpus *)
  (if !cores = 64 then sockets := 2
  else if !cores < 40 then sockets := 1);
  print_cores o;
  let startpoint =
    if startpoint < start then startpoint +. start else startpoint in
  (if !cores > 256 then failwith "no more than 256 cores supported");
  let rec loop n expected dir beforestack afterstack =
    curses(Curses.move (cps()+2) 0);
    curses(Curses.deleteln());
    (* get the next line *)
    let (info,beforestack,afterstack) =
      match dir with
	FWD ->
	  (match afterstack with
	    [] ->
	      let l = input_line i in
	      let res = Parse_line.parse_line l false elements in
	      (Some (res,l),l::beforestack,[])
	  | x::xs ->
	      let res = Parse_line.parse_line x false elements in
	      (Some (res,x),x::beforestack,xs))
      | BACK ->
	  (match beforestack with
	    [] -> (None,beforestack,afterstack)
	  | x::xs ->
	      let res = Parse_line.parse_line x false elements in
	      (Some (res,x),xs,x::afterstack)) in
    (* process the event *)
    let time =
      match info with
	Some((core,time,e),l) ->
	  let time =
	    match e with
	      Parse_line.Not_supported(c,t,cm,a) -> float_of_string t
	    | _ -> time in
	  (if time >= startpoint then curses(Curses.addstr l));
	  (if not !initialized && time >= startpoint
	  then
	    begin
	      initialized := true;
	      initialize o corestate
	    end);
	  todo := !todo @ process_line o dir time core corestate startpoint e;
	  curses(Curses.refresh());
	  validate time corestate startpoint;
	  time
      | None -> start in
    (* reading the input for the next iteration *)
    let rec read _ =
      let chr =
	try Some(char_of_int(Curses.getch()))
	with _ -> None in
      match chr with
	Some 'n' ->
	  freerun := false;
	  (if !step > 0
	  then curses(Curses.nodelay w true));
	  loop 0 None FWD beforestack afterstack
      | Some 'b' ->
	  freerun := false;
	  (if !step > 0
	  then curses(Curses.nodelay w true));
	  loop 0 None BACK beforestack afterstack
      | Some ' ' ->
	  freerun := true;
	  curses(Curses.nodelay w true);
	  loop 0 None dir beforestack afterstack
      | Some 's' ->
	  curses(Curses.echo());
	  curses(Curses.move (cps()+3) 0);
	  curses(Curses.deleteln());
	  let str = String.make !width ' ' in
	  curses(Curses.getstr str);
	  curses(Curses.noecho());
	  let str = String.trim str in
	  let str = String.sub str 0 (String.length str - 1) in
	  freerun := true;
	  curses(Curses.nodelay w true);
	  loop 0 (Some (Str.regexp str)) dir beforestack afterstack
      | Some 'q' -> ()
      | _ -> read() in
    let poll n =
      let c = Curses.getch() in
      if 0 <= c && c <= 255
      then
	begin
	  curses(Curses.nodelay w false);
	  read()
	end
      else loop n expected dir beforestack afterstack in
    if time < startpoint
    then loop 0 None dir beforestack afterstack
    else
      match expected with
	None ->
	  if !freerun
	  then poll 0
	  else if !step > 0 && n < !step
	  then poll (n+1)
	  else
	    begin
	      (if !step > 0 then curses(Curses.nodelay w false));
	      read()
	    end
      | Some str ->
	  (try
	    (match info with
	      Some(_,l) ->
		ignore(Str.search_forward str l 0);
		curses(Curses.nodelay w false);
		read()
	    | None ->
		poll (n+1))
	  with Not_found -> poll (n+1)) in
  try loop 0 None FWD [] [] with End_of_file -> ()

(* ------------------------------------------------------------------ *)

let startpoint = ref 0.0
let file = ref ""

let options = [
  "--cores", Arg.Set_int cores, "  number of cores";
  "--sockets", Arg.Set_int sockets, "  number of sockets";
  "--min", Arg.String (fun s -> startpoint := float_of_string s),
  "  start time";
  "--freerun", Arg.Set freerun, "  run without stopping";
  "--step", Arg.Set_int step, "  steps to run before stopping";
  "--after-sleep", Arg.Set O.after_sleep,
  "  skip the first sleep in the execution"
]

let anonymous s = file := s

let usage = "stepper file.dat [options]"

let _ =
  let o = open_out "stepper_tmp" in
  Arg.parse (Arg.align options) anonymous usage;
  let w = Curses.initscr() in
  curses(Curses.keypad w true); (* stdscr() ? *)
  curses(Curses.cbreak());
  curses(Curses.noecho());
  (if !freerun || !step > 0 then curses(Curses.nodelay w true));
  setup_colors();
  let (y,x) = Curses.get_size() in
  width := x;
  let files = Util.get_files_and_endpoint [!file] !O.after_sleep false [] in
  let (_base,ctx) = List.hd files in
  parse_all o w ctx !startpoint;
  Curses.endwin();
  close_out o;
  Util.remove 0 ctx
