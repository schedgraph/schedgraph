(* SPDX-License-Identifier: GPL-2.0 *)
module O = Options

type ty = Jgraph | JgraphPs | Latex | Latex2 | Python | Nograph | ImPlot of int
type edges = Solid | Segments | Dotted | Longdash | NoEdge
type graph =
    out_channel * string * ty * float *
      float option * float option * string list ref

let clean debug s =
  let s = String.concat "\\_" (Str.split_delim (Str.regexp "_") s) in
  let s = String.concat "\\#" (Str.split_delim (Str.regexp "#") s) in
  let s = String.concat "\\%" (Str.split_delim (Str.regexp "%") s) in
  let s = String.concat "\\&" (Str.split_delim (Str.regexp "&") s) in
  let s = String.concat "$>$" (Str.split_delim (Str.regexp ">") s) in
  let s = String.concat "$<$" (Str.split_delim (Str.regexp "<") s) in
  let s = String.concat "$\\mid$" (Str.split_delim (Str.regexp "|") s) in
  if String.length s > 50
  then Printf.sprintf "{\\footnotesize %s}" s
  else s

let latexclean s = clean "x" s

let tostring s = Printf.sprintf "\"%s\"" s

let latex2colornames = Hashtbl.create 101 (* used later *)
let namectr = ref 0

type ymax =
    Nomax | Ymax of float | Yminmax of float * float | Ylist of int list
  | Ymap of (int * string) list

let startgraph file ty time xsize xmin xmax xlabel ysize ymin ymax ylabel =
  Random.self_init();
  let fl =
    match ty with
      Jgraph | JgraphPs -> file ^ ".jgr"
    | Latex | Latex2 -> file ^ ".tex"
    | Python -> file ^ ".json"
    | ImPlot _ -> file ^ "_implot.cpp"
    | Nograph -> "" in
  let o =
    if ty = Nograph then stdout else open_out fl in
  let obj = (o,file,ty,time,xmin,xmax,ref []) in
  (match ty with
    Nograph -> ()
  | Jgraph | JgraphPs ->
      let xmin =
	match xmin with
	  None -> ""
	| Some xmin -> Printf.sprintf " min %f" xmin in
      let xmax =
	match xmax with
	  None -> ""
	| Some xmax -> Printf.sprintf " max %f" xmax in
      let ymin =
	match ymin with
	  None -> ""
	| Some m -> Printf.sprintf " min %d" (m-1) in
      let ymaxv =
	match ymax with
	  Nomax -> ""
	| Ymax m -> Printf.sprintf " max %f" m
	| Yminmax (mn,mx) -> Printf.sprintf " min %f max %f" mn mx
	| Ylist l -> Printf.sprintf " max %d" (List.length l)
	| Ymap l  -> Printf.sprintf " min 0 max %d" (List.length l) in
      Printf.fprintf o
	"newgraph xaxis size %fin%s%s label : %s\nyaxis size %fin%s%s label : %s\n\n"
	xsize xmin xmax xlabel ysize ymin ymaxv ylabel;
      (match ymax with
	Ylist l ->
	  Printf.fprintf o "no_auto_hash_labels\n";
	  List.iteri (fun i x -> Printf.fprintf o "hash_label at %d : %d\n" i x) l;
	  Printf.fprintf o "\n"
      | Ymap l ->
	  Printf.fprintf o "no_auto_hash_labels\n";
	  List.iter (fun (i,x) -> Printf.fprintf o "hash_label at %d : %s\n" i x) l;
	  Printf.fprintf o "\n"
      | _ -> ())
  | Latex ->
      let xmin =
	match xmin with
	  None -> ""
	| Some xmin -> Printf.sprintf ",xmin=%f" xmin in
      let xmax =
	match xmax with
	  None -> ""
	| Some xmax -> Printf.sprintf ",xmax=%f" xmax in
      let ymin =
	match ymin with
	  None -> ""
	| Some m -> Printf.sprintf ",ymin=%d" (m-1) in
      let ymax =
	match ymax with
	  Nomax -> ""
	| Ymax m -> Printf.sprintf ",ymax=%f" m
	| _ -> failwith "not supported1" in
      let xlabel = latexclean xlabel in
      let ylabel = latexclean ylabel in
      Printf.fprintf o "\\documentclass{article}\n";
      Printf.fprintf o "\\usepackage{xcolor}\n";
      Printf.fprintf o "\\usepackage[hidelinks]{hyperref}\n";
      Printf.fprintf o "\\usepackage{fullpage}\n";
      Printf.fprintf o "\\usepackage{pgfplots}\n";
      Printf.fprintf o "\\pgfplotsset{compat=newest}\n";
      Printf.fprintf o "\\usetikzlibrary{positioning, decorations.markings}\n";
      Printf.fprintf o "\\hypersetup{hidelinks}\n";
      Printf.fprintf o "\\pagestyle{empty}\n";
      Printf.fprintf o "\\begin{document}\n";
      Printf.fprintf o "\\noindent\\begin{tikzpicture}[define rgb/.code={\\definecolor{mycolor}{RGB}{#1}},rgb color/.style={define rgb={#1},mycolor}]\n";
      Printf.fprintf o "\\begin{axis}[xlabel={%s},ylabel={%s},clip=false\n"
	xlabel ylabel;
      Printf.fprintf o "     %s%s%s%s,width=%fin,height=%fin]\n"
	xmin xmax ymin ymax 6.5(*xsize*) ysize
  | Latex2 ->
      (match xmax with
	Some xmax ->
	  Hashtbl.clear latex2colornames;
	  namectr := 0;
(*        let xlabel = latexclean xlabel in
          let ylabel = latexclean ylabel in*)
	  Printf.fprintf o "\\documentclass{article}\n";
	  Printf.fprintf o "\\usepackage{xcolor}\n";
	  Printf.fprintf o "\\usepackage[hidelinks]{hyperref}\n";
	  Printf.fprintf o "\\usepackage{fullpage}\n";
	  Printf.fprintf o "\\usepackage[landscape]{geometry}\n";
	  Printf.fprintf o "\\hypersetup{hidelinks}\n";
	  Printf.fprintf o "\\pagestyle{empty}\n";
	  Printf.fprintf o "\\begin{document}\n"; (* width * height *)
	  Printf.fprintf o "\\noindent\\begin{picture}(%f,%f)\n"
	    (xmax *. 1000.)
	    (match ymax with
	      Nomax -> 100.
	    | Ymax x -> x
	    | _ -> failwith "not supported2")
      | None -> failwith "xmax required")
  | Python ->
      Printf.fprintf o "{\"setup\": { \"XLabel\": %s,\"YLabel\": %s,\"Width\": %f,\"Height\": %f,"
	(tostring xlabel) (tostring ylabel) xsize ysize;
      (match (xmin,xmax) with
	(None,None) -> Printf.fprintf o " \"Xmin\": -1, \"Xmax\": -1 },\n"
      | (Some xmin,Some xmax) -> Printf.fprintf o "\"Xmin\": %f,\"Xmax\": %f }\n" xmin xmax
      | _ -> failwith "only xmin or only xmax not supported");
      (match (ymin,ymax) with
	(None,Nomax) -> Printf.fprintf o " \"Ymin\": -1, \"Ymax\": -1 },\n"
      | (Some ymin,Ymax ymax) -> Printf.fprintf o "\"Ymin\": %d,\"Ymax\": %f }\n" ymin ymax
      | _ -> failwith "only ymin or only ymax not supported");
      Printf.fprintf o "\"data\": [\n"
  | ImPlot lineweight ->
      let clean s =
	let s = String.concat "" (String.split_on_char '\n' s) in
	let s = String.concat "" (String.split_on_char '\\' s) in
	s in
      let xlabel = clean xlabel in
      let (ymin,ymax) =
	match (ymin,ymax) with
	  (None,Nomax) -> (-1,-1.)
	| (Some ymin,Ymax ymax) -> (ymin-1, ymax)
	| _ -> failwith "only ymin or only ymax not supported" in
      Printf.fprintf o "#include <implot.h>\n\n";
      Printf.fprintf o "extern \"C\" void module_setup(void (*setup)(const char *test, const char *toplabel, const char *xlabel, const char *ylabel, int ymin, double ymax, ImPlotLocation legendplace, int lineweight)) {\n";
      Printf.fprintf o "  setup(\"%s\",\"%s\",\"time (seconds)\",\"%s\",%d,%f,ImPlotLocation_East,%d);\n"
	(List.hd(String.split_on_char ' ' xlabel)) xlabel ylabel ymin ymax lineweight;
      Printf.fprintf o "}\n";
      Printf.fprintf o "extern \"C\" void module_data(void (*setcolor)(float r, float g, float b),void (*setmark)(ImPlotMarker marker),void (*plotline)(const char* label_id, const double* xs, const double* ys, int count, ImPlotLineFlags flags),void (*plotscatter)(const char* label_id, const double* xs, const double* ys, int count, ImPlotLineFlags flags),void (*plotstairs)(const char* label_id, const double* xs, const double* ys, int count, ImPlotLineFlags flags)) {\n");
  obj

let extract_o (o,file,ty,_,_,_,labelled) = o

let endgraph (o,file,ty,_,_,_,labelled) =
  (match ty with
    Nograph | Jgraph | JgraphPs -> ()
  | Latex ->
      Printf.fprintf o "\\legend{%s}\n"
	(String.concat "," (List.rev !labelled));
      Printf.fprintf o "\\end{axis}\n";
      Printf.fprintf o "\\end{tikzpicture}\n";
      Printf.fprintf o "\\end{document}\n"
  | Latex2 ->
      Printf.fprintf o "\\end{picture}\n";
      Printf.fprintf o "\\end{document}\n"
  | Python ->
      Printf.fprintf o "{ \"LineType\": \"none\",\n";
      Printf.fprintf o "  \"MarkType\": \"none\",\n";
      Printf.fprintf o "  \"Arrow\": false,\n";
      Printf.fprintf o "  \"Red\": 0,\n";
      Printf.fprintf o "  \"Green\": 0,\n";
      Printf.fprintf o "  \"Blue\": 0,\n";
      Printf.fprintf o "  \"Label\": \"none\",\n";
      Printf.fprintf o "  \"HoverLabel\": \"none\",\n";
      Printf.fprintf o "  \"X\": [],\n";
      Printf.fprintf o "  \"Y\": [] }\n";
      Printf.fprintf o "]}\n"
  | ImPlot _ -> Printf.fprintf o "}\n");
  (if ty = Nograph then () else close_out o);
  (match ty with
    Nograph -> ()
  | Jgraph ->
      let cmd = Printf.sprintf "TMPDIR=%s jgraph %s.jgr | TMPDIR=%s ps2eps -q -l -r 600  | TMPDIR=%s epstopdf --filter > %s.pdf"
        !O.tmpdir file !O.tmpdir !O.tmpdir file in
      ignore (Sys.command cmd);
      let jg = file ^ ".jgr" in
      (if !O.save_tmp
      then Printf.eprintf "%s\n" jg
      else Sys.remove jg);
      ignore
	(Sys.command
	   (Printf.sprintf "ln -sf %s.pdf current_graph.pdf" file))
  | JgraphPs ->
      ignore
	(Sys.command
	   (Printf.sprintf "jgraph %s.jgr > %s.ps" file file));
      let jg = file ^ ".jgr" in
      (if !O.save_tmp
      then Printf.eprintf "%s\n" jg
      else Sys.remove jg);
      ignore
	(Sys.command
	   (Printf.sprintf "ln -sf %s.ps current_graph.ps" file))
  | Latex | Latex2 ->
      ignore (Sys.command ("pdflatex " ^ file));
      if not !O.save_tmp
      then ignore (Sys.command ("/bin/rm " ^ file ^ ".tex"))
  | Python ->
(*      let front = Filename.dirname(Array.get Sys.argv 0) in
      let cmd =
        Printf.sprintf "python3 %s/print_graph.py %s.json"
	  front file in
      ignore (Sys.command cmd);*)
      if not !O.save_tmp
      then ignore (Sys.command ("/bin/rm " ^ file ^ ".json"))
  | ImPlot _ ->
      let source = Filename.dirname (Array.get Sys.argv 0) in
      let cmd =
	Printf.sprintf
	  "g++ -std=c++11 -I%s/implot -I%s/imgui -g -Wall -Wformat -shared -rdynamic -fPIC -o %s_implot.so %s_implot.cpp"
	  source source file file in
      ignore (Util.sys_command cmd);
      (if not !O.save_tmp
      then ignore (Sys.command ("/bin/rm " ^ file ^ "_implot.cpp")));
      ignore
	(Sys.command
	   (Printf.sprintf "ln -sf %s_implot.so current_graph.so" file)))

(* ------------------------------------------------------- *)

type color =
    RGB of float * float * float | Choose of int | SChoose of string
  | Color of string | RandomColor

let colors =
  let l =
    Array.of_list
      [RGB(0.0,0.0,1.0);RGB(0.0,1.0,0.0);RGB(1.0,0.0,0.0);
	RGB(1.0,0.0,1.0);RGB(0.7,0.3,0.0);RGB(0.0,1.0,1.0);
	RGB(0.8,0.0,0.8);RGB(0.0,0.6,0.6);RGB(0.4,0.4,0.0);
	RGB(1.0,0.5,0.0)] in
  ref (Array.length l,l)

let get_color n = Array.get (snd !colors) (n mod (fst !colors))

let command_color = Hashtbl.create 101
let command_count : (string,int * int ref) Hashtbl.t = Hashtbl.create 101
let cc = ref 0

let set_command_count cmd n =
  Hashtbl.add command_count cmd (n,ref 0)

let inc_command_count cmd =
  let cell =
    try Hashtbl.find command_count cmd
    with Not_found ->
      let cell = (0,ref 0) in
      Hashtbl.add command_count cmd cell;
      cell in
  snd cell := 1 + !(snd cell)

let get_command_count cmd =
  try !(snd(Hashtbl.find command_count cmd))
  with Not_found -> 0

let command_to_color s assigning =
  try Hashtbl.find command_color s
  with Not_found ->
    let c = get_color !cc in
    Hashtbl.add command_color s c;
    cc := !cc + 1;
    c

let assign_colors _ =
  let l = Hashtbl.fold (fun k v r -> ((fst v,!(snd v)),k)::r) command_count [] in
  let l = List.rev(List.sort compare l) in
  let mx = fst !colors in
  let rec loop n = function
      [] -> ()
    | ((_,k)::rest) as all ->
	if n > 0
	then (ignore(command_to_color k true); loop (n-1) rest)
	else
	  List.iter
	    (fun (_,k) -> Hashtbl.add command_color k (Color "black"))
	    all in
  loop mx l

let ncolors n =
  let rec loop x =
    if (x * x * x) - 1 >= n
    then x
    else loop (x+1) in
  let count = loop 2 in
  let options =
    let rec loop x =
      if x = count then [] else x :: loop (x+1) in
    loop 0 in
  let increment =
    1. /. (float_of_int(count - 1)) in
  let rec perm = function
      [] -> [[]]
    | x::xs ->
	let xs = perm xs in
	let rec loop acc = function
	    [] -> acc
	  | y::ys ->
	      let acc =
		List.fold_left
		  (fun prev xs -> (y::xs)::prev)
		  acc xs in
	      loop acc ys in
	loop [] x in
  let res = perm [options;options;options] in
  let res =
    List.filter (fun x -> x <> [0;0;0] && x <> [count-1;count-1;count-1]) res in
  let rec take n = function
      x::xs -> if n = 0 then [] else x :: (take (n-1) xs)
    | [] -> [] in
  let res = take n res in
  let res =
    Array.of_list
      (List.map
	 (function
	     [r;g;b] ->
	       let r = increment *. (float_of_int r) in
	       let g = increment *. (float_of_int g) in
	       let b = increment *. (float_of_int b) in
	       RGB(r,g,b)
	   | _ -> failwith "not possible")
	 res) in
  colors := (Array.length res, res)

(* ------------------------------------------------------- *)
(* Jgraph configuration *)

let rec jgraphcolor = function
    RGB(r,g,b) -> Printf.sprintf "color %0.2f %0.2f %0.2f" r g b
  | Choose n -> jgraphcolor (get_color n)
  | SChoose s -> jgraphcolor (command_to_color s false)
  | Color "black" -> ""
  | Color "red" -> jgraphcolor(RGB(1.0,0.0,0.0))
  | Color "green" -> jgraphcolor(RGB(0.0,1.0,0.0))
  | Color _ -> failwith "only black supported"
  | RandomColor ->
      let get _ = (float_of_int(Random.int 1000)) /. 1000. in
      jgraphcolor(RGB(get(),get(),get()))

let jgraphedge = function
    Solid -> "solid"
  | Segments -> failwith "not supported for jgraph"
  | Dotted -> "dotted"
  | Longdash -> "longdash"
  | NoEdge -> "none"

(* ------------------------------------------------------- *)
(* LaTeX configuration *)

let rec latexcolor = function
    RGB(r,g,b) ->
      Printf.sprintf "rgb color={%0.2f,%0.2f,%0.2f}"
	(r *. 255.) (g *. 255.) (b *. 255.)
  | Choose n -> latexcolor (get_color n)
  | SChoose s -> latexcolor (command_to_color s false)
  | Color s -> "color="^s
  | RandomColor ->
      let get _ = float_of_int(Random.int 256) in
      latexcolor (RGB(get(),get(),get()))

let rec latex2color o = function
    RGB(r,g,b) ->
      (try Hashtbl.find latex2colornames (o,r,g,b)
      with Not_found ->
	begin
	  let nc = !namectr in
	  namectr := !namectr + 1;
	  let col = Printf.sprintf "color%d" nc in
	  Printf.fprintf o "\\definecolor{%s}{rgb}{%f,%f,%f}\n" col r g b;
	  Hashtbl.add latex2colornames (o,r,g,b) col;
	  col
	end)
  | Choose n -> latex2color o (get_color n)
  | SChoose s -> latex2color o (command_to_color s false)
  | Color s -> s
  | RandomColor ->
      let get _ = float_of_int(Random.int 256) in
      latex2color o (RGB(get(),get(),get()))

let latexedge = function
    Solid -> ""
  | Segments -> failwith "not supported for latex"
  | Dotted -> "dotted,"
  | Longdash -> "dashed,"
  | NoEdge -> failwith "unknown line type: NoEdge"

let latexmark = function
    "circle" -> "circle"
  | "box" -> "square"
  | "box cfill 1 1 1" -> "square,fill=white"
  | "diamond" -> "diamond"
  | "triangle" -> "triangle"
  | "x" -> "*"
  | "cross" -> "cross"
  | "ellipse" -> "ellipse"
  | "none" -> "none"
  | _ -> failwith "marktype not supported"

(* ------------------------------------------------------- *)
(* Python configuration *)

let rec pythoncolor = function
    RGB(r,g,b) -> (r,g,b)
  | Choose n -> pythoncolor (get_color n)
  | SChoose s -> pythoncolor (command_to_color s false)
  | Color "black" -> (0.0,0.0,0.0)
  | Color "red" -> (1.0,0.0,0.0)
  | Color "green" -> (0.0,1.0,0.0)
  | Color _ -> failwith "only black supported"
  | RandomColor ->
      let get _ = float_of_int(Random.int 256) in
      (get(),get(),get())


let pythonedge = function
    Solid -> "\"solid\""
  | Segments -> failwith "not supported for jgraph"
  | Dotted -> "\"dotted\""
  | Longdash -> "\"dashed\""
  | NoEdge -> failwith "unknown line type: NoEdge"

let pythonmark = function
    "circle" -> "o"
  | "box" -> "s"
  | "box cfill 1 1 1" -> "p" (* should be a hollow square, but not sure how to do that *)
  | "diamond" -> "d"
  | "triangle" -> "^"
  | "x" -> "x"
  | "cross" -> "+"
  | "ellipse" -> failwith "not supported3"
  | "none" -> "\"None\""
  | _ -> failwith "marktype not supported"

(* ------------------------------------------------------- *)
(* ImPlot configuration *)

let implotedge = function
    Solid -> "ImPlotLineFlags_None"
  | Segments -> "ImPlotLineFlags_Segments"
  | Dotted -> "ImPlotLineFlags_None"
  | Longdash -> "ImPlotLineFlags_None"
  | NoEdge -> failwith "unknown line type: NoEdge"

let implotmark = function
    "circle" -> "ImPlotMarker_Circle"
  | "box" -> "ImPlotMarker_Square"
  | "box cfill 1 1 1" -> "ImPlotMarker_Right" (* a bit complex, don't bother *)
  | "diamond" -> "ImPlotMarker_Diamond"
  | "triangle" -> "ImPlotMarker_Up"
  | "x" -> "ImPlotMarker_Asterisk"
  | "cross" -> "ImPlotMarker_Cross"
  | "ellipse" -> "ImPlotMarker_Down" (* no ellipse, use something else *)
  | "none" -> "ImPlotMarker_None"
  | _ -> failwith "marktype not supported"

(* ------------------------------------------------------- *)

let lt x endpoint =
  match endpoint with
    None -> true
  | Some endpoint -> x <= endpoint

let gt x startpoint =
  match startpoint with
    None -> true
  | Some startpoint -> startpoint <= x

let truncrange x startpoint endpoint =
  let x =
    match startpoint with
      None -> x
    | Some startpoint -> max startpoint x in
  let x =
    match endpoint with
      None -> x
    | Some endpoint -> min x endpoint in
  x

let safe_map fn l =
  List.rev (List.fold_left (fun prev cur -> (fn cur) :: prev) [] l)

let text g points =
  let (o,file,ty,time,startpoint,endpoint,labelled) = g in
  Printf.fprintf o "\n(*\n";
  List.iter
    (fun (n,s) ->
      Printf.fprintf o "%f = %f: %s\n" (n -. time) n s)
    points;
  Printf.fprintf o "*)\n\n"

type label = NoLabel | Label of string | FontLabel of int * string

let implot_ctr = ref 0

let doedge linetype marktype arrow g points color edgelabel0 comment =
  let (o,file,ty,time,startpoint,endpoint,labelled) = g in
  let tpts = safe_map (fun (x,y,com) -> (x -. time,y,com)) points in
  let tpts =
    let rec loop acc = function
	[] -> List.rev acc
      | [(x,y,com)] -> (* we actually have no points beyond the endpoint *)
	  List.rev
	    (if gt x startpoint
	    then (x,y,com)::acc else acc)
      | (x1,y1,com1)::(((x2,y2,com2)::_) as r) ->
	  if lt x1 endpoint && gt x2 startpoint
	  then loop ((x1,y1,com1)::acc) r
	  else loop acc r in
    let rec dloop acc = function
	(x,y,com)::rest ->
	  if gt x startpoint && lt x endpoint
	  then dloop ((x,y,com)::acc) rest
	  else if not(lt x endpoint)
	  then List.rev acc
	  else dloop acc rest
      | [] -> List.rev acc in
    if linetype = NoEdge
    then dloop [] tpts
    else loop [] tpts in
  let unnl s =
    if String.contains s '\n'
    then
      String.concat " "
	(Str.split (Str.regexp "\\\\\n") s)
    else s in
  let tpts =
    safe_map (fun (x,y,com) -> (truncrange x startpoint endpoint,y,com)) tpts in
  let edgelabel =
    match edgelabel0 with
      NoLabel -> ""
    | Label s | FontLabel (_,s) ->
	if List.mem s !labelled
	then ((if ty = Latex then labelled := "" :: !labelled);
	      let flat = unnl s in
	      if flat = ""
	      then ""
	      else Printf.sprintf " (* %s *)" flat)
	else
	  begin
	    labelled := s :: !labelled;
	    match edgelabel0 with
	      FontLabel (n,s) ->
		(* the following doesn't work, no idea how to change label font size *)
		Printf.sprintf " label fontsize %d : %s\nlegend custom" n s
	    | _ -> " label : "^s
	  end in
  let label =
    if !O.save_tmp
    then
      match (points,List.rev points) with
	((x1,_,_)::_,(x2,_,_)::_) ->
	  Printf.sprintf "%f-%f = %f%s" x1 x2 (x2 -. x1)
	    (match comment with None -> "" | Some s -> ": "^s)
      | _ -> ""
    else "" in
  let printcom l =
    String.concat " "
      (List.map (fun c -> Printf.sprintf "(* %s *) " (c())) l) in
  match ty with
    Nograph -> ()
  | Jgraph | JgraphPs ->
      Printf.fprintf o "newcurve linetype %s marktype %s %s%s\npts\n"
	(jgraphedge linetype) marktype (jgraphcolor color) edgelabel;
      (match tpts with
	[] -> ()
      | [(x,y,com)] ->
	  if !O.save_tmp
	  then Printf.fprintf o "%s %s %s(* %s *)\n" (string_of_float x) y (printcom com) label
	  else Printf.fprintf o "%s %s\n" (string_of_float x) y
      | [(x,y,com);(x1,y1,com1)] ->
	  if !O.save_tmp
	  then
            Printf.fprintf o "%s %s %s %s %s%s(* %s *)\n"
              (string_of_float x) y (string_of_float x1) y1
	      (printcom com) (printcom com1) label
	  else
	    Printf.fprintf o "%s %s %s %s\n"
	      (string_of_float x) y (string_of_float x1) y1
      | _ ->
	  List.iter
	    (fun (x,y,com) ->
              Printf.fprintf o "%s %s %s(* %s *)\n"
                (string_of_float x) y (printcom com) (string_of_float(x +. time)))
	    tpts;
	  Printf.fprintf o "(* %s *)\n" label);
      if arrow then Printf.fprintf o "rarrows\n";
      Printf.fprintf o "\n"
  | Latex ->
      Printf.fprintf o "\\addplot [%s,mark=%s%s%s] coordinates { %s };\n"
	(latexcolor color) (latexmark marktype)
	(latexedge linetype) (if arrow then ",->" else "")
	(String.concat " "
	   (safe_map (fun (x,y,_) -> Printf.sprintf "(%f,%s)" x y) tpts));
      let at (x,y,_) =
	Printf.fprintf o "\\node at (axis cs:%f,%s){\\href{%s}{\\phantom{x}}}"
	  x y label in
      List.iter at tpts
  | Latex2 ->
      (match tpts with
	[(x1,y1,_);(x2,y2,_)] ->
	  let operator = if arrow then "\\vector" else "\\line" in
	  let orient =
	    if x2 > x1
	    then "(1,0)"
	    else "(0,1)" in
	  let x1 = x1 *. 1000. in
	  let x2 = x2 *. 1000. in
	  let dist = max(x2 -. x1) ((float_of_string y2) -. (float_of_string y1)) in (* FIX! *)
	  let cmd = Printf.sprintf "%s%s{%f}" operator orient dist in
	  let color = latex2color o color in
	  Printf.fprintf o "\\put(%f,%s){\\color{%s}{%s}}\n" x1 y1 color cmd;
	  let href = Printf.sprintf "\\href{%s}{\\phantom{x}}" label in
	  Printf.fprintf o "\\put(%f,%s){%s}\n" x1 y1 href
      | _ -> ())
  | Python ->
      Printf.fprintf o "{ \"LineType\": %s,\n" (pythonedge linetype);
      Printf.fprintf o "  \"MarkType\": %s,\n" (pythonmark marktype);
      Printf.fprintf o "  \"Arrow\": %b,\n" arrow;
      let (r,g,b) = pythoncolor color in
      Printf.fprintf o "  \"Red\": %0.2f,\n" r;
      Printf.fprintf o "  \"Green\": %0.2f,\n" g;
      Printf.fprintf o "  \"Blue\": %0.2f,\n" b;
      Printf.fprintf o "  \"Label\": \"%s\",\n" edgelabel;
      Printf.fprintf o "  \"HoverLabel\": \"%s\",\n" label;
      Printf.fprintf o "  \"X\": [%s],\n"
	(String.concat ","
	   (safe_map (fun x -> Printf.sprintf "%f" x)
	      (safe_map (fun (a,_,_) -> a) tpts)));
      Printf.fprintf o "  \"Y\": [%s] },\n"
	(String.concat "," (safe_map (fun (_,a,_) -> a) tpts))
  | ImPlot(_) ->
      implot_ctr := !implot_ctr + 1;
      Printf.fprintf o "  static double xs%d[] = {\n    " !implot_ctr;
      List.iteri
	(fun i (a,b,_) ->
	  Printf.fprintf o "%f," a;
	  if i mod 30 = 0 then Printf.fprintf o "\n    ")
	tpts;
      Printf.fprintf o "};\n";
      Printf.fprintf o "  static double ys%d[] = {\n    " !implot_ctr;
      List.iteri
	(fun i (a,b,_) ->
	  Printf.fprintf o "%s," b;
	  if i mod 30 = 0 then Printf.fprintf o "\n    ")
	tpts;
      Printf.fprintf o "};\n";
      let (legend,key) = (* legend is shown, if not key chooses color *)
	match edgelabel0 with
	  Label s | FontLabel (_,s) -> (Some s,"")
	| NoLabel ->
	    (match color with
	      Choose n -> (None,string_of_int n)
	    | SChoose s -> (None,s)
	    | _ -> (None,"")) in
      (match color with
	Color _ | RGB _ | RandomColor ->
	  let (r,g,b) = pythoncolor color in
	  Printf.fprintf o "  setcolor(%f,%f,%f);\n" r g b
      | _ -> ());
      (match marktype with
	"none" -> ()
      | "circle" when linetype = NoEdge -> ()
      | _ -> Printf.fprintf o "  setmark(%s);\n" (implotmark marktype));
      let fn =
	if linetype = NoEdge
	then "plotscatter"
	else "plotline" (* only solid lines supported *) in
      match legend with
	Some s ->
	  Printf.fprintf o "  %s(\"%s\",xs%d,ys%d,%d,%s);\n"
	    fn s !implot_ctr !implot_ctr (List.length tpts)
	    (if linetype = NoEdge then "0" else implotedge linetype)
      | None ->
	  Printf.fprintf o "  %s(\"%s\",xs%d,ys%d,%d,%s|ImPlotItemFlags_NoLegend);\n"
	    fn key !implot_ctr !implot_ctr (List.length tpts)
	    (if linetype = NoEdge then "0" else implotedge linetype)
      

let floatify l = safe_map (fun (x,y) -> (x,string_of_int y)) l
let cfloatify l = safe_map (fun (x,y,com) -> (x,string_of_int y,com)) l
let ffloatify l =
   if !O.nanoseconds
   then safe_map (fun (x,y) -> (x,Printf.sprintf "%0.9f" y)) l
   else safe_map (fun (x,y) -> (x,Printf.sprintf "%f" y)) l

let addcomment l = safe_map (fun (x,y) -> (x,y,[])) l

type edgetype =
    graph -> (float * int) list -> color -> label -> string option -> unit

type edgecomtype =
    graph -> (float * int * (unit -> string) list) list -> color -> label -> string option -> unit

type fedgetype =
    graph -> (float * float) list -> color -> label -> string option -> unit

let edge g points   = doedge Solid "none" false g (addcomment(floatify points))
let edgecom g points = doedge Solid "none" false g (cfloatify points)
let fedge g points  = doedge Solid "none" false g (addcomment(ffloatify points))
let segments g points = doedge Segments "none" false g (addcomment(floatify points))
let fsegments g points = doedge Segments "none" false g (addcomment(ffloatify points))
let dots g points   = doedge Dotted "none" false g (addcomment(floatify points))
let fdots g points  = doedge Dotted "none" false g (addcomment(ffloatify points))
let arrow g points  = doedge Longdash "none" true g (addcomment(floatify points))
let mark m g points = doedge Solid m false g (addcomment(floatify points))
let fmark m g points = doedge Solid m false g (addcomment(ffloatify points))
let markonly m g points = doedge NoEdge m false g (addcomment(floatify points))
let fmarkonly m g points = doedge NoEdge m false g (addcomment(ffloatify points))

let notch g points =
  match points with
    [(x,y)] ->
      let y1 = if y > 0 then Printf.sprintf "%d.5" (y-1) else "0" in
      let y2 = Printf.sprintf "%d.5" y in
      doedge Solid "none" false g [(x,y1,[]);(x,y2,[])]
  | _ -> failwith "wrong number of points"

let smallnotch g points =
  match points with
    [(x,y)] ->
      let y1 = if y > 0 then Printf.sprintf "%d.75" (y-1) else "0" in
      let y2 = Printf.sprintf "%d.25" y in
      doedge Solid "none" false g [(x,y1,[]);(x,y2,[])]
  | _ -> failwith "wrong number of points"

let print_text g x y text =
  let (o,file,ty,time,startpoint,endpoint,labelled) = g in
  match ty with
    Jgraph | JgraphPs ->
      Printf.fprintf o
	"newstring hjc vjb x %f y %f fontsize 2 : %s\n\n"
	(truncrange (x -. time) startpoint endpoint) y text
  | _ -> () (* text not supported *)

(* ------------------------------------------------------------------------ *)
(* TikZ graphs *)

let latex_clean s =
  let s = String.concat "\\_" (String.split_on_char '_' s) in
  let s = String.concat "\\&" (String.split_on_char '&' s) in
  let s = String.concat "$>$" (String.split_on_char '>' s) in
  let s = String.concat "$\\mid$" (String.split_on_char '|' s) in
  s

let acm_latex_starter o =                                                            
  Printf.fprintf o                                                              
"\\documentclass[sigplan,review,dvipsnames,anonymous,10pt]{acmart}
\\renewcommand\\footnotetextcopyrightpermission[1]{}
\\pagestyle{plain}
\\usepackage{pgfplots}
\\pgfplotsset{compat=newest}
\\usepackage{graphicx}
\\usepackage{url}
\\usepackage{multirow}
\\usepackage{array}
\\usepackage{amsmath,amsthm}
\\usepackage{subfig}
\\newcommand{\\snest}{\\textsc{Nest}}
\\definecolor{gr}{rgb}{0.22,0.753,0.08}
\\definecolor{color1}{rgb}{0.549,0.815,0.960}
\\definecolor{color2}{rgb}{0.607,0.937,0.498}
\\definecolor{color3}{rgb}{0.839,0.929,0.427}
\\definecolor{color4}{rgb}{0.878,0.619,0.141}
\\definecolor{color5}{rgb}{0.396,0.165,0.055}
\\definecolor{color6}{rgb}{0.643,0.368,0.898}
\\definecolor{colortop}{rgb}{0.878,0.141,0.141}
\\title{OS Scheduling with Nest: Keeping Threads Close Together on Warm Cores}
\\copyrightyear{2021}
\\acmYear{2021}
\\setcopyright{acmlicensed}
\\acmConference[EuroSys '22]{EuroSys 2022: The European Conference on Computer Systems}{April 5--8, 2022}{Rennes, France}
\\acmSubmissionID{Paper ???}
\\begin{document}
\\settopmatter{printfolios=true}
\\maketitle\n"

let latex_starter o =
  Printf.fprintf o
"\\documentclass[landscape]{article}
\\usepackage{fullpage}
\\usepackage{pdflscape}
\\usepackage{xcolor}
\\newcommand{\\snest}{\\textsc{Nest}}
\\definecolor{gr}{rgb}{0.22,0.753,0.08}
\\usepackage{pgfplots}
\\pgfplotsset{compat=newest}
\\definecolor{color1}{rgb}{0.549,0.815,0.960}
\\definecolor{color2}{rgb}{0.607,0.937,0.498}
\\definecolor{color3}{rgb}{0.839,0.929,0.427}
\\definecolor{color4}{rgb}{0.878,0.619,0.141}
\\definecolor{color5}{rgb}{0.396,0.165,0.055}
\\definecolor{color6}{rgb}{0.643,0.368,0.898}
\\definecolor{colortop}{rgb}{0.878,0.141,0.141}
\\begin{document}\\small\n"

let latex_ender o =
  Printf.fprintf o "\\end{document}\n"

type width = BIG | MIDDLE of int | SMALL | TINY

let start_tikz_graph o xlabels ylabels xrotation ylabel ynums largest smallest width height stacked legcols quiet center limits legendright extra =
  let xlabels = List.map latexclean xlabels in
  let center = if center then "\\[" else "" in
  let (ylabel,ypct) =
    if String.contains ylabel '%'
    then ("",true)
    else (ylabel,false) in
  Printf.fprintf o "%s\\begin{tikzpicture}\\begin{axis}[%sybar %s, ymax = %f, ymin = %f,enlarge x limits=%f,\nwidth = %f\\textwidth, height = %fin,\nxtick={%s},xtick pos=left,\nxticklabels={%s},%s\n"
    center ylabels
    (if stacked then "stacked" else "=0pt")
    largest smallest (if width = SMALL then 0.06 else limits)
    (match width with BIG -> (if ylabel = "" && not ypct then 1.05 else 1.) | MIDDLE _ -> (if ylabel = "" then 0.55 else 0.5) | SMALL -> 0.28 | TINY -> 0.15)
    (if width = SMALL then 1.5 else height)
    (String.concat "," (List.mapi (fun i x -> string_of_int i) xlabels))
    (String.concat "," xlabels)
    (if xlabels = [] then "xmajorticks=false," else "");
  (if ypct
  then
    (if ynums
    then Printf.fprintf o "yticklabel={$\\pgfmathprintnumber{\\tick}\\%%$},"
    else Printf.fprintf o "yticklabel={$\\phantom{\\pgfmathprintnumber{\\tick}\\%%}$},")
  else Printf.fprintf o "     ylabel={%s},%s\n" ylabel (if ynums then "" else "yticklabels={},"));
  (match width with SMALL -> Printf.fprintf o "yticklabel style={font=\\footnotesize}," | _ -> ());
  Printf.fprintf o "     xticklabel style={rotate=%d,anchor=%s},legend style={legend columns=%d,at={(%f,1.01)},anchor=%s%s},bar width=%dpt%s]\n"
    xrotation (if xrotation = 90 then "east" else "north east")
    (if legcols = 2 then 4 else legcols)
    (if legendright then 1.0 else 0.5) (if legendright then "south east" else "south")
    (if legcols = 2 then ",font=\\scriptsize" else "")
    (match width with MIDDLE n -> n | SMALL -> 2 | _ -> 3) extra

let end_tikz_graph o len ymax ymin legend quiet center =
  (if not quiet
  then
    begin
      let ln color y =
	Printf.fprintf o "\\addplot[%sdotted,sharp plot,update limits=false] coordinates {(%d,%d) (%d,%d)};\n"
	  color (-1 * len) y (2 * len) y in
      ln "" 0;
      (if ymax >= 5. then ln "red," 5);
      (if ymin <= (-5.) then ln "red," (-5))
    end);
  Printf.fprintf o "\\legend{%s}\n" (String.concat "," legend);
  let center = if center then "\\]" else "" in
  Printf.fprintf o "\\end{axis}\\end{tikzpicture}%s\n\n" center

let tikz_quiet_bar_plot color o data =
  Printf.fprintf o "\\addplot+[%s,error bars/.cd, y dir=both, y explicit] table[x index=0, y index=1, y error index=2] {\n" color;
  List.iter (function (x,v,std,file) -> Printf.fprintf o "%d %f %f\n%% %s\n" x v std file) data;
  Printf.fprintf o "};\n"

let tikz_bar_plot o data =
  Printf.fprintf o "\\addplot+[error bars/.cd, y dir=both, y explicit] table[x index=0, y index=1, y error index=2] {\n";
  List.iter (function (x,v,std,file) -> Printf.fprintf o "%d %f %f\n%% %s\n" x v std file) data;
  Printf.fprintf o "};\n"

let tikz_colored_bar_plot o outercolor color data =
  Printf.fprintf o "\\addplot+[%s,fill=%s] coordinates {\n" outercolor color;
  List.iter (function (x,v,file) -> Printf.fprintf o "(%d, %f)\n%% %s\n" x v file) data;
  Printf.fprintf o "};\n"

let tikz_edge_plot o color data defaultx defaulty forget =
  let data = if data = [] then [(defaultx,defaulty)] else data in
  Printf.fprintf o "\\addplot[%smark=none,color=%s] coordinates {\n"
    (if forget then "forget plot," else "") color;
  List.iter (function (x,v) -> Printf.fprintf o "(%d, %f)\n" x v) data;
  Printf.fprintf o "};\n"

let tikz_mark_plot o color mark data =
  Printf.fprintf o "\\addplot[forget plot,only marks,mark=%s,color=%s%s] coordinates {\n" mark color (if mark = "x" then ",mark size=5" else ",mark size=1");
  List.iter (function (x,v) -> Printf.fprintf o "(%d, %f)\n" x v) data;
  Printf.fprintf o "};\n"

let tikz_mark_fplot o color mark data =
  Printf.fprintf o "\\addplot[forget plot,only marks,mark=%s,color=%s] coordinates {\n" mark color;
  List.iter (function (x,v) -> Printf.fprintf o "(%f, %f)\n" x v) data;
  Printf.fprintf o "};\n"
