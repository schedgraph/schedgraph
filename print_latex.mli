(* SPDX-License-Identifier: GPL-2.0 *)
type column = Left | Center | Right | RightLeft | RightCenterLeft | Sized of float
type value =
    Color of string * value
  | Bold of value
  | FloatV2 of float
  | FloatV4 of float
  | IntV of int
  | StringV of string
  | Pct of value * value option
  | PM of value * float * value

val print_table : out_channel -> column list -> string list list -> value list list -> int option -> unit

val clean : string -> string -> string
