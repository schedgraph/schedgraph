(* SPDX-License-Identifier: GPL-2.0 *)
type ty = Jgraph | JgraphPs | Latex | Latex2 | Python | Nograph | ImPlot of int
type graph =
    out_channel * string * ty * float *
      float option * float option * string list ref
type color =
    RGB of float * float * float | Choose of int
  | SChoose of string | Color of string | RandomColor

val colors : (int * color array) ref
val ncolors : int -> unit

type ymax =
    Nomax | Ymax of float | Yminmax of float * float | Ylist of int list
  | Ymap of (int * string) list

val startgraph :
    string ->  ty -> float ->
      float -> float option -> float option -> string -> (* x information *)
	float -> int option -> ymax -> string -> (* y information *)
	  graph

val extract_o : graph -> out_channel

val endgraph : graph -> unit

type label = NoLabel | Label of string | FontLabel of int * string

type edgetype =
    graph -> (float * int) list -> color -> label -> string option -> unit

type edgecomtype =
    graph -> (float * int * (unit -> string) list) list -> color -> label -> string option -> unit

type fedgetype =
    graph -> (float * float) list -> color -> label -> string option -> unit

val jgraphcolor : color -> string
val latexcolor : color -> string
val pythoncolor : color -> float * float * float

val text  : graph -> (float * string) list -> unit

val edge  : edgetype
val edgecom : edgecomtype
val fedge : fedgetype
val segments : edgetype
val fsegments : fedgetype
val dots  : edgetype
val fdots : fedgetype
val arrow : edgetype
val notch : edgetype
val smallnotch : edgetype
val mark  : string -> edgetype
val fmark  : string -> fedgetype
val markonly : string -> edgetype
val fmarkonly : string -> fedgetype

val print_text : graph -> float -> float -> string -> unit

val command_color : (string,color)   Hashtbl.t
val command_count : (string,int * int ref) Hashtbl.t
val assign_colors : unit -> unit
val command_to_color : string -> bool -> color
val inc_command_count : string -> unit
val set_command_count : string -> int -> unit
val get_command_count : string -> int

(* -------------------------------------- *)
(* TikZ *)

val acm_latex_starter : out_channel -> unit
val latex_starter : out_channel -> unit
val latex_ender : out_channel -> unit

type width = BIG | MIDDLE of int | SMALL | TINY
val start_tikz_graph :
    out_channel -> string list -> string -> int -> string -> bool -> float -> float -> width -> float -> bool -> int -> bool -> bool -> float -> bool -> string -> unit
val tikz_quiet_bar_plot : string -> out_channel -> (int * float * float * string) list -> unit
val tikz_bar_plot : out_channel -> (int * float * float * string) list -> unit
val tikz_colored_bar_plot : out_channel -> string -> string -> (int * float * string) list -> unit (* no error bars - for absolute values *)
val tikz_edge_plot : out_channel -> string -> (int * float) list -> int -> float -> bool -> unit
val tikz_mark_plot : out_channel -> string -> string -> (int * float) list -> unit
val tikz_mark_fplot : out_channel -> string -> string -> (float * float) list -> unit
val end_tikz_graph : out_channel -> int -> float -> float -> string list -> bool -> bool -> unit
