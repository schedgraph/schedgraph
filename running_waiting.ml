(* SPDX-License-Identifier: GPL-2.0 *)
(* number of threads running waiting and overall at any point in time *)

(* ----------------------------------------------------- *)

module O = Options

let ty = ref Printer.Jgraph
let forked = ref false
let sockets = ref 1
let occem = ref false
let overload = ref false
let waiting_only = ref false
let wc = ref false
let wcgraph = ref false
let socket_order = ref false
let extension = ref []

let runtime s f =
  if !O.debug
  then
    begin
      let t = Sys.time() in
      let res = f() in
      let u = Sys.time() in
      Printf.eprintf "%s: %f\n" s (u -. t);
      res
    end
  else f()

let check_fork oldpid pid pids =
  if !forked || Hashtbl.mem pids oldpid
  then Hashtbl.replace pids pid ()

let same_diff pid l =
  if List.mem pid l
  then Some(List.filter (fun x -> x <> pid) l)
  else None

let tracking pids pid =
  pid > 0 && (Hashtbl.mem pids pid || (not !forked && !O.cmds = []))

let update force time cell core delta pids s startpoint =
  let compress = function
      ((t1,v1)::(t2,v2)::(t3,v3)::rest) as l ->
	if v2 = v3 (* only this is needed due to print_edge in mkgraph *)
	then (t1,v1)::(t3,v3)::rest
	else if t1 = t2 (* drop intermediate states, ie t2,v2 *)
	then (t1,v1)::(t3,v3)::rest
	else if !O.relaxed
	then
	  (if v1 < v2 && v2 < v3 && t1 +. 0.00005 > t2 && t2 +. 0.00005 > t3
	  then (t1,v1)::(t3,v3)::rest
	  else if v1 > v2 && v2 > v3 && t1 +. 0.00005 > t2 && t2 +. 0.00005 > t3
	  then (t1,v1)::(t3,v3)::rest
	  else l)
	else l
    | l -> l in
  let socket = core mod !sockets in
  (match Array.get cell socket with
    [] -> Array.set cell socket [(time,max delta 0)]
  | ((t2,prev)::_) as l ->
      let l =
	if time >= startpoint
	then compress ((time,prev+delta)::l)
	else [(time,prev+delta)] in
      Array.set cell socket l);
  if !O.debug && not force
  then
    begin
      if !occem
      then
	begin
	  let cur = snd(List.hd (Array.get cell socket)) in
	  Printf.eprintf "cur: %d\n" cur;
	  Printf.eprintf "running: %s\n"
	    (let ((_,running),(_,waiting),(_,pending)) = s in
	    let mx = Array.length running / 2 in
	    String.concat " "
	      (List.mapi
		 (fun i v ->
		   if i mod !sockets = socket
		   then
		     (if i < mx then "" else "h") ^
		     (string_of_int(List.length v))
		   else "")
		 (Array.to_list running)));
	  let fo =
	    let ct = ref 0 in
	    let ((_,running),(_,waiting),(_,pending)) = s in
	    let mx = Array.length running / 2 in
	    Array.iteri
	      (fun i v ->
		if i mod !sockets = socket && i < mx && v <> [] && Array.get running (i+mx) <> []
		then ct := !ct + 1)
	      running;
	    !ct in
	  let fi =
	    let ct = ref 0 in
	    let ((_,running),(_,waiting),(_,pending)) = s in
	    let mx = Array.length running / 2 in
	    Array.iteri
	      (fun i v ->
		if i mod !sockets = socket && i < mx && v = [] && Array.get running (i+mx) = []
		then ct := !ct + 1)
	      running;
	    !ct in
	  Printf.eprintf "fully occupied: %d, fully idle: %d\n\n" fo fi;
	  (if not(cur = fo) && not(cur = fi) then failwith "inconsistent cur");
	end
      else
	begin
	  let cur = snd(List.hd (Array.get cell socket)) in
	  let ((_,running),(_,waiting),(_,pending)) = s in
	  let count states =
	    let core = ref 0 in
	    List.fold_left
	      (Array.fold_left
		 (fun prev cur ->
	           let c = !core in
		   core := c + 1;
		   if c mod !sockets = socket
		   then
                     let cur = List.filter (tracking pids) cur in
                     List.length cur + prev
		   else prev))
	      0 states in
	  let pcount states =
	    let core = ref 0 in
	    List.fold_left
	      (Array.fold_left
		 (fun prev cur ->
	           let c = !core in
		   core := c + 1;
		   if c mod !sockets = socket
		   then
                     let cur = List.filter (tracking pids) cur in
                     (if List.length cur > 0 then 1 else 0) + prev
		   else prev))
	      0 states in
	  let all = count [running;waiting;pending] in
	  let run = count [running] + pcount [pending] in
	  let tostring states =
            String.concat " "
              (List.fold_left
		 (fun prev st ->
		   Array.fold_left
                     (fun prev l ->
                       let l = List.filter (tracking pids) l in
                       (List.map string_of_int l) @ prev)
                     prev st)
		 [] states) in
	  if not (cur = all || cur = run)
	  then
            failwith
              (Printf.sprintf
		 "%f: number doesn't correspond: cur: %d, all: %s, run: %s\n"
		 time cur (tostring [running;waiting;pending]) (tostring [running]))
	end
    end

let get_relevant pids = List.filter (tracking pids)
let get_count pids state core =
  List.length (get_relevant pids (Array.get state core))

(* ----------------------------------------------------- *)

let rw_ops = Hashtbl.create 7

let init_rw_ops allthreads runningthreads s pids startpoint =
  let starting trace time core pid =
    if tracking pids pid
    then update false time trace core 1 pids s startpoint in
  let ending trace time core pid =
    if tracking pids pid
    then update false time trace core (-1) pids s startpoint in
  let start_pending time core pid =
    if tracking pids pid
    then
      begin
	let (_,_,(_,pending)) = s in
	let len = get_count pids pending core in
	update false time allthreads core 1 pids s startpoint;
	if len = 1 (* the first one is considered to be running *)
	then update false time runningthreads core 1 pids s startpoint
      end in
  let end_pending time core pid =
    if tracking pids pid
    then
      begin
	let (_,_,(_,pending)) = s in
	let len = get_count pids pending core in
	update false time allthreads core (-1) pids s startpoint;
	if len = 0 (* the last one was considered to be running *)
	then update false time runningthreads core (-1) pids s startpoint
      end in
  let combine fn t1 t2 time core pid =
    fn t1 time core pid; fn t2 time core pid in
  Hashtbl.add rw_ops (Manage_runqueues.Src Manage_runqueues.Running)
    (combine ending allthreads runningthreads);
  Hashtbl.add rw_ops (Manage_runqueues.Src Manage_runqueues.Waiting)
    (ending allthreads);
  Hashtbl.add rw_ops (Manage_runqueues.Src Manage_runqueues.Pending)
    end_pending;
  Hashtbl.add rw_ops (Manage_runqueues.Dst Manage_runqueues.Running)
    (combine starting allthreads runningthreads);
  Hashtbl.add rw_ops (Manage_runqueues.Dst Manage_runqueues.Waiting)
    (starting allthreads);
  Hashtbl.add rw_ops (Manage_runqueues.Dst Manage_runqueues.Pending)
    start_pending

(* waiting only *)
let w_ops = Hashtbl.create 7

let init_w_ops waitingthreads s pids startpoint =
  let starting time core pid =
    if tracking pids pid
    then update false time waitingthreads core 1 pids s startpoint in
  let ending time core pid =
    if tracking pids pid
    then update false time waitingthreads core (-1) pids s startpoint in
  Hashtbl.add w_ops (Manage_runqueues.Src Manage_runqueues.Waiting) ending;
  Hashtbl.add w_ops (Manage_runqueues.Src Manage_runqueues.Pending) ending;
  Hashtbl.add w_ops (Manage_runqueues.Dst Manage_runqueues.Waiting) starting;
  Hashtbl.add w_ops (Manage_runqueues.Dst Manage_runqueues.Pending) starting

(* fully occupied and fully empty cores *)
let oe_ops = Hashtbl.create 7

let init_oe_ops fully_occupied fully_empty (((_,running),_,_) as s) pids startpoint cores =
  let hcores = cores / 2 in (* works for Intel only *)
  let hcores_per_socket = hcores / !sockets in
  Array.iteri (fun i _ -> Array.set fully_empty i [(0.,hcores_per_socket)]) fully_empty; (* all cores start empty *)
  let starting time core pid =
    if pid > 0
    then
      begin
	let neighbor = (core + hcores) mod cores in
	(if Array.get running neighbor = []
	then update false time fully_empty core (-1) pids s startpoint
	else update false time fully_occupied core 1 pids s startpoint)
      end in
  let ending time core pid =
    if pid > 0
    then
      begin
	let neighbor = (core + hcores) mod cores in
	(if Array.get running neighbor = []
	then update false time fully_empty core 1 pids s startpoint
	else update false time fully_occupied core (-1) pids s startpoint)
      end in
  Hashtbl.add oe_ops (Manage_runqueues.Src Manage_runqueues.Running) ending;
  Hashtbl.add oe_ops (Manage_runqueues.Dst Manage_runqueues.Running) starting

(* duration of time with work conservation issues *)
let wc_ops = Hashtbl.create 7

type wc = OpenWc of float | CloseWc of float * float

let init_wc_ops wc_trace ((_,running),(_,waiting),_) pids startpoint cores =
  let changed time core pid =
    let has_idle = Array.exists (fun v -> v = []) running in
    let has_overload = Array.exists (fun v -> v <> []) waiting in
    if has_overload && has_idle
    then
      match !wc_trace with
	(OpenWc time) :: _ -> ()
      | l -> wc_trace := (OpenWc time) :: l
    else
      match !wc_trace with
	(OpenWc t0) :: l -> wc_trace := (CloseWc (t0,time)) :: l
      | _ -> () in
  Hashtbl.add wc_ops (Manage_runqueues.Src Manage_runqueues.Running) changed;
  Hashtbl.add wc_ops (Manage_runqueues.Dst Manage_runqueues.Running) changed;
  Hashtbl.add wc_ops (Manage_runqueues.Src Manage_runqueues.Waiting) changed;
  Hashtbl.add wc_ops (Manage_runqueues.Dst Manage_runqueues.Waiting) changed


(* overload amounts *)
let overload_ops = Hashtbl.create 7

let init_overload_ops load_trace ((_,running),(_,waiting),_) pids startpoint cores =
  let changed time core pid =
    let running = List.filter (tracking pids) (Array.get running core) in
    let waiting = List.filter (tracking pids) (Array.get waiting core) in
    let cur = Array.get load_trace core in
    let load = List.length running + List.length waiting in
    (match cur with
      (t2,l2)::(t1,l1)::rest when l2 = load && l1 = load -> Array.set load_trace core ((time,load)::(t1,l1)::rest)
    | (t2,l2)::rest when time = t2 && l2 = load -> ()
    | _ -> Array.set load_trace core ((time,load) :: cur)) in
  Hashtbl.add overload_ops (Manage_runqueues.Src Manage_runqueues.Running) changed;
  Hashtbl.add overload_ops (Manage_runqueues.Dst Manage_runqueues.Running) changed;
  Hashtbl.add overload_ops (Manage_runqueues.Src Manage_runqueues.Waiting) changed;
  Hashtbl.add overload_ops (Manage_runqueues.Dst Manage_runqueues.Waiting) changed

let process_line ops time core
    (allthreads,runningthreads,
     (((_,running),(_,waiting),(_,pending)) as s),pids,start)
    startpoint l =
  (* manage detection of interesting pids - try to move to parser *)
  (match l with
    Parse_line.Sched_process_exec(cmd,oldcmd,pid,oldpid) ->
      let tracking_oldpid = tracking pids oldpid in
      Util.update_requested_pids oldpid oldcmd pids;
      Util.update_requested_pids pid cmd pids;
      check_fork oldpid pid pids;
      (* the following compensates for what should have happened, but did not
	 due to a tracked name not being matched *)
      (* running could be [] when we haven't started yet *)
      if not tracking_oldpid && tracking pids pid &&
         Array.get running core <> []
      then
	begin
	  update true time allthreads core 1 pids s startpoint;
	  update true time runningthreads core 1 pids s startpoint;
	end
      else if tracking_oldpid && not (tracking pids pid)
      then
	begin
	  update true time allthreads core (-1) pids s startpoint;
	  update true time runningthreads core (-1) pids s startpoint
	end
  | Parse_line.Sched_process_fork(parent_cmd,parent_pid,cmd,pid) ->
      Util.update_requested_pids parent_pid parent_cmd pids;
      Util.update_requested_pids pid cmd pids;
      check_fork parent_pid pid pids
  | Parse_line.Sched_switch(fromcmd,frompid,state,tocmd,topid) ->
      Util.update_requested_pids frompid fromcmd pids;
      Util.update_requested_pids topid tocmd pids
  | Parse_line.Sched_wakeup_new(cmd,pid,_,cpu) ->
      Util.update_requested_pids pid cmd pids;
      check_fork (-1) pid pids
  | Parse_line.Sched_wakeup(cmd,pid,_,cpu) ->
      Util.update_requested_pids pid cmd pids
  | _ -> ());
  Manage_runqueues.process_line time core ops s l

let events =
  ["sched_switch";"sched_process_exec";"sched_process_fork";
    "sched_wakeup_new";"sched_wakeup";"sched_migrate_task"]

let parse_all ctx =
  let allthreads = Array.make !sockets [] in
  let fully_occupied = allthreads in
  let runningthreads = Array.make !sockets [] in
  let fully_empty = runningthreads in
  let st = Manage_runqueues.init ctx.Util.cores in
  let pids = Hashtbl.create 101 in
  let start = ctx.Util.start in
  let ops =
    if !occem
    then
      begin
        init_oe_ops fully_occupied fully_empty st pids (!O.min +. start) ctx.Util.cores;
	[oe_ops]
      end
    else if !waiting_only
    then
      begin
        init_w_ops allthreads st pids (!O.min +. start);
	[w_ops]
      end
    else
      begin
        init_rw_ops allthreads runningthreads st pids (!O.min +. start);
	[rw_ops]
      end in
  Util.parse_loop ctx !O.min !O.max events
    (allthreads,runningthreads,st,pids,start) (process_line ops)


let process_line_wc ops time core
    (_,(((_,running),(_,waiting),(_,pending)) as s),pids,start)
    startpoint l =
  Manage_runqueues.process_line time core ops s l

let parse_all_wc ctx =
  let st = Manage_runqueues.init ctx.Util.cores in
  let wc_data = ref [] in
  let pids = Hashtbl.create 101 in
  let start = ctx.Util.start in
  let ops =
    init_wc_ops wc_data st pids (!O.min +. start) ctx.Util.cores;
    [wc_ops] in
  Util.parse_loop ctx !O.min !O.max events
    (wc_data,st,pids,start) (process_line_wc ops)


let process_line_overload ops time core
    (_,(((_,running),(_,waiting),(_,pending)) as s),pids,start)
    startpoint l =
  Manage_runqueues.process_line time core ops s l

let parse_all_overload ctx =
  let st = Manage_runqueues.init ctx.Util.cores in
  let overload_data = Array.make ctx.Util.cores [] in
  let pids = Hashtbl.create 101 in
  let start = ctx.Util.start in
  let ops =
    init_overload_ops overload_data st pids (!O.min +. start) ctx.Util.cores;
    [overload_ops] in
  Util.parse_loop ctx !O.min !O.max events
    (overload_data,st,pids,start) (process_line_overload ops)

(* ----------------------------------------------------- *)
(* focused on pids - how many threads of a command are running or waiting *)

let target_ops = Hashtbl.create 7

type openclosed = Open of float | Closed of float * float

let init_target_ops running_trace waiting_trace pids startpoint =
  let find trace pid =
    try Hashtbl.find trace pid with _ -> [] in
  let starting trace time core pid =
    if time >= startpoint && Hashtbl.mem pids pid
    then
      match find trace pid with
	(Open t1) :: rest -> failwith "already open"
      | l -> Hashtbl.replace trace pid ((Open time) :: l) in
  let ending trace time core pid =
    if time >= startpoint && Hashtbl.mem pids pid
    then
      match find trace pid with
	(Open t1) :: rest ->
	  Hashtbl.replace trace pid ((Closed(t1,time)) :: rest)
      | [] -> Hashtbl.replace trace pid [Closed(startpoint,time)]
      | _ -> failwith "not open" in
  Hashtbl.add target_ops (Manage_runqueues.Src Manage_runqueues.Running)
    (ending running_trace);
  Hashtbl.add target_ops (Manage_runqueues.Src Manage_runqueues.Waiting)
    (ending waiting_trace);
  Hashtbl.add target_ops (Manage_runqueues.Dst Manage_runqueues.Running)
    (starting running_trace);
  Hashtbl.add target_ops (Manage_runqueues.Dst Manage_runqueues.Waiting)
    (starting waiting_trace)

let process_line_target time core (runtrace,waittrace,((running,waiting,pending) as s),pids,start) startpoint l =
  (* manage detection of interesting pids - try to move to parser *)
  (match l with
    Parse_line.Sched_process_exec(cmd,oldcmd,pid,oldpid) ->
      Util.update_requested_pids oldpid oldcmd pids;
      Util.update_requested_pids pid cmd pids;
      check_fork oldpid pid pids
  | Parse_line.Sched_process_fork(parent_cmd,parent_pid,cmd,pid) ->
      Util.update_requested_pids parent_pid parent_cmd pids;
      Util.update_requested_pids pid cmd pids;
      check_fork parent_pid pid pids
  | Parse_line.Sched_switch(fromcmd,frompid,state,tocmd,topid) ->
      Util.update_requested_pids frompid fromcmd pids;
      Util.update_requested_pids topid tocmd pids
  | Parse_line.Sched_wakeup_new(cmd,pid,_,cpu) ->
      Util.update_requested_pids pid cmd pids;
      check_fork (-1) pid pids
  | Parse_line.Sched_wakeup(cmd,pid,_,cpu) ->
      Util.update_requested_pids pid cmd pids
  | _ -> ());
  Manage_runqueues.process_line time core [target_ops] s l

let parse_all_target ctx =
  let runtrace = Hashtbl.create 101 in
  let waittrace = Hashtbl.create 101 in
  let st = Manage_runqueues.init ctx.Util.cores in
  let pids = Hashtbl.create 101 in
  let start = ctx.Util.start in
  init_target_ops runtrace waittrace pids (!O.min +. start);
  Util.parse_loop ctx !O.min !O.max events
    (runtrace,waittrace,st,pids,start) process_line_target

(* ----------------------------------------------------- *)
(* start and last are absolute values. startpoint and endpoint are offsets from start *)

let biggest getter l =
    List.fold_left
	(fun prev l ->
	     List.fold_left
		(fun prev cur ->
		     max prev (getter cur))
		prev l)
        0. l

let detect_hidden_edges front back =
  let rec startsame acc f b =
    match (f,b) with
      (f1::((f2::_) as frest), b1::((b2::_) as brest)) ->
	if f2 = b2
	then startsame acc frest brest
	else
	  let (bfront,frest,brest) = startdiff frest brest in
	  startsame ((b1::bfront) :: acc) frest brest
    | _ -> List.rev(b :: acc)
  and moveon (ft1,fv1) frest (bt1,bv1) brest =
    (match compare ft1 bt1 with
      -1 ->
	let (bfront,frest,brest) = startdiff frest ((bt1,bv1)::brest) in
	(bfront,frest,brest)
    | 0 ->
	let (bfront,frest,brest) = startdiff frest brest in
	((bt1,bv1)::bfront,frest,brest)
    | _ -> (* must be 1 *)
	let (bfront,frest,brest) = startdiff ((ft1,fv1)::frest) brest in
	((bt1,bv1)::bfront,frest,brest))
  and startdiff f b =
    match (f,b) with
      (f1::((f2::((f3::_) as frest)) as bigfrest),b1::((b2::((b3::_) as brest)) as bigbrest)) ->
    	if f2 = b2 && f3 = b3
	then ([b1;b2],frest,brest)
	else moveon f1 bigfrest b1 bigbrest
    | (f1::frest,b1::brest) -> moveon f1 frest b1 brest
    | (_,b) -> (b,[],[]) in
  match (front,back) with
    (f1::frest,b1::brest) ->
      if f1 = b1
      then startsame [] front back
      else
	let (res,frest,brest) = startdiff front back in
	res :: startsame [] frest brest
  | (_,l) -> [l]

let detect_hidden_edges front back =
  let rec loop pending acc f b =
    match (f,b) with
      (f1::((f2::_) as frest),b1::((b2::_) as brest)) ->
	if f1 = b1
	then
	  (if f2 = b2
	  then
	    (if pending = []
	    then loop pending acc frest brest
	    else loop [] (List.rev (b1::pending) :: acc) frest brest)
	  else loop (b1::pending) acc frest brest)
	else
	  (match compare (fst f1) (fst b1) with
	    0 -> loop (b1::pending) acc frest brest
	  | -1 -> loop pending acc frest (b1::brest)
	  | _ (*1*) -> loop (b1::pending) acc (f1::frest) brest)
    | _,b ->
      let newpending =
	if pending = []
	then List.rev b
	else List.fold_left (fun prev b -> b :: prev) pending b in
      if newpending = []
      then List.rev acc
      else List.rev (List.rev newpending :: acc) in
  loop [] [] front back

let auc l =
  let rec loop acc = function
      (x,y)::(((x1,y1)::_) as rest) ->
	loop (((x1 -. x) *. (float_of_int y)) +. acc) rest
    | _ -> acc in
  loop 0. l

let mkgraph start last (* times *) startpoint endpoint (* offsets *)
    flbase allthreads maxrunningthreads minrunningthreads cores =
  let implot = match !ty with Printer.ImPlot _ -> true | _ -> false in
  Printf.eprintf "start %f last %f startpoint %f endpoint %f\n"
    start last startpoint endpoint;
  (* main graph *)
  let flbase =
    if !O.outdir = ""
    then flbase
    else Printf.sprintf "%s/%s" !O.outdir flbase in
  let xlabel = flbase in
  let ymin = Some 0 in
  let ybiggest =
    biggest (fun (x,y) -> float_of_int y)
      [allthreads;maxrunningthreads;minrunningthreads] in
  let ymax = Printer.Ymax ybiggest in
  let ylabel = if !occem then "cores" else "threads" in
  let last_time =
    if !O.max > 0. then !O.max else last -. start in
  let startpoint = !O.min in
  let endpoint = last_time in
  let o =
    Printer.startgraph flbase !ty start
      !O.standard_xsize (Some startpoint) (Some endpoint)
      xlabel !O.standard_ysize ymin ymax ylabel in
  (if not implot
  then Printf.fprintf (Printer.extract_o o) "legend custom\n\n");
  let label_height = max (ybiggest *. 1.1) (ybiggest +. 1.) in
  let xbiggest n = startpoint +. ((endpoint -. startpoint) *. n) in
  let allthreads =
    runtime "hidden"
      (fun _ -> detect_hidden_edges maxrunningthreads allthreads) in
  (if not implot
  then
    begin
      Printf.fprintf (Printer.extract_o o)
	"newcurve linetype solid marktype none color 1 0 0 label x %f y %f hjl : %s\npts\n\n"
	(xbiggest 0.2) label_height (if !occem then "fully occupied cores" else "all threads");
      Printf.fprintf (Printer.extract_o o)
	"newcurve linetype solid marktype none color 0 1 0 label x %f y %f hjl : %s%s\npts\n\n"
	(xbiggest 0.6) label_height (if !occem then "fully empty cores" else "running threads")
	(if !occem then Printf.sprintf " (AUC: %f sec.)" (auc maxrunningthreads) else "")
    end);
  let reorganize l = (* to remove if support implot stairs *)
    match l with
      [] -> []
    | (t,v)::rest ->
	let rec loop prev acc = function
	    (t,v)::rest -> loop v ((t,v)::(t,prev)::acc) rest
	  | [] -> List.rev acc in
	(t,v)::(loop v [] rest) in
  let label =
    match !ty with
      Printer.ImPlot _ -> Printer.Label "all threads"
    | _ -> Printer.NoLabel in
  List.iter
    (fun allthreads ->
      Printer.edge o (reorganize allthreads) (Printer.Color "red") label None)
    allthreads;
  let label =
    match !ty with
      Printer.ImPlot _ -> Printer.Label "running threads"
    | _ -> Printer.NoLabel in
  Printer.edge o (reorganize maxrunningthreads) (Printer.Color "green") label None;
  (if not !waiting_only
  then
    let numcores =
      if !occem
      then cores / 2
      else cores in
    let numcores = numcores / !sockets in
    Printer.dots o [(startpoint +. start,numcores);(endpoint +. start,numcores)] (Printer.Color "black")
      Printer.NoLabel None);
  Printer.endgraph o

let mktxt startpoint endpoint flbase allthreads runningthreads cores =
  let fl =
    if !O.outdir = ""
    then flbase ^ ".rw"
    else Printf.sprintf "%s/%s.rw" !O.outdir flbase in
  let o = open_out fl in
  let total_time = endpoint -. startpoint in
  let time_in n =
    let rec loop acc = function
	[] | [_] -> acc
      | (t1,n1)::(((t2,_)::_) as rest) ->
	  if n1 <= n
	  then loop ((t2 -. t1) +. acc) rest
	  else loop acc rest in
    loop 0.0 allthreads in
  let time_above n =
    let rec loop acc = function
	[] | [_] -> acc
      | (t1,n1)::(((t2,_)::_) as rest) ->
	  if n1 >= n
	  then loop ((t2 -. t1) +. acc) rest
	  else loop acc rest in
    loop 0.0 allthreads in
  let ti4 = time_in 4 in
  let ti8 = time_in 8 in
  let ti12 = time_in 12 in
  let timax = time_above cores in
  let tinearmax = time_above (cores - (cores/10)) in
  let alltime =
    let rec loop acc = function
	[] | [_] -> acc
      | (t1,n1)::(((t2,_)::_) as rest) ->
	  loop (((t2 -. t1) *. (float_of_int n1)) +. acc) rest in
    loop 0.0 allthreads in
  Printf.fprintf o "time volume: %f\naverage threads: %f\ntime in <=4: %f (%f)\ntime in <=8: %f (%f)\ntime in <=12: %f (%f)\ntime at max (%d): %f (%f)\ntime near max (>=%d): %f (%f)\n"
    alltime (alltime /. total_time) ti4 (ti4 /. total_time)
    ti8 (ti8 /. total_time) ti12 (ti12 /. total_time)
    cores timax (timax /. total_time) (cores - (cores/10))
    tinearmax (tinearmax /. total_time);
  close_out o

let extract_edge mn mx start l =
  let mn = mn +. start in
  let mx = mx +. start in
  Hashtbl.fold
    (fun k l r ->
      let rec loop acc = function
	  (Closed(t1,t2))::rest ->
	    let t1 = max t1 mn in
	    let t2 = min t2 mx in
	    loop ((Closed(t1,t2))::acc) rest
	| [] -> List.rev acc
	| _ -> failwith "not possible" in

      if List.length !O.ranges > 1
      then (k,loop [] l) :: r
      else (k,l) :: r)
    l []

let extract mn mx start l =
  let mn = mn +. start in
  let mx = mx +. start in
  let rec loop acc = function
      ((t,v) as a)::rest ->
	if t < mn
	then loop acc rest
	else if t > mx && mx > 0.
	then List.rev acc
	else loop (a::acc) rest
    | [] -> List.rev acc in
  if List.length !O.ranges > 1
  then loop [] l
  else l

let reorder c cores =
  if !socket_order
  then Util.reorder cores c
  else c

let close_and_reverse last data =
  Array.iteri
    (fun i history ->
      match history with
       (((t,load)::_) as rest) when t < last ->
         Array.set data i (List.rev ((last,load)::rest))
      | l -> Array.set data i (List.rev l))
    data

let main (base,ctx) =
  let cores = ctx.Util.cores in
  if !wc
  then
    begin
      let (success,start,last, (wc_data,_,pids,_start)) =
	runtime "parse_all" (fun _ ->
	  try parse_all_wc ctx
	  with e -> begin Util.remove 1 ctx; raise e end) in
      Util.remove 2 ctx;
      let duration = last -. start in
      let wc_time =
	List.fold_left
	  (fun acc ->
	    function
		OpenWc t0 -> (last -. t0) +. acc
	      | CloseWc(t0,t1) -> (t1 -. t0) +. acc)
	  0. !wc_data in
      Printf.printf
	"%s: WC time: %f, duration: %f, WC percentage: %0.2f%%\n"
	base wc_time duration ((wc_time *. 100.) /. duration)
    end
  else if !overload
  then
    begin
      let (success,start,last, (overload_data,_,pids,_start)) =
	runtime "parse_all" (fun _ ->
	  try parse_all_overload ctx
	  with e -> begin Util.remove 1 ctx; raise e end) in
      Util.remove 2 ctx;
      let last_time =
	if !O.max > 0. then !O.max else last -. start in
      let self_last_time =
	if !O.max > 0. then (min ctx.endoffset !O.max) +. start else last in
      let (startpoint,endpoint) = (* startpoint and endpoint are offsets *)
	!O.min,last_time in
      close_and_reverse self_last_time overload_data;
      let xlabel =
	Printf.sprintf "%s overload\\\n\n" base in
      let ylabel =
	if !socket_order
	then "cores (socket order)"
	else "cores" in
      let (ymin,ymax) =
	(Some 0,Printer.Ymax (float_of_int ctx.Util.cores)) in
      let fl = base^"_"^(String.concat "_" (List.sort compare !extension)) in
      let o =
	Printer.startgraph fl !ty start
	  !O.standard_xsize
	  (Some startpoint) (Some endpoint)
	  xlabel !O.standard_ysize ymin ymax ylabel in
      Array.iteri
	(fun i history ->
	  let rec loop = function
	      (t1,load)::(((t2,_)::_) as a) ->
		(if load > 0
		then
		  let color = Printer.Choose load in
		  let label = Printf.sprintf "%d threads" load in
		  let i = reorder i cores in
		  Printer.edge o [(t1,i);(t2,i)] color (Printer.Label label) None);
		loop a
	    | [] | [_] -> () in
	  loop history)
	overload_data;
      Printer.endgraph o
    end
  else
    let (success,start,last, (allthreads,runningthreads,_,pids,_start)) =
      runtime "parse_all" (fun _ ->
	try parse_all ctx
	with e -> begin Util.remove 1 ctx; raise e end) in
    Util.remove 2 ctx;
    let self_last_time =
      if !O.max > 0. then (min ctx.endoffset !O.max) +. start else last in
    close_and_reverse self_last_time allthreads;
    close_and_reverse self_last_time runningthreads;
    let name =
      if !occem then "oe"
      else if !waiting_only then "waiting"
      else "rw" in
    Array.iteri
      (fun i allthreads ->
	let runningthreads = Array.get runningthreads i in
	List.iter
	  (fun (mn,mx,modifiers) ->
	    let base =
	      Printf.sprintf "%s_%s%s" base name
		(String.concat ""
		   (List.map (fun x -> Printf.sprintf "_%s" x)
		      (modifiers @
		       (List.sort compare !O.modifiers)))) in
	    let base =
	      if !sockets = 1
	      then base
	      else Printf.sprintf "%s_socket%d" base i in
	    let all = extract mn mx start allthreads in
	    let rt  = extract mn mx start runningthreads in
	    runtime "mktxt" (fun _ ->
	      mktxt (mn +. start) (mx +. start) base all rt cores);
	    let (maxrt,minrt) = (rt,[]) in
	    if not !O.nograph
	    then
	      runtime "mkgraph"
		(fun _ ->
		  mkgraph start last mn mx base all maxrt minrt cores))
	  !O.ranges)
      allthreads
      
let main_target (base,ctx) =
  let (success,start,last,(runtrace,waittrace,_,pids,_start)) =
    runtime "parse_all" (fun _ ->
      try parse_all_target ctx
      with e -> begin Util.remove 1 ctx; raise e end) in
  Util.remove 2 ctx;
  let last_time =
      if !O.max > 0. then !O.max +. start else last in
  let close trace =
    Hashtbl.iter
      (fun k l ->
	match l with
	  (Open t) :: res ->
	    Hashtbl.replace trace k
	      (List.rev ((Closed(t,last_time)) :: res))
	| res -> Hashtbl.replace trace k (List.rev res))
      trace in
  close runtrace;
  close waittrace;
  List.iter
    (fun (mn,mx,modifiers) ->
      let base =
	Printf.sprintf "%s_rwtgt%s" base
	  (String.concat ""
	     (List.map (fun x -> Printf.sprintf "_%s" x)
		(modifiers @
		 (List.sort compare !O.modifiers)))) in
      let runtrace  = extract_edge mn mx start runtrace in
      let waittrace = extract_edge mn mx start waittrace in
      let xlabel =
	let dur =
	  if List.length !O.ranges > 1
	  then mx -. mn
	  else last -. start in
	Printf.sprintf "%s, time (sec), duration: %f" base dur in
      let (ymin,ymax) =
	let mx = ref (-1) in
	let mn = ref (-1) in
	Hashtbl.iter
	  (fun pid _ ->
	    (if pid > !mx then mx := pid);
	    (if !mn < 0 || pid < !mn then mn := pid))
	  pids;
	(!mn,float_of_int (!mx - !mn + 2)) in
      let o =
	Printer.startgraph base Printer.Jgraph start
	  !O.standard_xsize (Some mn) None xlabel
	  (max !O.standard_ysize (ymax /. 10.)) (Some ymin) Printer.Nomax
	  "pids" in
      let print_edge pid label color = function
	  Closed(t1,t2) ->
	    Printer.edge o [(t1,pid);(t2,pid)] color (Printer.Label label) None
	| _ -> failwith "unexpected open" in
      List.iter
	(fun (pid, l) ->
	  List.iter (print_edge pid "running" (Printer.RGB(0.0,1.0,0.0))) l)
	runtrace;
      List.iter
	(fun (pid, l) ->
	  List.iter (print_edge pid "waiting" (Printer.RGB(1.0,0.0,0.0))) l)
	waittrace;
      Printer.endgraph o)
    !O.ranges

let dowcgraph files =
  let files = List.sort compare files in
  let (base,ctx) = List.hd files in
  let infos =
    List.map
      (function (base,ctx) ->
	let (success,start,last, (wc_data,_,pids,_start)) =
	  runtime "parse_all" (fun _ -> parse_all_wc ctx
	    (*try parse_all_wc ctx
	    with e -> begin Util.remove 1 ctx; raise e end*)) in
	Util.remove 2 ctx;
	let duration = last -. start in
	let wc_time =
	  List.fold_left
	    (fun acc ->
	      function
		  OpenWc t0 -> (last -. t0) +. acc
		| CloseWc(t0,t1) -> (t1 -. t0) +. acc)
	    0. !wc_data in
	(duration,((wc_time *. 100.) /. duration)))
      files in
  let infos = List.sort compare infos in
  let fl = Printf.sprintf "%s_wc" base in
  let start = 0. in
  Random.init (Hashtbl.hash base);
  let color = Printer.RGB(Random.float 1.,Random.float 1.,Random.float 1.) in
  let o =
    Printer.startgraph fl Printer.Jgraph start !O.standard_xsize None None
      base !O.standard_ysize (Some 0) Printer.Nomax "execution time (sec)" in
  let pts = List.mapi (fun i (dur,_) -> (float_of_int i,dur)) infos in
  Printer.fedge o pts color (Printer.Label "duration") None;
  let o0 = Printer.extract_o o in
  Printf.fprintf o0 "\ncopygraph yaxis min 0 max 100 hash_scale 1\n\n";
  Printf.fprintf o0 "newcurve linetype dashed marktype none %s label : wc percentage\npts\n"
    (Printer.jgraphcolor color);
  List.iteri (fun i (dur,pct) -> Printf.fprintf o0 "  %d %f\n" i pct) infos;
  Printer.endgraph o

(* ----------------------------------------------------- *)

module PL = Print_latex

let op = '('

let make_table output files =
  let machines = ref [] in
  let benchs = ref [] in
  let numlines = ref 0 in
  let parse_name cores file =
    match String.split_on_char '_' file with
      [_trace;bench;_;os;gov;machine;_] ->
	let machine =
	  (String.concat "-"
	     (List.rev(List.tl(List.rev(String.split_on_char '-' machine))))) in
	let machine = (cores,os,gov,machine) in
	(if not (List.mem machine !machines)
	then machines := machine :: !machines);
	(if not (List.mem bench !benchs)
	then benchs := bench :: !benchs);
	(bench,machine)
    | _ -> failwith "file name format not supported" in
  let get_cores (mx,_) =
    match String.split_on_char op mx with
      [_;cores] -> int_of_string (String.sub cores 0 (String.length cores - 1))
    | _ -> failwith "bad line" in
  let parse_data l =
    let two = function
	[a;b] -> (a,b)
      | l -> failwith ("wrong number: "^(String.concat ":" l)) in
    let d = List.map (fun x -> two(String.split_on_char ':' x)) l in
    numlines := List.length d;
    let cores =
      match d with
	[tv;at;four;eight;twelve;mx;nmax] -> get_cores mx
      | _ -> failwith "bad line" in
    (cores,d) in
  let infos =
    List.sort compare
      (List.map
	 (function file ->
	   let (cores,d) = parse_data(Util.cmd_to_list ("cat "^file)) in
	   (parse_name cores file, d))
	 files) in
  let tbl = Hashtbl.create 101 in
  List.iter
    (function ((bench,machine),infos) ->
      Util.hashadd tbl bench (machine,infos))
    infos;
  let benchs = List.sort compare !benchs in
  let (reference,machines) =
    let l = List.sort compare !machines in
    (List.hd l,l) in
  (* create tables *)
  let o = open_out output in
  let columns1 =
    PL.Left :: (List.map (function machine -> PL.Center) machines) in
  let columns2 =
    PL.Left :: (List.map (function machine -> PL.RightLeft) machines) in
  let titles1 =
    "Bench" ::
    (List.map (function (_,os,gov,machine) -> os) machines) in
  let titles2 =
    "" ::
    (List.map (function (_,os,gov,machine) -> gov) machines) in
  let titles3 =
    "" ::
    (List.map (function (_,os,gov,machine) -> machine) machines) in
  Printer.latex_starter o;
  List.iter
    (function n ->
      let (topdata,model) =
	let (_,infos) = List.hd(!(Hashtbl.find tbl (List.hd benchs))) in
	let (s,v) = List.nth infos n in
	(String.trim(List.hd(String.split_on_char op s)),v) in
      let (c1,lineparser) =
	match String.split_on_char op model with
	  [_] ->
	    (true,function (_,line) ->
	      let v = float_of_string(String.trim line) in
	      (v, fun color -> color(PL.FloatV2 v)))
	| [_;_] ->
	    (false,function (_,line) ->
	      match String.split_on_char op line with
		[v;pct] ->
		  let v = float_of_string(String.trim v) in
		  let vs = PL.FloatV2 v in
		  let pct =
		    let pct = String.sub pct 0 (String.length pct - 1) in
		    PL.FloatV2(100. *. (float_of_string pct)) in
		  (v,fun color -> PL.Pct(color vs,Some pct))
	      | _ -> failwith "not possible")
	| _ -> failwith "not possible" in
      Printf.fprintf o "\\paragraph*{%s}\n\n" (PL.clean "X" topdata);
      let lines =
	List.map
	  (function bench ->
	    let benchinfos = !(Hashtbl.find tbl bench) in
	    let refv =
	      try
		Some
		  (fst(lineparser(List.nth (List.assoc reference benchinfos) n)))
	      with Not_found -> None in
	    (PL.StringV bench) ::
	    (List.map
	      (function machine ->
		try
		  let (v,colorfn) =
		    lineparser(List.nth (List.assoc machine benchinfos) n) in
		  (match refv with
		    Some refv ->
		      let ratio = v /. refv in
		      if ratio > 1.05
		      then colorfn (fun x -> PL.Color("gr",x))
		      else if ratio < 0.95
		      then colorfn (fun x -> PL.Color("red",x))
		      else colorfn (fun x -> x)
		  | None -> colorfn (fun x -> x))
		with Not_found -> PL.StringV "---")
	       machines))
	  benchs in
      PL.print_table o (if c1 then columns1 else columns2)
	[titles1;titles2;titles3] lines None;
      Printf.fprintf o "\\newpage\n\n")
    (Util.iota !numlines);
  Printer.latex_ender o;
  close_out o;
  ignore(Sys.command (Printf.sprintf "rubber -d --quiet %s" output))

(* ----------------------------------------------------- *)

let files = ref []
let mktbl = ref None
let mktgt = ref false

let options =
  O.generic_options @
  ["--waiting-only",
    Arg.Unit (fun _ -> extension := "waiting_only" :: !extension; waiting_only := true),
    "  count only waiting tasks";
    "--forked", Arg.Set forked, "  consider only forked processes";
    "--mktbl", Arg.String (fun s -> mktbl := Some s),
    "  make a comparison table";
    "--mktgt", Arg.Set mktgt, "  make a target graph (requires --cmd)";
    "--sockets", Arg.Set_int sockets,
    "  number of sockets (make a graph per socket)";
    "--occupied-idle", Arg.Set occem, "  count fully occupied and fully idle cores";
    "--overload", Arg.Unit (fun _ -> extension := "overload" :: !extension; overload := true),
    "  count load on each core";
    "--wc", Arg.Unit (fun _ -> extension := "wc" :: !extension; wc := true),
    "  time in which there is a work conservation issue";
    "--wcgraph", Arg.Unit (fun _ -> extension := "wc" :: !extension; wcgraph := true),
    "  graph of time in which there is a work conservation issue";
    "--socket-order", Arg.Unit (fun _ -> extension := "socketorder" :: !extension; socket_order := true),
    "  reorganize cores to put cores on the same socket adjacent (only relevant for --overload)";
    "--implot", Arg.Unit (fun _ -> ty := Printer.ImPlot(1)), (* TODO: use the stairs function *)
    "  generate graphs for implot";
]

let anonymous s = files := Util.cmd_to_list (Printf.sprintf "ls %s" s) @ !files

let usage = "running_waiting file*.dat [options]"

let _ =
  Arg.parse (Arg.align options) anonymous usage;
  O.fixmin();
  (if !mktgt && !O.cmds = [] && not !forked
  then failwith "--mktgt needs --cmd or --forked");
  match !mktbl with
    Some s -> make_table s !files
  | None ->
      let files =
      Util.get_files_and_endpoint !files !O.after_sleep false events in
      if !wcgraph
      then dowcgraph files
      else List.iter (if !mktgt then main_target else main) files
